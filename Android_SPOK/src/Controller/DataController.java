package Controller;

import java.util.ArrayList;
import Helpers.DataManager;
import Helpers.DataFetchers.Schedule.MessageList;
import Helpers.ObserverPattern.Observer;
import Model.AdSubject;
import Model.Schools;
import Model.ScheduleComposite.SchedulePart;

public class DataController
{
	// singleton
	public static DataController instance;

	private DataManager dataManager;

	public DataController()
	{
		dataManager = new DataManager();
	}

	public void GetDataFromServer()
	{
		dataManager.GetDataFromServer();
	}

	public SchedulePart getMySchedule(Observer obs)
	{
		return dataManager.getMySchedule(obs);
	}

	public MessageList getChangesInSchedule(Observer obs)
	{
		return dataManager.getChangesInSchedule(obs);
	}

	public ArrayList<String> getSchoolList(Schools schools)
	{
		return dataManager.getSchoolList(schools);
	}

	public String[] getStudyCallSettings()
	{
		return dataManager.getStudyCallSettings();
	}

	public String[] getAvansSettings()
	{
		return dataManager.getAvansSettigns();
	}

	public AdSubject getAds(AdSubject ads)
	{
		return dataManager.getAds(ads);
	}

	public void setDataChanged()
	{

		dataManager.setDataChanged();

	}

	// singleton
	public static DataController getInstance()
	{
		if(instance == null)
			instance = new DataController();
		return instance;
	}

}
