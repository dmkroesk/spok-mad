package Controller;

import android.content.Context;
import android.content.Intent;
import com.spok.android_spok.AdActivity;
import com.spok.android_spok.ClockSettingsActivity;
import com.spok.android_spok.LogoffSettingsActivity;
import com.spok.android_spok.OVSettingsActivity;
import com.spok.android_spok.R;
import com.spok.android_spok.ScheduleOptionsActivity;
import com.spok.android_spok.SettingsActivity;
import com.spok.android_spok.SocialMediaActivity;
import com.spok.android_spok.StudyCallProfileSettingsActivity;

public class SettingsController
{
	private static SettingsController instance;
	private String school;

	private SettingsController()
	{
		school = "";
	}

	public static SettingsController getInstance()
	{
		if(instance == null)
			instance = new SettingsController();
		return instance;
	}

	public void navigateToSettings(Context context)
	{
		Intent settingsIntent = new Intent(context, SettingsActivity.class);
		if(school.equalsIgnoreCase(context.getString(R.string.avans)))
			settingsIntent.putExtra(context.getString(R.string.intent_extra_type), context.getString(R.string.avans_settings));
		else
			settingsIntent.putExtra(context.getString(R.string.intent_extra_type), context.getString(R.string.studycall_settings));
		context.startActivity(settingsIntent);
	}

	public void navigateToSetting(Context context, String item)
	{
		Intent settingIntent = null;
		if(item.equalsIgnoreCase(context.getString(R.string.schedule_item)))
		{
			settingIntent = new Intent(context, ScheduleOptionsActivity.class);
		}
		else if(item.equalsIgnoreCase(context.getString(R.string.travel_item)))
		{
			settingIntent = new Intent(context, OVSettingsActivity.class);
		}
		else if(item.equalsIgnoreCase(context.getString(R.string.profile_item)))
		{
			settingIntent = new Intent(context, StudyCallProfileSettingsActivity.class);
		}
		else if(item.equalsIgnoreCase(context.getString(R.string.social_item)))
		{
			settingIntent = new Intent(context, SocialMediaActivity.class);
			if(school.equalsIgnoreCase(context.getString(R.string.avans)))
				settingIntent.putExtra(context.getString(R.string.intent_extra_type), context.getString(R.string.social_avans_settings));
			else
				settingIntent.putExtra(context.getString(R.string.intent_extra_type), context.getString(R.string.social_settings));
		}
		else if(item.equalsIgnoreCase(context.getString(R.string.ad_item)))
		{
			settingIntent = new Intent(context, AdActivity.class);
		}
		else if(item.equalsIgnoreCase(context.getString(R.string.alarm_item)))
		{
			settingIntent = new Intent(context, ClockSettingsActivity.class);
		}
		else if(item.equalsIgnoreCase(context.getString(R.string.logoff_item)))
		{
			settingIntent = new Intent(context, LogoffSettingsActivity.class);
		}
		context.startActivity(settingIntent);
	}

	public String getSchool()
	{
		return school;
	}

	public void setSchool(String school)
	{
		this.school = school;
	}

}
