package Helpers.Account;

import Helpers.GlobalVariables;
import Helpers.DataFetchers.DataGetter;

public class AccountController
{
	public static String accountname = "";
	public static boolean verified = false;
	private static AccountController instance = null;
	private DataGetter dg = null;
	
	private AccountController(DataGetter dg){
		this.dg = dg;
	}
	
	public static AccountController getInstance(){
		if(instance == null){
			instance = new AccountController(GlobalVariables.globalDG);
			return instance;
		}else{
			return instance;
		}
	}
	
	public boolean verifyUser(String user, String password){
		if(dg.verifyUser(user, password)){
			return true;
		}
		return false;
	}
	
	public boolean registerUser(String user, String password){
		if(dg.registerUser(user, password)){
			return true;
		}
		return false;
	}
	
}
