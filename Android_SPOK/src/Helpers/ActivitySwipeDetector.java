package Helpers;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class ActivitySwipeDetector implements View.OnTouchListener
{

	static final String logTag = "ActivitySwipeDetector";
	private Swipe activity;
	static final int MIN_DISTANCE = 100;
	private float downX, downY, upX, upY;

	public ActivitySwipeDetector(Swipe activity)
	{
		this.activity = activity;
	}

	public void onRightToLeftSwipe()
	{
		activity.onRightToLeftSwipe();
	}

	public void onLeftToRightSwipe()
	{
		activity.onLeftToRightSwipe();
	}

	public void onTopToBottomSwipe()
	{
		activity.onTopToBottomSwipe();

	}

	public void onBottomToTopSwipe()
	{
		activity.onBottomToTopSwipe();

	}

	public boolean onTouch(View v, MotionEvent event)
	{

		switch(event.getAction())
		{
			case MotionEvent.ACTION_DOWN:
			{
				downX = event.getX();
				downY = event.getY();
				return true;
			}
			case MotionEvent.ACTION_UP:
			{
				upX = event.getX();
				upY = event.getY();

				float deltaX = downX - upX;
				float deltaY = downY - upY;

				// swipe horizontal?
				if(Math.abs(deltaX) > MIN_DISTANCE)
				{
					// left or right
					if(deltaX < 0)
					{
						this.onLeftToRightSwipe();
						return true;
					}
					if(deltaX > 0)
					{
						this.onRightToLeftSwipe();
						return true;
					}
				}
				else
				{
					Log.i(logTag, "Swipe was only " + Math.abs(deltaX) + " long, need at least " + MIN_DISTANCE);
					return false; // We don't consume the event
				}

				// swipe vertical?
				if(Math.abs(deltaY) > MIN_DISTANCE)
				{
					// top or down
					if(deltaY < 0)
					{
						this.onTopToBottomSwipe();
						return true;
					}
					if(deltaY > 0)
					{
						this.onBottomToTopSwipe();
						return true;
					}
				}
				else
				{
					Log.i(logTag, "Swipe was only " + Math.abs(deltaX) + " long, need at least " + MIN_DISTANCE);
					return false; // We don't consume the event
				}

				return true;
			}
		}
		return false;
	}

}