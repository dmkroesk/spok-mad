package Helpers;

import java.util.ArrayList;
import Model.Ad;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import com.spok.android_spok.R;

public class AdListAdapter extends ArrayAdapter<Ad>
{
	private final Context context;
	private final ArrayList<Ad> items;

	public AdListAdapter(Context con, ArrayList<Ad> list)
	{
		super(con, R.layout.ads_list_item, list);
		context = con;
		items = list;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.ads_list_item, parent, false);

		TextView titleText = (TextView) rowView.findViewById(R.id.ad_title);
		TextView contentText = (TextView) rowView.findViewById(R.id.ad_description);
		CheckBox enableBox = (CheckBox) rowView.findViewById(R.id.ad_checkbox);

		titleText.setText(items.get(position).getHeader());
		contentText.setText(items.get(position).getContent());
		enableBox.setChecked(items.get(position).isSelected());
		return rowView;
	}
}
