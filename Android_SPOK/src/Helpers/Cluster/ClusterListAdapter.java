package Helpers.Cluster;

import java.util.List;
import Model.ScheduleComposite.Periode;
import Model.ScheduleComposite.SchedulePart;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.spok.android_spok.R;

public class ClusterListAdapter extends ArrayAdapter<SchedulePart>
{

	private final Context context;
	private final List<SchedulePart> itemsArrayList;

	public ClusterListAdapter(Context context, List<SchedulePart> itemsArrayList, ListView lv)
	{
		super(context, R.layout.day_studycall_listrow, itemsArrayList);

		lv.setDividerHeight(0);

		this.context = context;
		this.itemsArrayList = itemsArrayList;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{

		// 1. Create inflater
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		// 2. Get rowView from inflater
		View rowView = inflater.inflate(R.layout.day_studycall_listrow, parent, false);

		// 3. Get the two text view from the rowView
		TextView labelView = (TextView) rowView.findViewById(R.id.Time);
		TextView valueView = (TextView) rowView.findViewById(R.id.Subject);
		TextView studyHourView = (TextView) rowView.findViewById(R.id.subjectID);
		TextView locationView = (TextView) rowView.findViewById(R.id.location);

		// 4. Set the text for textView
		labelView.setText(itemsArrayList.get(position).getPeriode().timeToString());
		valueView.setText(itemsArrayList.get(position).getPeriode().getSubject());
		studyHourView.setText(String.valueOf(itemsArrayList.get(position).getPeriode().getStudyHour()));
		locationView.setText(itemsArrayList.get(position).getPeriode().getLocation());
		Periode periode = itemsArrayList.get(position).getPeriode();
		View clusterCLine = (View) rowView.findViewById(R.id.clusterLine);
		View clusterBLine = (View) rowView.findViewById(R.id.bottomHr);

		clusterCLine.setVisibility(View.INVISIBLE);
		clusterBLine.setVisibility(View.INVISIBLE);

		// 5. return rowView
		return rowView;
	}
}