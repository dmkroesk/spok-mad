package Helpers.Cluster;

import com.spok.android_spok.R;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ClusterMasterFragment  extends Fragment
{
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		 return inflater.inflate(R.layout.cluster_option_fragment_master, container, false);
	}

}
