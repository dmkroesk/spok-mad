package Helpers.DataFetchers.Account;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.net.ssl.HttpsURLConnection;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import Helpers.HttpNewClient;
import Helpers.ObserverPattern.LoginObserver;
import Helpers.ObserverPattern.LoginSubject;
import android.os.AsyncTask;
import android.util.Log;

public class DataFetchVerifyUser extends AsyncTask<String, String, Boolean> implements LoginSubject
{
	private List<LoginObserver> observers = new ArrayList<LoginObserver>();
	private boolean status = false;
	@Override
	protected Boolean doInBackground(String... param)
	{
		/*try {
			// Simulate network access.
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			return false;
		}*/
		String password = param[0];
		String user = param[1];
		Log.i("SPOK", "ENTERING BACKGROUND VERIFYUSER with info:" + user + " and pass " + password);
		String https_url = "https://145.48.6.12/authentication/login.php?user=" + user + "&pass=" + password;

		HttpNewClient httpclientgetter = new HttpNewClient();
		HttpClient httpclient = httpclientgetter.getNewHttpClient();
		// Prepare a request object
		HttpGet httpget = new HttpGet(https_url);

		// Execute the request
		HttpResponse response;
		try
		{
			response = httpclient.execute(httpget);
			// Examine the response status
			Log.i("Praeda", response.getStatusLine().toString());

			// Get hold of the response entity
			HttpEntity entity = response.getEntity();
			// If the response does not enclose an entity, there is no need
			// to worry about connection release

			if(entity != null)
			{

				// A Simple JSON Response Read
				InputStream instream = entity.getContent();
				String result = convertStreamToString(instream);
				// now you have the string representation of the HTML request
				instream.close();
				Log.e("SPOK", result);
				if(result.equals("{\"status\":\"Logged in\"}\n")){
					Log.e("SPOK", "LOGGED IN");
					return true;
				}else{
					Log.e("SPOK", "NOT LOGGED IN");
					return false;
				}
			}

		}
		catch(Exception e)
		{
			e.getStackTrace();
		}

		return false;
	}
	
	@Override
	protected void onPostExecute(final Boolean success) {

		if (success) {
			status = true;
			notifyObservers();
		} else {
			status = false;
			notifyObservers();
		}
	}

	@Override
	protected void onCancelled() {
		status = false;
		notifyObservers();
	}
	

	private static String convertStreamToString(InputStream is)
	{
		/* To convert the InputStream to String we use the BufferedReader.readLine()
		 * method. We iterate until the BufferedReader return null which means
		 * there's no more data to read. Each line will appended to a StringBuilder
		 * and returned as String. */
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try
		{
			while((line = reader.readLine()) != null)
			{
				sb.append(line + "\n");
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				is.close();
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

	@Override
	public void registerObserver(LoginObserver observer)
	{
		observers.add(observer);

	}

	@Override
	public void removeObserver(LoginObserver observer)
	{
		observers.remove(observer);

	}

	@Override
	public void notifyObservers()
	{
		try
		{
			for(LoginObserver ob : observers)
			{
				System.out.println("Notifying Observers on change");
				ob.update(status);
			}
		}
		catch(Exception q)
		{
		}
	}
}
