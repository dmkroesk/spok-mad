package Helpers.DataFetchers.Ad;

import java.io.IOException;
import java.util.ArrayList;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import Helpers.HttpNewClient;
import Model.Ad;
import Model.AdSubject;
import android.os.AsyncTask;
import android.util.Log;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class AdApiCall extends AsyncTask<String, Void, Void>
{
	private static final String AD_TITLE = "categorie_naam";
	private static final String AD_CONTENT = "omschrijving";
	private static final String AD_LINK = "hyperlink";
	private static final String AD_ENABLED = "enabled";
	private static String TAG = "Ad Api Caller";
	private AdSubject adSubject;

	public AdApiCall(AdSubject ads)
	{
		adSubject = ads;
	}

	@Override
	protected Void doInBackground(String... args)
	{
		Log.i(TAG, "doInBackground");
		getApiData();
		return null;
	}

	@Override
	protected void onPostExecute(Void result)
	{
		Log.i(TAG, "onPostExecute");
		adSubject.notifyAllObservers();
	}

	void getApiData()
	{
		final String url = "https://145.48.6.12/getadverts.php?studentnummer=112";
		HttpNewClient sslClient = new HttpNewClient();
		HttpClient client = sslClient.getNewHttpClient();
		try
		{
			HttpGet adGet = new HttpGet(url);
			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			String result = client.execute(adGet, responseHandler);
			Log.d(TAG, result);
			parseJson(result);
		}
		catch(ClientProtocolException cpe)
		{
			cpe.printStackTrace();
		}
		catch(IOException ioe)
		{
			ioe.printStackTrace();
		}
	}

	void parseJson(String jsonString)
	{
		JsonElement jsonElement = new JsonParser().parse(jsonString);
		JsonArray ads = jsonElement.getAsJsonArray();
		ArrayList<Ad> adList = new ArrayList<Ad>();
		for(int i = 0; i < ads.size(); i++)
		{
			JsonObject jsonObj = (JsonObject) ads.get(i);
			boolean enabled = jsonObj.get(AD_ENABLED).getAsInt() != 0;
			Ad ad = new Ad(jsonObj.get(AD_TITLE).getAsString(), jsonObj.get(AD_CONTENT).getAsString(), jsonObj.get(AD_LINK).getAsString(), enabled);
			adList.add(ad);
		}
		adSubject.setAds(adList);
	}
}
