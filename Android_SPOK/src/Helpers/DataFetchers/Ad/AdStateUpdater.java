package Helpers.DataFetchers.Ad;

import java.io.IOException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import Helpers.HttpNewClient;
import Model.Ad;
import android.os.AsyncTask;
import android.util.Log;

public class AdStateUpdater extends AsyncTask<String, Void, Void>
{
	private static final String BASE_URL = "https://145.48.6.12/updateadverts.php?";
	private static final String STUDENT_NO = "studentnummer=";
	private static final String CATEGORY_NAME = "categorie_naam=";
	private static final String DESCRIPTION = "omschrijving=";
	private static final String ENABLED = "enabled=";
	private static final String HYPERLINK = "hyperlink=";

	private static final String TEST_NO = "112";
	private static String TAG = "Ad State Updater";
	private Ad ad;

	public AdStateUpdater(Ad adToUpdate)
	{
		ad = adToUpdate;
	}

	@Override
	protected Void doInBackground(String... args)
	{
		Log.i(TAG, "doInBackground");
		int enabled = ad.isSelected() ? 1 : 0;
		ad.replaceSpaces();
		final String url = BASE_URL + STUDENT_NO + TEST_NO + "&" + CATEGORY_NAME + ad.getHeader() + "&" + DESCRIPTION + ad.getContent() + "&" + ENABLED + enabled + "&" + HYPERLINK + ad.getHyperlink();
		HttpNewClient sslClient = new HttpNewClient();
		HttpClient client = sslClient.getNewHttpClient();
		try
		{
			HttpGet postAd = new HttpGet(url);
			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			String result = client.execute(postAd, responseHandler);
			Log.d(TAG, result);
			/* If it succeeds, the result is the studentnumber AFAIK. */
		}
		catch(ClientProtocolException cpe)
		{
			cpe.printStackTrace();
		}
		catch(IOException ioe)
		{
			ioe.printStackTrace();
		}
		return null;
	}
}
