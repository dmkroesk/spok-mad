package Helpers.DataFetchers;

import java.util.ArrayList;
import Helpers.DataFetchers.Schedule.MessageList;
import Model.Ad;
import Model.AdSubject;
import Model.AppSettings;
import Model.Schools;
import Model.ScheduleComposite.SchedulePart;

public interface DataGetter
{
	void getData();

	SchedulePart getMySchedule(SchedulePart week);

	MessageList getChangesInSchedule(MessageList list);

	ArrayList<String> getSchoolList(Schools schools);

	String[] getStudyCallSettings();

	String[] getAvansSettings();

	AppSettings getAppSettings();

	AdSubject getAds(AdSubject ads);

	Boolean updateAd(Ad ad);

	Boolean verifyUser(String user, String password);

	Boolean registerUser(String user, String password);
}