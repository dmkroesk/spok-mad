package Helpers.DataFetchers;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import Helpers.DataFetchers.Schedule.MessageList;
import Model.Ad;
import Model.AdSubject;
import Model.AppSettings;
import Model.Schools;
import Model.ScheduleComposite.SchedulePart;
import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class LocalDatabaseData extends SQLiteOpenHelper implements DataGetter
{

	private static LocalDatabaseData instance;

	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 1;

	// Database Name
	private static final String DATABASE_NAME = "Schedule";
	// table name
	private static final String TABLE_ROOSTER = "ROOSTER_ITEM";
	private static final String SETTINGS = "SETTINGS";

	// Table Columns TABLE_ROOSTER
	private static final String KEY_ROOSTER_ID = "ROOSTER_ID";
	private static final String KEY_ROOSTER_DAY = "DAY";
	private static final String KEY_ROOSTER_TIME = "TIME";
	private static final String KEY_ROOSTER_ACTIVITY = "ACTIVITY";

	// Table Columns SETTINGS
	private static final String KEY_SETTINGS_APPVERSION = "APPVERSION";
	private static final String KEY_SETTINGS_REGID = "REGID";

	private LocalDatabaseData(Context context)
	{
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		File database = context.getDatabasePath("databasename.db");

		if(!database.exists())
		{
			// Database does not exist so copy it from assets here
			setupdb(context);
		}
	}

	private void setupdb(Context context)
	{
		AssetManager assetManager = context.getAssets();
		String text = "";
		InputStream input;
		try
		{
			input = assetManager.open("INITSQLLOCAL.txt");
			int size = input.available();
			byte[] buffer = new byte[size];
			input.read(buffer);
			input.close();
			text = new String(buffer);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}

	public static LocalDatabaseData getInstance(Context context)
	{
		if(instance == null)
		{
			instance = new LocalDatabaseData(context);
		}
		return instance;
	}

	@Override
	public void getData()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public SchedulePart getMySchedule(SchedulePart week)
	{
		// TODO not yet fully implemented

		// Create a List of Rooster Items

		// Make a select query
		String selectQuery = "SELECT * FROM " + TABLE_ROOSTER;

		// Get the database
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if(cursor.moveToFirst())
		{
			do
			{
				// Create the rooster item through cursor.getString(index);

				// Add rooster to list

			}
			while(cursor.moveToNext());
		}

		// Return rooster here
		return null;
	}

	@Override
	public ArrayList<String> getSchoolList(Schools schools)
	{
		/* TODO: Get all the schools in the database and return them. */
		return null;
	}

	@Override
	public String[] getStudyCallSettings()
	{
		/* TODO: Get all the settings for studycall from the database and return them. */
		return null;
	}

	@Override
	public String[] getAvansSettings()
	{
		/* TODO: Get all the settings for Avans from the database and return them. */
		return null;
	}

	@Override
	public AppSettings getAppSettings()
	{
		// todo
		return null;
	}

	@Override
	public AdSubject getAds(AdSubject ads)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onCreate(SQLiteDatabase arg0)
	{
		// We do nothing here, since we need the applicationcontext to setup our db through the assets sql file.
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2)
	{
		// We will have to discuss about how we gonna do this.
	}

	@Override
	public MessageList getChangesInSchedule(MessageList list)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean verifyUser(String user, String password)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean registerUser(String user, String password)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean updateAd(Ad ad)
	{
		// TODO Auto-generated method stub
		return null;
	}

}