package Helpers.DataFetchers.Profile;

import java.io.IOException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import Helpers.HttpNewClient;
import Model.PersonalInformation;
import android.os.AsyncTask;
import android.util.Log;

public class Registrator extends AsyncTask<String, String, Void>
{
	private static final String BASE_URL = "https://145.48.6.12/createuser.php?";
	private static final String STUDENT_NO = "studentnummer=";
	private static final String PASSWORD = "wachtwoord=";
	private static final String FIRST_NAME = "voornaam=";
	private static final String LAST_NAME = "achternaam=";
	private static final String INSERTION = "tussenvoegsel=";
	private static final String EMAIL = "email=";
	private static final String BIRTH_DATE = "geboortedatum=";
	private static final String SCHOOL = "school=";

	private static String TAG = "Registrator";
	private PersonalInformation profile;

	public Registrator(PersonalInformation profileToRegister)
	{
		profile = profileToRegister;
	}

	@Override
	protected Void doInBackground(String... args)
	{
		Log.i(TAG, "doInBackground");
		final String url = BASE_URL + STUDENT_NO + profile.getStudentNumber() + "&" + PASSWORD + profile.getPassword() + "&" + FIRST_NAME + profile.getFirstName() + "&" + INSERTION
				+ profile.getInsertion() + "&" + LAST_NAME + profile.getLastName() + "&" + EMAIL + profile.getEmail() + "&" + BIRTH_DATE + profile.getBirthDate() + "&" + SCHOOL + profile.getSchool();
		HttpNewClient sslClient = new HttpNewClient();
		HttpClient client = sslClient.getNewHttpClient();
		try
		{
			HttpGet postAd = new HttpGet(url);
			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			String result = client.execute(postAd, responseHandler);
			Log.d(TAG, result);
		}
		catch(ClientProtocolException cpe)
		{
			cpe.printStackTrace();
		}
		catch(IOException ioe)
		{
			ioe.printStackTrace();
		}
		return null;
	}
}
