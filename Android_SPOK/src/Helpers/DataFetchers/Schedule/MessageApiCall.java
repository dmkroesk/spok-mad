package Helpers.DataFetchers.Schedule;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import Helpers.HttpNewClient;
import Helpers.Message.Message;
import Model.ScheduleComposite.Cluster;
import android.os.AsyncTask;
import android.util.Log;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class MessageApiCall extends AsyncTask<String, Void, Void>
{
	private static final String TAG = "AsyncScheduleCall";
	List<Message> messages;
	MessageList list;
	ArrayList<Cluster> clusterList = new ArrayList<Cluster>();

	public MessageApiCall()
	{

	}

	public MessageApiCall(MessageList list)
	{
		this.list = list;
		this.messages = list.getMessages();
	}

	@Override
	protected Void doInBackground(String... arg0)
	{
		Log.i(TAG, "doInBackground");
		getApiData();
		return null;
	}

	@Override
	protected void onPostExecute(Void result)
	{
		list.notifyAllObservers();
	}

	@Override
	protected void onPreExecute()
	{
		Log.i(TAG, "onPreExecute");
		// dialog = ProgressDialog.show((MainActivity)user,"" , "Loading...");
	}

	@Override
	protected void onProgressUpdate(Void... values)
	{
		Log.i(TAG, "onProgressUpdate");
	}

	void getApiData()
	{

		// The URL
		final String url = "https://145.48.6.12/getberichten.php?studentnummer=40825";

		try
		{

			HttpNewClient httpclientgetter = new HttpNewClient();
			HttpClient httpclient = httpclientgetter.getNewHttpClient();
			// Prepare a request object
			HttpGet httpget = new HttpGet(url);

			// Execute the request
			HttpResponse response;

			response = httpclient.execute(httpget);
			// Examine the response status
			// Get hold of the response entity
			HttpEntity entity = response.getEntity();
			// If the response does not enclose an entity, there is no need
			// to worry about connection release

			if(entity != null)
			{

				// A Simple JSON Response Read
				InputStream instream = entity.getContent();
				String result = convertStreamToString(instream);
				parseJson(result);

			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	void parseJson(String jsonString)
	{
		JsonElement jelement = new JsonParser().parse(jsonString);

		JsonArray messages = jelement.getAsJsonArray();
		for(int i = 0; i < messages.size(); i++)
		{
			JsonObject currentObject = (JsonObject) messages.get(i);
			addMessage(currentObject);
		}
	}

	void addMessage(JsonObject currentObject)
	{
		Message message = new Message();
		message.setMessage(currentObject.get("bericht").getAsString());

		// get Start object and end
		JsonObject start = (JsonObject) currentObject.get("datum");
		String startDate = start.get("date").getAsString();

		try
		{
			Date startParsed = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(startDate);
			message.setDate(startParsed);
		}
		catch(ParseException parseEx)
		{
			parseEx.printStackTrace();
		}
		messages.add(message);
	}

	private static String convertStreamToString(InputStream is)
	{
		/* To convert the InputStream to String we use the BufferedReader.readLine()
		 * method. We iterate until the BufferedReader return null which means
		 * there's no more data to read. Each line will appended to a StringBuilder
		 * and returned as String. */
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try
		{
			while((line = reader.readLine()) != null)
			{
				sb.append(line + "\n");
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				is.close();
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

}