package Helpers.DataFetchers.Schedule;

import java.util.ArrayList;
import java.util.List;
import Helpers.Message.Message;
import Helpers.ObserverPattern.Subject;

public class MessageList extends Subject
{
	List<Message> messages = new ArrayList<Message>();

	public List<Message> getMessages()
	{
		return messages;
	}

	public void setMessages(List<Message> messages)
	{
		this.messages = messages;
	}
}
