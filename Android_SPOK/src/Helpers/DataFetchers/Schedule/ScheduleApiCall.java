package Helpers.DataFetchers.Schedule;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import Helpers.HttpNewClient;
import Model.ScheduleComposite.Cluster;
import Model.ScheduleComposite.Periode;
import Model.ScheduleComposite.ScheduleDayPart;
import Model.ScheduleComposite.SchedulePart;
import Model.ScheduleComposite.SchedulePeriodePart;
import Model.ScheduleComposite.ScheduleWeekPart;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class ScheduleApiCall extends AsyncTask<String, Void, Void>
{
	private static final String TAG = "AsyncScheduleCall";
	ScheduleWeekPart week;
	ArrayList<Cluster> clusterList = new ArrayList<Cluster>();

	public ScheduleApiCall()
	{

	}

	public ScheduleApiCall(ScheduleWeekPart week)
	{
		this.week = week;
	}

	@Override
	protected Void doInBackground(String... arg0)
	{
		Log.i(TAG, "doInBackground");
		getApiData();
		return null;
	}

	@Override
	protected void onPostExecute(Void result)
	{
		week.notifyAllObservers();
	}

	@Override
	protected void onPreExecute()
	{
		Log.i(TAG, "onPreExecute");
		// dialog = ProgressDialog.show((MainActivity)user,"" , "Loading...");
	}

	@Override
	protected void onProgressUpdate(Void... values)
	{
		Log.i(TAG, "onProgressUpdate");
	}

	void getApiData()
	{

		// The URL
		final String url = "https://145.48.6.12/getrooster.php?studentnummer=40825";

		try
		{

			HttpNewClient httpclientgetter = new HttpNewClient();
			HttpClient httpclient = httpclientgetter.getNewHttpClient();
			// Prepare a request object
			HttpGet httpget = new HttpGet(url);

			// Execute the request
			HttpResponse response;

			response = httpclient.execute(httpget);
			// Examine the response status
			// Get hold of the response entity
			HttpEntity entity = response.getEntity();
			// If the response does not enclose an entity, there is no need
			// to worry about connection release

			if(entity != null)
			{

				// A Simple JSON Response Read
				InputStream instream = entity.getContent();
				String result = convertStreamToString(instream);
				parseJson(result);
				// set the cluster list on the higest lvl
				// these will be used at the cluster options
				week.setClusterList(this.clusterList);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	boolean fiveDays = false;

	void parseJson(String jsonString)
	{
		JsonElement jelement = new JsonParser().parse(jsonString);

		JsonArray Appointments = jelement.getAsJsonArray();
		for(int i = 0; i < Appointments.size(); i++)
		{
			JsonObject currentObject = (JsonObject) Appointments.get(i);
			addDay(currentObject);

			if(fiveDays)
				return;
		}
	}

	void addDay(JsonObject currentObject)
	{
		// check if day exists inside week
		SchedulePart day = week.getDay(currentObject.get("Dag").getAsString());
		if(day == null)
		{
			if(week.getChilderen().size() >= 5)
			{
				fiveDays = true;
				return;
			}
			day = new ScheduleDayPart();
			Periode dayData = new Periode();
			dayData.setSubject(currentObject.get("Dag").getAsString());

			// get Start object and end
			JsonObject start = (JsonObject) currentObject.get("BeginDatumtijd");
			JsonObject end = (JsonObject) currentObject.get("EindDatumTijd");

			String startDate = start.get("date").getAsString();
			String endDate = end.get("date").getAsString();

			try
			{
				Date startParsed = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(startDate);
				Date endParsed = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(endDate);
				dayData.setStart(startParsed);
				dayData.setEnd(endParsed);
			}
			catch(ParseException parseEx)
			{
				parseEx.printStackTrace();
			}

			day.setPeriode(dayData);
			week.add(day);
		}
		addAppointmentToDay(day, currentObject);
	}

	void addAppointmentToDay(SchedulePart day, JsonObject currentObject)
	{
		// add appointment to day
		SchedulePeriodePart appointment = new SchedulePeriodePart();
		Periode dayData = new Periode();
		dayData.setSubject(currentObject.get("Vak").getAsString());
		dayData.setStudyHour((Integer.parseInt(currentObject.get("Lesuur").getAsString())));
		dayData.setTeacher(currentObject.get("Docent").getAsString());
		dayData.setLocation(currentObject.get("Lokaal").getAsString());
		dayData.setCluster(getCluster(currentObject.get("Groep").getAsString()));
		// get Start object and end
		JsonObject start = (JsonObject) currentObject.get("BeginDatumtijd");
		JsonObject end = (JsonObject) currentObject.get("EindDatumTijd");

		String startDate = start.get("date").getAsString();
		String endDate = end.get("date").getAsString();

		try
		{
			Date startParsed = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(startDate);
			Date endParsed = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(endDate);
			dayData.setStart(startParsed);
			dayData.setEnd(endParsed);
		}
		catch(ParseException parseEx)
		{
			parseEx.printStackTrace();
		}
		appointment.setPeriode(dayData);
		day.add(appointment);
	}

	Cluster getCluster(String clusterName)
	{
		// return cluster -> if not there it creates
		for(int i = 0; i < this.clusterList.size(); i++)
		{
			Cluster clus = this.clusterList.get(i);
			if(clus.getClusterName().equals(clusterName))
				return clus;
		}
		Cluster clus = new Cluster();
		clus.setClusterName(clusterName);
		Random rnd = new Random();
		clus.setColorOfCluster(Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256)));

		this.clusterList.add(clus);
		return clus;
	}

	private static String convertStreamToString(InputStream is)
	{
		/* To convert the InputStream to String we use the BufferedReader.readLine()
		 * method. We iterate until the BufferedReader return null which means
		 * there's no more data to read. Each line will appended to a StringBuilder
		 * and returned as String. */
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try
		{
			while((line = reader.readLine()) != null)
			{
				sb.append(line + "\n");
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				is.close();
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

}