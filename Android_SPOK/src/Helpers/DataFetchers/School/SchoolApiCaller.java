package Helpers.DataFetchers.School;

import java.io.IOException;
import java.util.ArrayList;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import Helpers.HttpNewClient;
import Model.Schools;
import android.os.AsyncTask;
import android.util.Log;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class SchoolApiCaller extends AsyncTask<String, Void, Void>
{
	private static final String SCHOOL_URL = "https://145.48.6.12/getschool.php";
	private static final String SCHOOL_NAME = "Name";
	private static String TAG = "School Api Caller";
	private Schools schools;

	public SchoolApiCaller(Schools schools)
	{
		this.schools = schools;
	}

	@Override
	protected Void doInBackground(String... args)
	{
		Log.i(TAG, "doInBackground");
		getApiData();
		return null;
	}

	@Override
	protected void onPostExecute(Void result)
	{
		Log.i(TAG, "onPostExecute");
		schools.notifyAllObservers();
	}

	void getApiData()
	{
		HttpNewClient sslClient = new HttpNewClient();
		HttpClient client = sslClient.getNewHttpClient();
		try
		{
			HttpGet schoolGet = new HttpGet(SCHOOL_URL);
			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			String result = client.execute(schoolGet, responseHandler);
			// Log.d(TAG, result);
			parseJson(result);
		}
		catch(ClientProtocolException cpe)
		{
			cpe.printStackTrace();
		}
		catch(IOException ioe)
		{
			ioe.printStackTrace();
		}
	}

	void parseJson(String jsonString)
	{
		JsonElement jsonElement = new JsonParser().parse(jsonString);
		JsonArray schoolsList = jsonElement.getAsJsonArray();
		ArrayList<String> schoolList = new ArrayList<String>();
		schoolList.add("Avans");
		for(int i = 0; i < schoolsList.size(); i++)
		{
			JsonObject jsonObj = (JsonObject) schoolsList.get(i);
			if(jsonObj.get(SCHOOL_NAME) != JsonNull.INSTANCE)
				schoolList.add(jsonObj.get(SCHOOL_NAME).getAsString());
		}
		schools.setSchools(schoolList);
	}
}
