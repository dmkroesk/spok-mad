package Helpers.DataFetchers;

import java.util.ArrayList;
import Helpers.DataFetchers.Ad.AdApiCall;
import Helpers.DataFetchers.Schedule.MessageApiCall;
import Helpers.DataFetchers.Schedule.MessageList;
import Helpers.DataFetchers.Schedule.ScheduleApiCall;
import Helpers.DataFetchers.School.SchoolApiCaller;
import Model.Ad;
import Model.AdSubject;
import Model.AppSettings;
import Model.Schools;
import Model.ScheduleComposite.SchedulePart;
import Model.ScheduleComposite.ScheduleWeekPart;

public class ServerCallerData implements DataGetter
{

	@Override
	public void getData()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public SchedulePart getMySchedule(SchedulePart week)
	{
		ScheduleApiCall caller = new ScheduleApiCall((ScheduleWeekPart) week);
		caller.execute();
		return week;
	}

	public void weekFetched(SchedulePart week)
	{

	}

	@Override
	public ArrayList<String> getSchoolList(Schools schools)
	{
		SchoolApiCaller schoolCaller = new SchoolApiCaller(schools);
		schoolCaller.execute();
		return schools.getSchools();
	}

	@Override
	public String[] getStudyCallSettings()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] getAvansSettings()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MessageList getChangesInSchedule(MessageList list)
	{
		MessageApiCall caller = new MessageApiCall(list);
		caller.execute();
		return list;
	}

	@Override
	public AdSubject getAds(AdSubject ads)
	{
		AdApiCall adCaller = new AdApiCall(ads);
		adCaller.execute();
		return ads;
	}

	@Override
	public AppSettings getAppSettings()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean verifyUser(String user, String password)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean registerUser(String user, String password)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean updateAd(Ad ad)
	{
		// TODO Auto-generated method stub
		return null;
	}

}
