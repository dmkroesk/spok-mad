package Helpers.DataFetchers;

import java.util.ArrayList;
import java.util.Date;
import Helpers.DataFetchers.Schedule.MessageList;
import Model.Ad;
import Model.AdSubject;
import Model.AppSettings;
import Model.Schools;
import Model.ScheduleComposite.Cluster;
import Model.ScheduleComposite.Periode;
import Model.ScheduleComposite.ScheduleDayPart;
import Model.ScheduleComposite.SchedulePart;
import Model.ScheduleComposite.SchedulePeriodePart;
import Model.ScheduleComposite.ScheduleWeekPart;
import android.graphics.Color;

public class TestData implements DataGetter
{

	@Override
	public void getData()
	{
		// TODO Auto-generated method stub

	}

	public static final long HOUR = 3600 * 1000; // in milli-seconds.

	public SchedulePart getMySchedule(SchedulePart week)
	{
		// create Week object
		week = new ScheduleWeekPart();
		// create days
		ScheduleDayPart mDay = new ScheduleDayPart();
		Periode mPeriode = new Periode();
		mDay.setPeriode(mPeriode);
		mPeriode.setSubject("Monday");
		SchedulePeriodePart sP1 = new SchedulePeriodePart();
		Periode sP1P = new Periode();
		sP1.setPeriode(sP1P);
		sP1P.setSubject("Les1");
		Date now = new Date();
		sP1P.setStart(new Date(now.getTime() - (HOUR)));
		sP1P.setEnd(new Date(now.getTime() - (HOUR / 2)));

		SchedulePeriodePart sP2 = new SchedulePeriodePart();
		Periode sP2P = new Periode();
		sP2.setPeriode(sP2P);
		sP2P.setSubject("Les2");
		sP2P.setStart(new Date(now.getTime() + (3 * HOUR)));
		sP2P.setEnd(new Date(now.getTime() + (4 * HOUR)));

		ScheduleDayPart tDay = new ScheduleDayPart();
		Periode TPeriode = new Periode();
		tDay.setPeriode(TPeriode);
		TPeriode.setSubject("TuesDay");
		SchedulePeriodePart sTP1 = new SchedulePeriodePart();
		Periode pTP = new Periode();
		sTP1.setPeriode(pTP);
		pTP.setSubject("Derp1");
		pTP.setStart(new Date(now.getTime() + (24 * HOUR)));
		pTP.setEnd(new Date(now.getTime() + (25 * HOUR)));

		SchedulePeriodePart sTP2 = new SchedulePeriodePart();
		Periode pTP2 = new Periode();
		sTP2.setPeriode(pTP2);
		pTP2.setSubject("Derp2");
		pTP2.setStart(new Date(now.getTime() + (27 * HOUR)));
		pTP2.setEnd(new Date(now.getTime() + (28 * HOUR)));

		// cluster crap
		SchedulePeriodePart c1 = new SchedulePeriodePart();
		Periode pc1 = new Periode();
		// new cluster obj
		Cluster clus1 = new Cluster();
		// clus1.setID(1);
		clus1.setColorOfCluster(Color.BLUE);
		pc1.setCluster(clus1);

		c1.setPeriode(pc1);
		pc1.setSubject("clus1");
		pc1.setStart(new Date(now.getTime() + (28 * HOUR)));
		pc1.setEnd(new Date(now.getTime() + (29 * HOUR)));

		SchedulePeriodePart c2 = new SchedulePeriodePart();
		Periode pc2 = new Periode();
		// new cluster obj
		Cluster clus2 = new Cluster();
		// clus2.setID(2);
		clus2.setColorOfCluster(Color.YELLOW);
		pc2.setCluster(clus2);

		c2.setPeriode(pc2);
		pc2.setSubject("clus2");
		pc2.setStart(new Date(now.getTime() + (29 * HOUR)));
		pc2.setEnd(new Date(now.getTime() + (30 * HOUR)));

		SchedulePeriodePart c3 = new SchedulePeriodePart();
		Periode pc3 = new Periode();
		// new cluster obj
		Cluster clus3 = new Cluster();
		// clus3.setID(3);
		clus3.setColorOfCluster(Color.GREEN);
		pc3.setCluster(clus3);
		c3.setPeriode(pc3);
		pc3.setSubject("clus3");
		pc3.setStart(new Date(now.getTime() + (30 * HOUR)));
		pc3.setEnd(new Date(now.getTime() + (31 * HOUR)));

		// fill composite object
		week.add(mDay);
		week.add(tDay);

		mDay.add(sP1);
		mDay.add(sP2);

		tDay.add(sTP1);
		tDay.add(sTP2);
		tDay.add(c1);
		tDay.add(c2);
		tDay.add(c3);

		return week;
	}

	// public SchedulePart getChangesInSchedule()
	// {
	// // create Week object
	// ScheduleWeekPart week = new ScheduleWeekPart();
	// // create days
	// ScheduleDayPart mDay = new ScheduleDayPart();
	// Periode mPeriode = new Periode();
	// mDay.setPeriode(mPeriode);
	// mPeriode.setSubject("Monday");
	//
	// Date now = new Date();
	//
	// SchedulePeriodePart sP2 = new SchedulePeriodePart();
	// Periode sP2P = new Periode();
	// sP2.setPeriode(sP2P);
	// sP2P.setSubject("Les2");
	// sP2P.setStart(new Date(now.getTime() + (3 * HOUR)));
	// sP2P.setEnd(new Date(now.getTime() + (4 * HOUR)));
	//
	// ScheduleDayPart tDay = new ScheduleDayPart();
	// Periode TPeriode = new Periode();
	// tDay.setPeriode(TPeriode);
	// TPeriode.setSubject("TuesDay");
	//
	// SchedulePeriodePart sTP1 = new SchedulePeriodePart();
	// Periode pTP = new Periode();
	// sTP1.setPeriode(pTP);
	// pTP.setSubject("Derp1");
	// pTP.setStart(new Date(now.getTime() + (24 * HOUR)));
	// pTP.setEnd(new Date(now.getTime() + (25 * HOUR)));
	//
	// // fill composite object
	// week.add(mDay);
	// week.add(tDay);
	// mDay.add(sP2);
	// tDay.add(sTP1);
	//
	// return week;
	// }

	public MessageList getChangesInSchedule(MessageList list)
	{
		// create Week object
		ScheduleWeekPart week = new ScheduleWeekPart();
		// create days
		ScheduleDayPart mDay = new ScheduleDayPart();
		Periode mPeriode = new Periode();
		mDay.setPeriode(mPeriode);
		mPeriode.setSubject("Monday");

		Date now = new Date();

		SchedulePeriodePart sP2 = new SchedulePeriodePart();
		Periode sP2P = new Periode();
		sP2.setPeriode(sP2P);
		sP2P.setSubject("Les2");
		sP2P.setStart(new Date(now.getTime() + (3 * HOUR)));
		sP2P.setEnd(new Date(now.getTime() + (4 * HOUR)));

		ScheduleDayPart tDay = new ScheduleDayPart();
		Periode TPeriode = new Periode();
		tDay.setPeriode(TPeriode);
		TPeriode.setSubject("TuesDay");

		SchedulePeriodePart sTP1 = new SchedulePeriodePart();
		Periode pTP = new Periode();
		sTP1.setPeriode(pTP);
		pTP.setSubject("Derp1");
		pTP.setStart(new Date(now.getTime() + (24 * HOUR)));
		pTP.setEnd(new Date(now.getTime() + (25 * HOUR)));

		// fill composite object
		week.add(mDay);
		week.add(tDay);
		mDay.add(sP2);
		tDay.add(sTP1);

		return list;
	}

	@Override
	public ArrayList<String> getSchoolList(Schools schools)
	{
		ArrayList<String> returnValue = new ArrayList<String>();
		returnValue.add("Avans");
		returnValue.add("Atlas College");
		returnValue.add("Adelaar de");
		returnValue.add("Anker 't");
		returnValue.add("Bosje 't");
		returnValue.add("Binkenburg");
		returnValue.add("De Boef Voortgezet Onderwijs");
		returnValue.add("De Beer College");
		returnValue.add("Hendriks C ollege");
		return returnValue;
	}

	@Override
	public String[] getStudyCallSettings()
	{
		String[] returnValue = new String[4];
		returnValue[0] = "Profiel";
		returnValue[1] = "Roosteropties";
		returnValue[2] = "Social Media";
		returnValue[3] = "Advertenties";
		return returnValue;
	}

	@Override
	public String[] getAvansSettings()
	{
		String[] returnValue = new String[5];
		returnValue[0] = "Roosteropties";
		returnValue[1] = "Reisopties";
		returnValue[2] = "Social Media";
		returnValue[3] = "Wekkeropties";
		returnValue[4] = "Afmelden";
		return returnValue;
	}

	@Override
	public AdSubject getAds(AdSubject ads)
	{
		ArrayList<Ad> adList = new ArrayList<Ad>();
		adList.add(new Ad("Bijbaantjes", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ut aliquam sapien, vitae rhoncus lectus.", "http://google.com", false));
		adList.add(new Ad("Diensten", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ut aliquam sapien, vitae rhoncus lectus.", "http://google.com", true));
		adList.add(new Ad("Evenementen", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ut aliquam sapien, vitae rhoncus lectus.", "http://google.com", true));
		adList.add(new Ad("Vodafone", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ut aliquam sapien, vitae rhoncus lectus.", "http://google.com", true));
		ads.setAds(adList);
		return ads;
	}

	@Override
	public AppSettings getAppSettings()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean verifyUser(String user, String password)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean registerUser(String user, String password)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean updateAd(Ad ad)
	{
		// TODO Auto-generated method stub
		return null;
	}

}