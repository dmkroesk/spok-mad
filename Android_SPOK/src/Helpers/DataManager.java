package Helpers;

import java.util.ArrayList;
import Helpers.DataFetchers.DataGetter;
import Helpers.DataFetchers.ServerCallerData;
import Helpers.DataFetchers.TestData;
import Helpers.DataFetchers.Schedule.MessageList;
import Helpers.ObserverPattern.Observer;
import Model.AdSubject;
import Model.Schools;
import Model.ScheduleComposite.SchedulePart;
import Model.ScheduleComposite.ScheduleWeekPart;

// class that will manage all the data
public class DataManager
{

	DataGetter TestData;
	DataGetter ServerData;
	DataGetter LocalData;

	public DataManager()
	{
		TestData = new TestData();
		ServerData = new ServerCallerData();
	}

	public void GetDataFromServer()
	{
		ServerData.getData();
	}

	// we are going to chach the week object
	// clusteroption must use the same week object as overview
	ScheduleWeekPart week;

	public SchedulePart getMySchedule(Observer obs)
	{
		if(week == null)
		{
			// create the weekPart and add the Observer
			week = new ScheduleWeekPart();
			week.addListener(obs);
			ServerData.getMySchedule(week);
		}
		else
		{
			week.addListener(obs);
			week.notifyAllObservers();
		}
		// is updated with the observerPattern
		return week;
	}

	public void setDataChanged()
	{
		week = null;
	}

	MessageList messages;

	public MessageList getChangesInSchedule(Observer obs)
	{
		if(messages == null)
		{
			messages = new MessageList();
			messages.addListener(obs);
			ServerData.getChangesInSchedule(messages);
		}
		else
		{
			messages.addListener(obs);
			messages.notifyAllObservers();
		}
		// is updated with the observerPattern
		return messages;
		// return TestData.getChangesInSchedule();
	}

	public ArrayList<String> getSchoolList(Schools schools)
	{
		return ServerData.getSchoolList(schools);
	}

	public String[] getStudyCallSettings()
	{
		return TestData.getStudyCallSettings();
	}

	public String[] getAvansSettigns()
	{
		return TestData.getAvansSettings();
	}

	public AdSubject getAds(AdSubject ads)
	{
		return ServerData.getAds(ads);
	}
}
