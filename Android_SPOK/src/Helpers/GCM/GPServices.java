package Helpers.GCM;

import java.io.IOException;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

public class GPServices
{

	private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

	/**
	 * Check the device to make sure it has the Google Play Services APK. If
	 * it doesn't, display a dialog that allows users to download the APK from
	 * the Google Play Store or enable it in the device's system settings.
	 */
	public boolean checkPlayServices(Context context)
	{
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
		if(resultCode != ConnectionResult.SUCCESS)
		{
			if(GooglePlayServicesUtil.isUserRecoverableError(resultCode))
			{
				GooglePlayServicesUtil.getErrorDialog(resultCode, (Activity) context, PLAY_SERVICES_RESOLUTION_REQUEST).show();
			}
			else
			{
				Log.i("SPOK", "This device is not supported.");
				((Activity) context).finish();
			}
			return false;
		}
		return true;
	}

	/**
	 * sets the current registration ID for application on GCM service, if there is one.
	 * <p>
	 * If result is empty, the app needs to register.
	 * 
	 * @return registration ID, or empty string if there is no existing
	 *         registration ID.
	 */
	public String getRegistrationId(Context context)
	{
		String registrationId = getRegistrationIdFromSQLLite();
		Log.e("SPOK", "id = ?/ " + registrationId);
		if(registrationId.isEmpty())
		{
			Log.i("SPOK", "Registration not found.");
			return "";
		}

		// Check if app was updated; if so, it must clear the registration ID
		// since the existing regID is not guaranteed to work with the new
		// app version.
		int registeredVersion = getRegisteredAppVersion();
		int currentVersion = getAppVersion(context);
		Log.i("SPOK", "current version " + currentVersion);
		if(registeredVersion != currentVersion)
		{
			Log.i("SPOK", "App version changed.");
			return "";
		}
		return registrationId;
	}

	private int getRegisteredAppVersion()
	{
		return 1;
	}

	/**
	 * Registers the application with GCM servers asynchronously.
	 * Stores the registration ID and the app versionCode
	 */
	public void registerInBackground(Context context)
	{
		new AsyncTask<Context, Void, String>()
		{
			@Override
			protected String doInBackground(Context... params)
			{
				GoogleCloudMessaging gcm = null;
				String regid = "";
				try
				{
					if(gcm == null)
					{
						gcm = GoogleCloudMessaging.getInstance(params[0]);
					}
					regid = gcm.register(GCMVariables.SENDER_ID);
					Log.i("SPOK", "Device registered, registration ID=" + regid);

					sendRegistrationIdToBackend();

				}
				catch(IOException ex)
				{
				}
				return regid;
			}

			@Override
			protected void onPostExecute(String regid)
			{
				//
			}
		}.execute(context, null, null);
	}

	/**
	 * @return Application's version code from the {@code PackageManager}.
	 */
	private int getAppVersion(Context context)
	{
		try
		{
			PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		}
		catch(NameNotFoundException e)
		{
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	/**
	 * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP or CCS to send
	 * messages to your app.
	 */
	private void sendRegistrationIdToBackend()
	{
		// TODO
	}

	/**
	 * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP or CCS to send
	 * messages to your app.
	 */
	private String getRegistrationIdFromSQLLite()
	{
		return TempGCMVariables.regid;
	}

}
