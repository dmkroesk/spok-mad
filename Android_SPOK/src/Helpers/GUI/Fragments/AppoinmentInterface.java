package Helpers.GUI.Fragments;

import android.view.View;

public interface AppoinmentInterface
{
	int getCirleColor();

	int getTextColor();

	void preformChanges(View rootview);
}
