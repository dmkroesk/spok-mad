package Helpers.GUI.Fragments;

import Helpers.GUI.Initialize_GUI;
import android.app.Activity;
import android.view.View;
import com.spok.android_spok.R;

public class Imp_Initialize_GUI_AvansAppointmentFragment implements Initialize_GUI, AppoinmentInterface
{
	int cirlceColor, textColor;

	@Override
	public void init(Activity activity)
	{
		cirlceColor = activity.getResources().getColor(R.color.avans_actionbar);
		textColor = activity.getResources().getColor(R.color.white);
	}

	@Override
	public int getCirleColor()
	{
		return cirlceColor;
	}

	@Override
	public int getTextColor()
	{
		return textColor;
	}

	@Override
	public void preformChanges(View rootview)
	{
		// TODO Auto-generated method stub

	}

}
