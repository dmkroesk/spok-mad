package Helpers.GUI.Fragments;

import Helpers.GUI.Initialize_GUI;
import android.app.Activity;
import android.view.View;
import android.widget.TextView;
import com.spok.android_spok.R;

public class Imp_Initialize_GUI_StudyCallAppointmentFragment implements Initialize_GUI, AppoinmentInterface
{
	int cirlceColor, textColor, textViewColor;

	@Override
	public void init(Activity activity)
	{
		cirlceColor = activity.getResources().getColor(R.color.white);
		textColor = activity.getResources().getColor(R.color.black);
		textViewColor = activity.getResources().getColor(R.color.white);
	}

	@Override
	public int getCirleColor()
	{
		return cirlceColor;
	}

	@Override
	public int getTextColor()
	{
		return textColor;
	}

	@Override
	public void preformChanges(View rootview)
	{

		TextView title = (TextView) rootview.findViewById(R.id.datum);
		title.setTextColor(textViewColor);

		TextView subject = (TextView) rootview.findViewById(R.id.subject);
		subject.setTextColor(textViewColor);

		TextView app = (TextView) rootview.findViewById(R.id.dayAppointment);
		app.setTextColor(textViewColor);

		TextView location = (TextView) rootview.findViewById(R.id.location);
		location.setTextColor(textViewColor);

		View hr = (View) rootview.findViewById(R.id.appointmenthr);
		hr.setBackgroundColor(textViewColor);

	}

}
