package Helpers.GUI.Fragments;

import Helpers.GUI.Initialize_GUI;
import android.app.Activity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.spok.android_spok.R;

public class Imp_Initialize_GUI_StudyCallDayFragment implements Initialize_GUI, DayInterface
{

	int textColor;

	@Override
	public void init(Activity activity)
	{
		textColor = activity.getResources().getColor(R.color.white);
	}

	@Override
	public void preformChanges(View rootview)
	{
		LinearLayout lay = (LinearLayout) rootview.findViewById(R.id.transparentrow);
		lay.setVisibility(View.GONE);

		TextView title = (TextView) rootview.findViewById(R.id.title);
		title.setTextColor(textColor);

		View hr = (View) rootview.findViewById(R.id.overviewhr);
		hr.setBackgroundColor(textColor);

	}

}
