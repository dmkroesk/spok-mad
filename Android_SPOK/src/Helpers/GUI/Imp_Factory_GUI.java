package Helpers.GUI;

import java.util.HashMap;
import Helpers.GUI.Fragments.Imp_Initialize_GUI_AvansAppointmentFragment;
import Helpers.GUI.Fragments.Imp_Initialize_GUI_AvansDayFragment;
import Helpers.GUI.Fragments.Imp_Initialize_GUI_StudyCallAppointmentFragment;
import Helpers.GUI.Fragments.Imp_Initialize_GUI_StudyCallDayFragment;
import Helpers.GUI.Message.Imp_Initialize_AvansMessage;
import Helpers.GUI.Message.Imp_Initialize_GUI_StudyCallMessage;
import Helpers.GUI.Overview.Imp_Initialize_AvansOverview;
import Helpers.GUI.Overview.Imp_Initialize_GUI_StudyCallOverview;
import android.annotation.SuppressLint;

public class Imp_Factory_GUI implements Factory_GUI
{
	private static Imp_Factory_GUI instance;
	@SuppressLint("UseSparseArrays")
	private HashMap<String, Integer> map = new HashMap<String, Integer>();

	// Implement it so it's protected.
	protected Imp_Factory_GUI()
	{
		map.put("Avans_Login", 0);
		map.put("StudyCall_Login", 1);
		map.put("MessagescreenActivity", 2);

		map.put("PersonalInformationActivity", 4);
		map.put("AvansSettings", 5);
		map.put("StudyCallLoginActivity", 6);
		map.put("StudyCallSchoolSelectionActivity", 7);
		map.put("AvansOverviewActivity", 8);
		map.put("StudyCallOverviewActivity", 9);
		map.put("StudyCallSettings", 10);
		map.put("AvansMessageActivity", 11);
		map.put("StudyCallMessageActivity", 12);

		map.put("AvansAppointmentFragment", 16);
		map.put("StudyCallAppointmentFragment", 17);
		map.put("AvansDayFragment", 18);
		map.put("StudyCallDayFragment", 19);
		map.put("SocialMediaActivity", 20);
		map.put("AvansSocialActivity", 21);
	}

	synchronized public static Imp_Factory_GUI getInstance()
	{
		if(instance == null)
		{
			instance = new Imp_Factory_GUI();
		}
		return instance;
	}

	@Override
	public Initialize_GUI create(String key)
	{

		Initialize_GUI guiinit = null;
		switch(map.get(key))
		{

			case 0:// Avans_Login
				guiinit = new Imp_Initialize_GUI_AvansLoginActivity();
				break;
			case 1:// StudyCall_Login
				guiinit = new Imp_Initialize_GUI_StudyCallLoginActivity();
				break;
			case 2:// MessagescreenActivity
					// TODO
				break;
			case 3:// OverviewActivity
				guiinit = new Imp_Initialize_GUI_StudyCallOverview();
				break;
			case 4:// PersonalInformationActivity
					// TODO
				break;
			case 5:// SettingsActivity
				guiinit = new Imp_Initialize_GUI_AvansSettingsActivity();
				break;
			case 6:// StudyCallLoginActivity
					// TODO
				break;
			case 7:// StudyCallSchoolSelectionActivity
					// TODO
				break;
			case 8:// StudyCallSchoolSelectionActivity
				guiinit = new Imp_Initialize_AvansOverview();
				break;
			case 9:// StudyCallSchoolSelectionActivity
				guiinit = new Imp_Initialize_GUI_StudyCallOverview();
				break;
			case 10:
				guiinit = new Imp_Initialize_GUI_StudyCallSettingsActivity();
				break;
			case 11:
				guiinit = new Imp_Initialize_AvansMessage();
				break;
			case 12:
				guiinit = new Imp_Initialize_GUI_StudyCallMessage();
				break;
			case 16:
				guiinit = new Imp_Initialize_GUI_AvansAppointmentFragment();
				break;
			case 17:
				guiinit = new Imp_Initialize_GUI_StudyCallAppointmentFragment();
				break;
			case 18:
				guiinit = new Imp_Initialize_GUI_AvansDayFragment();
				break;
			case 19:
				guiinit = new Imp_Initialize_GUI_StudyCallDayFragment();
				break;
			case 20:
				guiinit = new Imp_Initialize_GUI_SocialMediaActivity();
				break;
			case 21:
				guiinit = new Imp_Initialize_GUI_AvansSocialMediaActivity();
		}

		return guiinit;

	}

}
