package Helpers.GUI;

import Helpers.Account.AccountController;
import Helpers.DataFetchers.Account.DataFetchVerifyUser;
import Helpers.ObserverPattern.LoginObserver;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.spok.android_spok.HelpActivity;
import com.spok.android_spok.MainActivity;
import com.spok.android_spok.MessagescreenActivity;
import com.spok.android_spok.R;

public class Imp_Initialize_GUI_AvansLoginActivity implements Initialize_GUI, LoginObserver
{
	private DataFetchVerifyUser mAuthTask = null;
	Context context;
	private View mLoginFormView;
	private View mLoginStatusView;
	private TextView mUsernameText;
	private EditText mPasswordText;
	Activity activity;

	@Override
	public void init(final Activity activity)
	{
		this.activity = activity;
		this.context = activity.getApplicationContext();
		activity.getActionBar().hide();

		mLoginFormView = activity.findViewById(R.id.login_form);
		mLoginStatusView = activity.findViewById(R.id.login_status);
		mUsernameText = (TextView) activity.findViewById(R.id.login_username);
		mPasswordText = (EditText) activity.findViewById(R.id.login_password);

		ImageView imagelogo = (ImageView) activity.findViewById(R.id.login_logo);
		imagelogo.setImageResource(R.drawable.avanslogo);

		TextView logintext = (TextView) activity.findViewById(R.id.login_logintext);
		logintext.setText(R.string.login_blackboard);
		logintext.setTextColor(activity.getResources().getColor(R.color.avansred));

		Button helpcentrumtext = (Button) activity.findViewById(R.id.login_helpcenterbutton);
		helpcentrumtext.setTextColor(activity.getResources().getColor(R.color.black));
		helpcentrumtext.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Intent myIntent = new Intent(activity, HelpActivity.class);
				activity.startActivity(myIntent);
			}
		});
		Button cancelbutton = (Button) activity.findViewById(R.id.cancel_login);
		cancelbutton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Intent myIntent = new Intent(activity, MainActivity.class);
				activity.startActivity(myIntent);
			}
		});

		Button okbutton = (Button) activity.findViewById(R.id.ok_login);
		okbutton.setTextColor(activity.getResources().getColor(R.color.avansred));
		okbutton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				attemptLogin(mUsernameText.getText().toString(), mPasswordText.getText().toString());
			}
		});
	}

	/**
	 * Attempts to sign in or register the account specified by the login form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	public void attemptLogin(String username, String password)
	{
		Log.e("SPOK", "BUTTON CLICKED OK");
		if(mAuthTask != null)
		{
			return;
		}

		boolean cancel = false;

		// Check for a valid password.
		if(TextUtils.isEmpty(password))
		{
			cancel = true;
		}

		// Check for a valid email address.
		if(TextUtils.isEmpty(username))
		{
			cancel = true;
		}

		if(cancel)
		{
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			CharSequence text = "Login error!";
			int duration = Toast.LENGTH_SHORT;

			Toast toast = Toast.makeText(context, text, duration);
			toast.show();
		}
		else
		{
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			showProgress(true);
			mAuthTask = new DataFetchVerifyUser();
			mAuthTask.registerObserver(this);
			mAuthTask.execute(password, username);
		}
	}

	private void showProgress(final boolean show)
	{
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2)
		{
			int shortAnimTime = context.getResources().getInteger(android.R.integer.config_shortAnimTime);

			mLoginStatusView.setVisibility(View.VISIBLE);
			mLoginStatusView.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0).setListener(new AnimatorListenerAdapter()
			{
				@Override
				public void onAnimationEnd(Animator animation)
				{
					mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
				}
			});

			mLoginFormView.setVisibility(View.VISIBLE);
			mLoginFormView.animate().setDuration(shortAnimTime).alpha(show ? 0 : 1).setListener(new AnimatorListenerAdapter()
			{
				@Override
				public void onAnimationEnd(Animator animation)
				{
					mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
				}
			});
		}
		else
		{
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

	@Override
	public void update(boolean status)
	{
		// login success
		if(status == true)
		{
			mAuthTask = null;
			Intent myIntent = new Intent(activity, MessagescreenActivity.class);
			myIntent.putExtra("Type", "OverviewActivity");
			activity.startActivity(myIntent);
		}
		else
		{
			mAuthTask = null;
			showProgress(false);
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			CharSequence text = "Login error!";
			int duration = Toast.LENGTH_SHORT;

			Toast toast = Toast.makeText(context, text, duration);
			toast.show();
		}

	}

}
