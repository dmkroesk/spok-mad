package Helpers.GUI;

import Controller.DataController;
import Controller.SettingsController;
import android.app.ActionBar;
import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.spok.android_spok.R;

public class Imp_Initialize_GUI_AvansSettingsActivity implements Initialize_GUI
{

	@Override
	public void init(final Activity activity)
	{
		ColorDrawable actionBarColor = new ColorDrawable(activity.getResources().getColor(R.color.avans_actionbar));
		ActionBar actionBar = activity.getActionBar();
		actionBar.setBackgroundDrawable(actionBarColor);

		final ListView settingsList = (ListView) activity.findViewById(R.id.settings_list);
		ArrayAdapter<String> settingsListAdapter = new ArrayAdapter<String>(activity, R.layout.avans_settings_item, DataController.getInstance().getAvansSettings());
		settingsList.setAdapter(settingsListAdapter);
		settingsList.setOnItemClickListener(new AdapterView.OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
				String item = (String) settingsList.getItemAtPosition(position);
				SettingsController.getInstance().navigateToSetting(activity, item);
			}
		});
	}
}
