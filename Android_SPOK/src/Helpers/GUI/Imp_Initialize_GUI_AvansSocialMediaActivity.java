package Helpers.GUI;

import com.spok.android_spok.R;
import android.app.ActionBar;
import android.app.Activity;
import android.graphics.drawable.ColorDrawable;

public class Imp_Initialize_GUI_AvansSocialMediaActivity implements Initialize_GUI
{

	@Override
	public void init(Activity activity)
	{
		ColorDrawable actionBarColor = new ColorDrawable(activity.getResources().getColor(R.color.avans_actionbar));
		ActionBar actionBar = activity.getActionBar();
		actionBar.setBackgroundDrawable(actionBarColor);
	}
}
