package Helpers.GUI;

import android.app.ActionBar;
import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.spok.android_spok.R;

public class Imp_Initialize_GUI_SocialMediaActivity implements Initialize_GUI
{

	@Override
	public void init(Activity activity)
	{
		ColorDrawable actionBarColor = new ColorDrawable(activity.getResources().getColor(R.color.studycall_actionbar));
		ActionBar actionBar = activity.getActionBar();
		actionBar.setBackgroundDrawable(actionBarColor);

		RelativeLayout layout = (RelativeLayout) activity.findViewById(R.id.social_settings_layout);
		layout.setBackgroundResource(R.drawable.studycall_gradient);

		TextView socialText = (TextView) activity.findViewById(R.id.social_media_text);
		socialText.setTextColor(activity.getResources().getColor(R.color.white));
		TextView facebookText = (TextView) activity.findViewById(R.id.facebook_text);
		facebookText.setTextColor(activity.getResources().getColor(R.color.white));
		TextView twitterText = (TextView) activity.findViewById(R.id.twitter_text);
		twitterText.setTextColor(activity.getResources().getColor(R.color.white));
	}
}