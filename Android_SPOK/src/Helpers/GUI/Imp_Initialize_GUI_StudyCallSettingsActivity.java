package Helpers.GUI;

import Controller.DataController;
import Controller.SettingsController;
import android.app.ActionBar;
import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import com.spok.android_spok.R;

public class Imp_Initialize_GUI_StudyCallSettingsActivity implements Initialize_GUI
{

	@Override
	public void init(final Activity activity)
	{
		ColorDrawable actionBarColor = new ColorDrawable(activity.getResources().getColor(R.color.studycall_actionbar));
		ActionBar actionBar = activity.getActionBar();
		actionBar.setBackgroundDrawable(actionBarColor);

		LinearLayout layout = (LinearLayout) activity.findViewById(R.id.settings_root_layout);
		layout.setBackgroundResource(R.drawable.studycall_gradient);

		ImageView cogwheel = (ImageView) activity.findViewById(R.id.settings_cogwheel);
		cogwheel.setImageResource(R.drawable.settings_cogwheel);

		final ListView settingsList = (ListView) activity.findViewById(R.id.settings_list);
		settingsList.setDivider(activity.getResources().getDrawable(R.drawable.settings_list_item_divider));
		settingsList.setDividerHeight(1);
		ArrayAdapter<String> settingsListAdapter = new ArrayAdapter<String>(activity, R.layout.settings_item, DataController.getInstance().getStudyCallSettings());
		settingsList.setAdapter(settingsListAdapter);
		settingsList.setOnItemClickListener(new AdapterView.OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
				String item = (String) settingsList.getItemAtPosition(position);
				SettingsController.getInstance().navigateToSetting(activity, item);
			}
		});
	}
}
