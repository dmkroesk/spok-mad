package Helpers.GUI.Message;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

public class DayNumberView extends View
{
	String daynumber;
	float width = 500;
	float height = 500;
	float radius = 0;
	float center_x, center_y;
	float degree = 0;
	int outerCirlceColor = Color.YELLOW;
	int circle;
	int text;
	int sizeOfWidth;

	public DayNumberView(Context context, String daynumber, int circle, int text, int size)
	{
		super(context);
		this.circle = circle;
		this.text = text;
		this.daynumber = daynumber;
		this.sizeOfWidth = size;
	}

	@Override
	protected void onDraw(Canvas canvas)
	{
		// TODO Auto-generated method stub
		super.onDraw(canvas);

		// set up the basic stuff we need for both circles
		width = (int) (sizeOfWidth / 2.4);
		height = (int) (sizeOfWidth / 2.4);
		radius = width / 4;
		center_x = width / 4 + 50;
		center_y = height / 4;

		drawInnerCircle(canvas);
		drawText(canvas);
	}

	private void drawInnerCircle(Canvas canvas)
	{
		Paint paint = new Paint();
		paint.setColor(circle);
		paint.setDither(true);
		paint.setAntiAlias(true);
		canvas.drawCircle(center_x, center_y, radius - 10, paint);
	}

	private void drawText(Canvas canvas)
	{
		Paint paint = new Paint();
		paint.setColor(text);
		paint.setDither(true);                    // set the dither to true
		paint.setAntiAlias(true);
		paint.setTextSize(80);
		canvas.drawText(daynumber + "", center_x - 50, center_y + 25, paint);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
	{
		// maximum width we should use

		setMeasuredDimension((int) width - 150, (int) height - 250);
	}

}
