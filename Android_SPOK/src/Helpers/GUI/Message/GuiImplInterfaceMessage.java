package Helpers.GUI.Message;

import java.util.List;
import Helpers.DataFetchers.Schedule.MessageList;
import android.support.v4.app.Fragment;

public interface GuiImplInterfaceMessage
{
	List<Fragment> getAppointmentFragements(MessageList list);

}
