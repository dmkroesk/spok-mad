package Helpers.GUI.Message;

import java.util.ArrayList;
import java.util.List;
import Helpers.DataFetchers.Schedule.MessageList;
import Helpers.GUI.Initialize_GUI;
import Helpers.GUI.Overview.BaseOverviewImp;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import com.spok.android_spok.MessagescreenActivity;
import com.spok.android_spok.R;

public class Imp_Initialize_AvansMessage extends BaseOverviewImp implements Initialize_GUI, GuiImplInterfaceMessage
{
	Context context;

	@Override
	public void init(Activity context)
	{
		// Header stuff
		this.context = context;

		ColorDrawable actionBarColor = new ColorDrawable(((Activity) context).getResources().getColor(R.color.avans_actionbar));
		ActionBar actionBar = ((Activity) context).getActionBar();
		actionBar.setBackgroundDrawable(actionBarColor);

		ImageView banner = (ImageView) ((Activity) context).findViewById(R.id.banner);
		banner.setVisibility(View.GONE);

		prev = (ImageButton) ((Activity) context).findViewById(R.id.Mprev);
		prev.setVisibility(View.INVISIBLE);
		prev.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{

				((MessagescreenActivity) Imp_Initialize_AvansMessage.this.context).changeAppointment("down");

			}
		});

		next = (ImageButton) ((Activity) context).findViewById(R.id.Mnext);
		next.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				((MessagescreenActivity) Imp_Initialize_AvansMessage.this.context).changeAppointment("up");
			}
		});

	}

	@Override
	public List<Fragment> getAppointmentFragements(MessageList list)
	{
		// changed cotains days which contains apppoinments
		int x = 0;
		ArrayList<Fragment> listDayFragments = new ArrayList<Fragment>();
		// // loop through days
		// for(SchedulePart d : changed.getChilderen())
		// {
		// for(SchedulePart a : d.getChilderen())
		// {
		// Bundle args = new Bundle();
		// args.putInt("day", x);
		// args.putSerializable("appointment", (Serializable) a.getPeriode());
		// args.putSerializable("part", (Serializable) d);
		// android.support.v4.app.Fragment dayFrag = new AvansMessage();
		// ((AvansMessage) dayFrag).setCircleColor(Color.GRAY);
		// ((AvansMessage) dayFrag).setTextColor(Color.WHITE);
		// dayFrag.setArguments(args);
		// listDayFragments.add(dayFrag);
		// }
		// x++;
		// }
		return listDayFragments;
	}

}
