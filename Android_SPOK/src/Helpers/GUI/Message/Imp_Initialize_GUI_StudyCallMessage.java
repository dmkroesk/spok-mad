package Helpers.GUI.Message;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import Helpers.DataFetchers.Schedule.MessageList;
import Helpers.GUI.Initialize_GUI;
import Helpers.GUI.Overview.BaseOverviewImp;
import Helpers.Message.Message;
import Helpers.Message.StudyCallMessage;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import com.spok.android_spok.MessagescreenActivity;
import com.spok.android_spok.R;

public class Imp_Initialize_GUI_StudyCallMessage extends BaseOverviewImp implements Initialize_GUI, GuiImplInterfaceMessage
{
	Context context;

	@Override
	public void init(Activity context)
	{
		// To replace hide header
		this.context = context;
		ColorDrawable actionBarColor = new ColorDrawable(((Activity) context).getResources().getColor(R.color.studycall_actionbar));
		ActionBar actionBar = ((Activity) context).getActionBar();
		actionBar.setBackgroundDrawable(actionBarColor);

		RelativeLayout bg = (RelativeLayout) ((Activity) context).findViewById(R.id.lowest);
		bg.setBackgroundResource(R.drawable.studycall_gradient_background);

		ImageView banner = (ImageView) ((Activity) context).findViewById(R.id.banner);
		banner.setScaleType(ScaleType.FIT_XY);

		prev = (ImageButton) ((Activity) context).findViewById(R.id.Mprev);
		prev.setImageResource(R.drawable.ic_back);
		prev.setVisibility(View.INVISIBLE);
		prev.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{

				((MessagescreenActivity) Imp_Initialize_GUI_StudyCallMessage.this.context).changeAppointment("down");

			}
		});

		next = (ImageButton) ((Activity) context).findViewById(R.id.Mnext);
		next.setImageResource(R.drawable.ic_forward);
		next.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				((MessagescreenActivity) Imp_Initialize_GUI_StudyCallMessage.this.context).changeAppointment("up");
			}
		});

	}

	@Override
	public List<Fragment> getAppointmentFragements(MessageList changed)
	{
		// changed cotains days which contains apppoinments
		int x = 0;
		ArrayList<Fragment> listDayFragments = new ArrayList<Fragment>();
		// loop through days
		// for(SchedulePart d : changed.getChilderen())
		// {
		// for(SchedulePart a : d.getChilderen())
		// {
		// Bundle args = new Bundle();
		// args.putInt("day", x);
		// args.putSerializable("appointment", (Serializable) a.getPeriode());
		// args.putSerializable("part", (Serializable) d);
		// android.support.v4.app.Fragment dayFrag = new StudyCallMessage();
		// ((StudyCallMessage) dayFrag).setCircleColor(Color.WHITE);
		// ((StudyCallMessage) dayFrag).setTextColor(Color.BLACK);
		// dayFrag.setArguments(args);
		// listDayFragments.add(dayFrag);
		// }
		// x++;
		// }
		for(Message m : changed.getMessages())
		{
			Bundle args = new Bundle();
			args.putInt("day", x);
			args.putSerializable("message", (Serializable) m);

			android.support.v4.app.Fragment dayFrag = new StudyCallMessage();
			((StudyCallMessage) dayFrag).setCircleColor(Color.WHITE);
			((StudyCallMessage) dayFrag).setTextColor(Color.BLACK);
			dayFrag.setArguments(args);
			listDayFragments.add(dayFrag);

			x++;
		}
		return listDayFragments;
	}

}
