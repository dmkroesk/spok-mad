package Helpers.GUI.Overview;

import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class BaseOverviewImp
{
	// UI parts
	ListView dayList;
	TextView title, weekView, dayView, appointmentView;
	public ImageButton next, prev;
}
