package Helpers.GUI.Overview;

import java.util.List;
import Model.ScheduleComposite.SchedulePart;
import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public interface GuiImpInterfaceOverView
{
	void setAppointmentHeader();

	void setDayHeader();

	void setWeekHeader();

	ArrayAdapter<?> getAdapter(Context context, List<SchedulePart> itemsArrayList, ListView lv);
}
