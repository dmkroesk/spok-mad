package Helpers.GUI.Overview;

import java.util.List;
import Helpers.GUI.Initialize_GUI;
import Helpers.Schedule.DayFragment.ScheduleDayAdapter;
import Model.ScheduleComposite.SchedulePart;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.spok.android_spok.OverviewActivity;
import com.spok.android_spok.R;

public class Imp_Initialize_AvansOverview extends BaseOverviewImp implements Initialize_GUI, GuiImpInterfaceOverView
{
	Context context;

	@Override
	public void init(Activity context)
	{
		// Header stuff
		this.context = context;
		dayView = (TextView) ((Activity) context).findViewById(R.id.dayView);
		dayView.setTypeface(null, Typeface.BOLD_ITALIC);
		appointmentView = (TextView) ((Activity) context).findViewById(R.id.appointmentView);
		weekView = (TextView) ((Activity) context).findViewById(R.id.weekView);

		ColorDrawable actionBarColor = new ColorDrawable(((Activity) context).getResources().getColor(R.color.avans_actionbar));
		ActionBar actionBar = ((Activity) context).getActionBar();
		actionBar.setBackgroundDrawable(actionBarColor);

		ImageView banner = (ImageView) ((Activity) context).findViewById(R.id.banner);
		banner.setVisibility(View.GONE);
		TextView clusterB = (TextView) ((Activity) context).findViewById(R.id.clusterbutton);
		clusterB.setVisibility(View.GONE);

		prev = (ImageButton) ((Activity) context).findViewById(R.id.prev);
		prev.setVisibility(View.INVISIBLE);
		prev.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{

				((OverviewActivity) Imp_Initialize_AvansOverview.this.context).currentSelection.changePart(((OverviewActivity) Imp_Initialize_AvansOverview.this.context), "down");

			}
		});

		next = (ImageButton) ((Activity) context).findViewById(R.id.next);
		next.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				((OverviewActivity) Imp_Initialize_AvansOverview.this.context).currentSelection.changePart(((OverviewActivity) Imp_Initialize_AvansOverview.this.context), "up");

			}
		});

		weekView.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				setWeekHeader();
				Context test = Imp_Initialize_AvansOverview.this.context;
				((OverviewActivity) Imp_Initialize_AvansOverview.this.context).weekClick();
			}
		});
		dayView.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				setDayHeader();
				((OverviewActivity) Imp_Initialize_AvansOverview.this.context).dayClick();
			}
		});
		appointmentView.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				setAppointmentHeader();
				((OverviewActivity) Imp_Initialize_AvansOverview.this.context).appointmentClick();
			}
		});

	}

	// header stuff
	public void setAppointmentHeader()
	{
		dayView.setTypeface(null, Typeface.NORMAL);
		weekView.setTypeface(null, Typeface.NORMAL);
		dayView.setBackgroundColor(Color.GRAY);
		weekView.setBackgroundColor(Color.GRAY);
		appointmentView.setTypeface(null, Typeface.BOLD_ITALIC);
		appointmentView.setBackgroundColor(context.getResources().getColor(R.color.avans_actionbar));
	}

	public void setDayHeader()
	{
		appointmentView.setTypeface(null, Typeface.NORMAL);
		weekView.setTypeface(null, Typeface.NORMAL);
		appointmentView.setBackgroundColor(Color.GRAY);
		weekView.setBackgroundColor(Color.GRAY);

		dayView.setTypeface(null, Typeface.BOLD_ITALIC);
		dayView.setBackgroundColor(context.getResources().getColor(R.color.avans_actionbar));
	}

	public void setWeekHeader()
	{
		dayView.setTypeface(null, Typeface.NORMAL);
		appointmentView.setTypeface(null, Typeface.NORMAL);
		dayView.setBackgroundColor(Color.GRAY);
		appointmentView.setBackgroundColor(Color.GRAY);

		weekView.setTypeface(null, Typeface.BOLD_ITALIC);
		weekView.setBackgroundColor(context.getResources().getColor(R.color.avans_actionbar));
	}

	@Override
	public ArrayAdapter<?> getAdapter(Context context, List<SchedulePart> itemsArrayList, ListView lv)
	{
		return new ScheduleDayAdapter(context, itemsArrayList, lv);
	}

}
