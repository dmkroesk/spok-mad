package Helpers.GUI.Overview;

import java.util.List;
import Helpers.GUI.Initialize_GUI;
import Helpers.Schedule.DayFragment.ScheduleStudyCallDayAdapter;
import Model.ScheduleComposite.SchedulePart;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.spok.android_spok.ClusterScheduleSettingsActivity;
import com.spok.android_spok.OverviewActivity;
import com.spok.android_spok.R;

public class Imp_Initialize_GUI_StudyCallOverview extends BaseOverviewImp implements Initialize_GUI, GuiImpInterfaceOverView
{
	Context context;

	@Override
	public void init(final Activity context)
	{
		// To replace hide header
		this.context = context;
		LinearLayout header = (LinearLayout) ((Activity) context).findViewById(R.id.header);
		header.setVisibility(View.GONE);

		TextView clusterButton = (TextView) ((Activity) context).findViewById(R.id.clusterbutton);

		RelativeLayout bg = (RelativeLayout) ((Activity) context).findViewById(R.id.lowest);
		bg.setBackgroundResource(R.drawable.studycall_gradient_background);

		ColorDrawable actionBarColor = new ColorDrawable(((Activity) context).getResources().getColor(R.color.studycall_actionbar));
		ActionBar actionBar = ((Activity) context).getActionBar();
		actionBar.setBackgroundDrawable(actionBarColor);

		ImageView banner = (ImageView) ((Activity) context).findViewById(R.id.banner);
		banner.setScaleType(ScaleType.FIT_XY);

		prev = (ImageButton) ((Activity) context).findViewById(R.id.prev);
		prev.setImageResource(R.drawable.ic_back);
		prev.setVisibility(View.INVISIBLE);
		prev.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{

				((OverviewActivity) Imp_Initialize_GUI_StudyCallOverview.this.context).currentSelection.changePart(((OverviewActivity) Imp_Initialize_GUI_StudyCallOverview.this.context), "down");

			}
		});

		next = (ImageButton) ((Activity) context).findViewById(R.id.next);
		next.setImageResource(R.drawable.ic_forward);
		next.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				((OverviewActivity) Imp_Initialize_GUI_StudyCallOverview.this.context).currentSelection.changePart(((OverviewActivity) Imp_Initialize_GUI_StudyCallOverview.this.context), "up");

			}
		});

		final Activity tempactivity = context;
		clusterButton.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				Intent myIntent = new Intent(tempactivity, ClusterScheduleSettingsActivity.class);
				// give along the day so that we can fill the cluster views with the data of the days.
				// make sure that the schedule is fetched !!
				if(((OverviewActivity) Imp_Initialize_GUI_StudyCallOverview.this.context).doneLoading)
				{
					myIntent.putExtra("CurrentSelectedDay", ((OverviewActivity) Imp_Initialize_GUI_StudyCallOverview.this.context).day);

					tempactivity.startActivityForResult(myIntent, OverviewActivity.CLUSTER_INTENT);
				}
				return;
			}
		});
	}

	@Override
	public void setAppointmentHeader()
	{
		// Do Nothing :D
	}

	@Override
	public void setDayHeader()
	{
		// Do Nothing :D
	}

	@Override
	public void setWeekHeader()
	{
		// Do Nothing :D
	}

	@Override
	public ArrayAdapter<?> getAdapter(Context context, List<SchedulePart> itemsArrayList, ListView lv)
	{
		return new ScheduleStudyCallDayAdapter(context, itemsArrayList, lv);
	}

}
