package Helpers.Message;

import Helpers.GUI.Message.DayNumberView;
import Model.ScheduleComposite.Periode;
import Model.ScheduleComposite.SchedulePart;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.spok.android_spok.MessagescreenActivity;
import com.spok.android_spok.R;

public class AvansMessage extends Fragment implements MessageInterface
{
	View rootView;
	TextView time, subject, dayAppointment;
	Periode periode;
	SchedulePart day;
	int dayNumber;
	int circleColor, textColor;
	Message message;

	public int getCirlceColor()
	{
		return circleColor;
	}

	public void setCircleColor(int circleColor)
	{
		this.circleColor = circleColor;
	}

	public int getTextColor()
	{
		return textColor;
	}

	public void setTextColor(int textColor)
	{
		this.textColor = textColor;
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		this.rootView = inflater.inflate(R.layout.avans_message_fragment, container, false);
		time = (TextView) this.rootView.findViewById(R.id.datum);
		subject = (TextView) this.rootView.findViewById(R.id.subject);
		dayAppointment = (TextView) this.rootView.findViewById(R.id.dayAppointment);

		periode = (Periode) getArguments().getSerializable("appointment");

		time.setText(periode.timeToString());
		subject.setText(periode.getSubject());

		LinearLayout topToDrawCirlce = (LinearLayout) this.rootView.findViewById(R.id.location);
		topToDrawCirlce.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{

				((MessagescreenActivity) rootView.getContext()).appointmentClick();

			}
		});
		// draw day number circel
		Display display = ((MessagescreenActivity) rootView.getContext()).getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int width = size.x;

		DayNumberView draw = new DayNumberView(rootView.getContext(), "12", circleColor, textColor, width);

		topToDrawCirlce.addView(draw, 0);

		setRetainInstance(true);
		// return inflater.inflate(R.layout.avans_message_fragment, container, false);
		return this.rootView;
	}

	@Override
	public SchedulePart getDay()
	{
		return day;
	}

	@Override
	public int getDaynumber()
	{

		return dayNumber;
	}

	@Override
	public Message getMessage()
	{
		return message;
	}

}
