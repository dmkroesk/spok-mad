package Helpers.Message;

import Model.ScheduleComposite.SchedulePart;

public interface MessageInterface
{
	SchedulePart getDay();

	int getDaynumber();

	Message getMessage();
}
