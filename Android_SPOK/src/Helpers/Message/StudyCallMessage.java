package Helpers.Message;

import java.text.SimpleDateFormat;
import Helpers.GUI.Message.DayNumberView;
import Model.ScheduleComposite.SchedulePart;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.spok.android_spok.MessagescreenActivity;
import com.spok.android_spok.R;

public class StudyCallMessage extends Fragment implements MessageInterface
{
	View rootView;
	TextView Datum, message, dayAppointment, monthAppointment;
	Message messageObject;
	SchedulePart day;
	int dayNumber;
	int circleColor, textColor;

	public int getCirlceColor()
	{
		return circleColor;
	}

	public void setCircleColor(int circleColor)
	{
		this.circleColor = circleColor;
	}

	public void setTextColor(int color)
	{
		this.textColor = color;
	}

	public int getTextColor()
	{
		return textColor;
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		this.rootView = inflater.inflate(R.layout.studycall_message_fragment, container, false);
		Datum = (TextView) this.rootView.findViewById(R.id.datumStudyCall);
		message = (TextView) this.rootView.findViewById(R.id.messageStudyCall);
		dayAppointment = (TextView) this.rootView.findViewById(R.id.dayAppointment);
		monthAppointment = (TextView) this.rootView.findViewById(R.id.monthAppointment);
		messageObject = (Message) getArguments().getSerializable("message");

		// dayAppointment.setText(day.getPeriode().getSubject());
		Datum.setText(new SimpleDateFormat("EEE MMM dd-MM-yyyy HH:mm").format(messageObject.getDate()));
		message.setText(messageObject.getMessage());
		dayAppointment.setText(new SimpleDateFormat("EEEE dd").format(messageObject.getDate()));
		monthAppointment.setText(new SimpleDateFormat("MMMM").format(messageObject.getDate()));
		LinearLayout topToDrawCirlce = (LinearLayout) this.rootView.findViewById(R.id.location);
		topToDrawCirlce.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// ((MessagescreenActivity) rootView.getContext()).appointmentClick();

			}
		});

		// draw day number circel
		Display display = ((MessagescreenActivity) rootView.getContext()).getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int width = size.x;
		DayNumberView draw = new DayNumberView(rootView.getContext(), new SimpleDateFormat("dd").format(messageObject.getDate()), circleColor, textColor, width);

		topToDrawCirlce.addView(draw, 0);

		setRetainInstance(true);
		return this.rootView;
	}

	@Override
	public SchedulePart getDay()
	{
		return day;
	}

	@Override
	public int getDaynumber()
	{

		return dayNumber;
	}

	@Override
	public Message getMessage()
	{
		// TODO Auto-generated method stub
		return null;
	}

}
