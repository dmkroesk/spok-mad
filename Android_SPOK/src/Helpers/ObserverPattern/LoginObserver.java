package Helpers.ObserverPattern;

public interface LoginObserver
{
	void update(boolean status);
}
