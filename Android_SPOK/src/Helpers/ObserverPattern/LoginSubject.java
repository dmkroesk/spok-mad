package Helpers.ObserverPattern;

public interface LoginSubject
{

	public void registerObserver(LoginObserver observer);

    public void removeObserver(LoginObserver observer);

    public void notifyObservers();
	
}
