package Helpers.ObserverPattern;

public interface Observer {	 
	void update(Subject subject);
}