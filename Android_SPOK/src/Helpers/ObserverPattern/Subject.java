package Helpers.ObserverPattern;

import java.util.ArrayList;
import java.util.List;
// Steps to use
// 1) have an instance of this class
// 2) when creating a view that wants to listen to this instance => addListener
// 3) when this data changes => notifyAllObservers

public class Subject
{
	private List<Observer> observers = new ArrayList<Observer>();

	public void addListener(Observer observer)
	{
		observers.add(observer);
	}

	public void removeListener(Observer observer)
	{
		observers.remove(observer);
	}

	public void notifyAllObservers()
	{
		for(Observer observer : observers)
		{
			observer.update(this);
		}
	}
}
