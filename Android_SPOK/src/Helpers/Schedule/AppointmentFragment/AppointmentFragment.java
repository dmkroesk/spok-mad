package Helpers.Schedule.AppointmentFragment;

import java.text.SimpleDateFormat;
import Controller.SettingsController;
import Helpers.GUI.Factory_GUI;
import Helpers.GUI.Imp_Factory_GUI;
import Helpers.GUI.Initialize_GUI;
import Helpers.GUI.Fragments.AppoinmentInterface;
import Model.ScheduleComposite.Periode;
import Model.ScheduleComposite.SchedulePart;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.spok.android_spok.R;

public class AppointmentFragment extends Fragment
{
	View rootView;
	TextView time, subject, dayAppointment, location;
	Periode periode;
	SchedulePart day;
	int dayNumber;

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		this.rootView = inflater.inflate(R.layout.appointment, container, false);
		time = (TextView) this.rootView.findViewById(R.id.datum);
		subject = (TextView) this.rootView.findViewById(R.id.subject);
		dayAppointment = (TextView) this.rootView.findViewById(R.id.dayAppointment);
		location = (TextView) this.rootView.findViewById(R.id.location);
		periode = (Periode) getArguments().getSerializable("appointment");

		day = (SchedulePart) getArguments().getSerializable("part");
		dayNumber = getArguments().getInt("day");
		location.setText(periode.getLocation());
		dayAppointment.setText(new SimpleDateFormat("EEE MMM dd-MM-yyyy").format(day.getPeriode().getStart()));
		time.setText(periode.timeToString());
		subject.setText(periode.getSubject());
		// draw timer

		// specific color for school
		SettingsController scon = SettingsController.getInstance();
		String type = "";
		if(scon.getSchool().equals("Avans"))
			type = "AvansAppointmentFragment";
		else
			type = "StudyCallAppointmentFragment";

		Factory_GUI factory = Imp_Factory_GUI.getInstance();
		Initialize_GUI guiinit = factory.create(type);
		guiinit.init(((Activity) this.rootView.getContext()));

		int circleColor = ((AppoinmentInterface) guiinit).getCirleColor();
		int textcolor = ((AppoinmentInterface) guiinit).getTextColor();

		TimerView draw = new TimerView(this.rootView.getContext(), periode, circleColor, textcolor);

		LinearLayout lay = (LinearLayout) this.rootView.findViewById(R.id.drawn_cirlceCenter);

		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
		params.gravity = Gravity.CENTER_HORIZONTAL;
		// params.weight = 1;
		((AppoinmentInterface) guiinit).preformChanges(this.rootView);

		lay.addView(draw, params);

		setRetainInstance(true);
		return this.rootView;
	}

}
