package Helpers.Schedule.AppointmentFragment;

import java.util.Date;
import Model.ScheduleComposite.Periode;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.view.View;

public class TimerView extends View
{
	Periode periode;
	float width = 500;
	float height = 500;
	float radius = 0;
	float center_x, center_y;
	float degree = 0;
	int outerCirlceColor = Color.YELLOW;
	int circleColor, textcolor;
	Context con;

	public TimerView(Context context, Periode periode, int circleColor, int textcolor)
	{
		super(context);
		this.con = context;
		this.periode = periode;
		this.circleColor = circleColor;
		this.textcolor = textcolor;
		preCalculate();

	}

	@Override
	protected void onDraw(Canvas canvas)
	{
		// TODO Auto-generated method stub
		super.onDraw(canvas);

		// set up the basic stuff we need for both circles
		radius = width / 4;
		center_x = width / 4 + 50;
		center_y = height / 4 + 50;

		final RectF oval = new RectF();
		oval.set(center_x - radius, center_y - radius, center_x + radius, center_y + radius);

		drawOuterCircle(canvas, oval);
		drawInnerCircle(canvas);
		drawText(canvas);
	}

	public static final long DAY = 3600 * 1000 * 24; // in milli-seconds.
	public static final long HOUR = 3600 * 1000;

	private void preCalculate()
	{

		// check date ->
		// if > 24 don't draw outercircle
		// if <24 draw a circle ( calculate the degree)
		// set color for the circle
		// yellow for timer
		// blue time periode
		// green full cirlce if fully passed

		Date now = new Date();

		if(now.getTime() < periode.getStart().getTime())
		{
			// first - go to real monitor point 24 hours
			float window = periode.getStart().getTime() - DAY;
			// then get difference between now and window var
			float diff = now.getTime() - window;

			if(diff < 0)
			{
				// we are not drawing if not between the 24 window
				degree = 0;
				return;
			}
			// then divide by day * 100%
			float perc = (diff / DAY) * 100;
			// make degree -> *10 cause it didn't accept 3.6 -_-"
			degree = (perc * 36) / 10;

		}
		else if(now.getTime() > periode.getEnd().getTime())
		{
			outerCirlceColor = Color.GREEN;
			degree = 360;
		}
		else
		{
			// with in the time window start - end
			float timePassed = now.getTime() - periode.getStart().getTime();
			float totalDuration = periode.getEnd().getTime() - periode.getStart().getTime();
			float perc = (timePassed / totalDuration) * 100;
			degree = (perc * 36) / 10;
			outerCirlceColor = Color.BLUE;
		}

	}

	private void drawOuterCircle(Canvas canvas, RectF oval)
	{

		Paint paint = new Paint();

		paint.setColor(outerCirlceColor);
		paint.setStrokeWidth(5);
		paint.setStyle(Paint.Style.STROKE);

		paint.setDither(true);                    // set the dither to true
		paint.setStyle(Paint.Style.STROKE);       // set to STOKE
		paint.setStrokeJoin(Paint.Join.ROUND);    // set the join to round you want
		paint.setStrokeCap(Paint.Cap.ROUND);      // set the paint cap to round too
		paint.setPathEffect(new CornerPathEffect(10));   // set the path effect when they join.
		paint.setAntiAlias(true);

		Path path = new Path();
		// replace these two values to create the timer
		path.addArc(oval, -90, degree);

		canvas.drawPath(path, paint);

	}

	private void drawInnerCircle(Canvas canvas)
	{
		Paint paint = new Paint();
		paint.setColor(circleColor);
		paint.setDither(true);
		paint.setAntiAlias(true);
		canvas.drawCircle(center_x, center_y, radius - 10, paint);
	}

	private void drawText(Canvas canvas)
	{
		Paint paint = new Paint();
		paint.setColor(textcolor);
		paint.setDither(true);                    // set the dither to true
		paint.setAntiAlias(true);
		paint.setTextSize(40);
		canvas.drawText(periode.startToString(), center_x - 50, center_y, paint);
	}
}
