package Helpers.Schedule.DayFragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import Controller.SettingsController;
import Helpers.ActivitySwipeDetector;
import Helpers.GUI.Factory_GUI;
import Helpers.GUI.Imp_Factory_GUI;
import Helpers.GUI.Initialize_GUI;
import Helpers.GUI.Fragments.DayInterface;
import Model.ScheduleComposite.SchedulePart;
import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.spok.android_spok.OverviewActivity;
import com.spok.android_spok.R;

public class DayChangeFragement extends android.support.v4.app.ListFragment
{
	public View rootView;
	TextView title;
	ListView lv;
	SchedulePart week;
	ArrayAdapter<?> adapter;
	int day = 0;

	ActivitySwipeDetector listener;

	List<SchedulePart> dataList;
	boolean dataIsChanged;

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		this.rootView = inflater.inflate(R.layout.day_fragment_schedule, container, false);
		title = (TextView) this.rootView.findViewById(R.id.title);
		lv = (ListView) this.rootView.findViewById(android.R.id.list);

		lv.setItemsCanFocus(false);
		lv.setOnTouchListener(listener);
		day = getArguments().getInt("day");
		week = (SchedulePart) getArguments().getSerializable("week");

		LinearLayout lay = (LinearLayout) this.rootView.findViewById(R.id.transparentrow);

		// set gui
		SettingsController scon = SettingsController.getInstance();
		String type = "";
		if(scon.getSchool().equals("Avans"))
			type = "AvansDayFragment";
		else
			type = "StudyCallDayFragment";

		Factory_GUI factory = Imp_Factory_GUI.getInstance();
		Initialize_GUI guiinit = factory.create(type);
		guiinit.init(((Activity) this.rootView.getContext()));
		((DayInterface) guiinit).preformChanges(this.rootView);

		populateDayFragement();
		setRetainInstance(true);
		return this.rootView;
	}

	public void setListener(ActivitySwipeDetector listener)
	{
		this.listener = listener;
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id)
	{
		super.onListItemClick(l, v, position, id);
		// v.setSelected(true);
		// lv.setItemChecked(position, true);
		OverviewActivity activity = (OverviewActivity) getActivity();
		// first give day and then the schedule to display
		activity.changeToAppointmentView(week.getChild(day), position);
	}

	private void populateDayFragement()
	{
		// set title
		title.setText(new SimpleDateFormat("EEE MMM dd-MM-yyyy").format(week.getChild(day).getPeriode().getStart()));
		// set the adapter through the GUI impl
		dataList = week.getChild(day).getChilderen();
		if(dataIsChanged)
			refreshDataList();

		adapter = ((OverviewActivity) rootView.getContext()).getDayAdapter(dataList, lv);

		// new ScheduleDayAdapter(rootView.getContext(), week.getChild(day).getChilderen());
		setListAdapter(adapter);
	}

	public void refreshDataList()
	{
		List<SchedulePart> fullList = week.getChild(day).getChilderen();
		dataList = new ArrayList<SchedulePart>();
		for(SchedulePart sp : fullList)
		{
			if(sp.getPeriode().getCluster().isDisplay())
			{
				dataList.add(sp);
			}
		}
	}

	public void setDataIsChanged(boolean isChanged)
	{

		dataIsChanged = isChanged;
	}

	public void refresh()
	{
		refreshDataList();
		adapter = ((OverviewActivity) rootView.getContext()).getDayAdapter(dataList, lv);
		// new ScheduleDayAdapter(rootView.getContext(), week.getChild(day).getChilderen());
		setListAdapter(adapter);
		adapter.notifyDataSetChanged();
		lv.invalidate();
	}
}
