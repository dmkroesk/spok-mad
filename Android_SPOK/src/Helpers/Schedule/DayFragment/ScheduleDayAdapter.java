package Helpers.Schedule.DayFragment;

import java.util.List;
import Model.ScheduleComposite.SchedulePart;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.spok.android_spok.R;

public class ScheduleDayAdapter extends ArrayAdapter<SchedulePart>
{

	private final Context context;
	private final List<SchedulePart> itemsArrayList;

	public ScheduleDayAdapter(Context context, List<SchedulePart> itemsArrayList, ListView lv)
	{
		super(context, R.layout.day_listrow_schedule, itemsArrayList);

		this.context = context;
		this.itemsArrayList = itemsArrayList;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{

		// 1. Create inflater
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		// 2. Get rowView from inflater
		View rowView = inflater.inflate(R.layout.day_listrow_schedule, parent, false);

		// 3. Get the two text view from the rowView
		TextView labelView = (TextView) rowView.findViewById(R.id.Time);
		TextView valueView = (TextView) rowView.findViewById(R.id.Subject);

		// 4. Set the text for textView
		labelView.setText(itemsArrayList.get(position).getPeriode().timeToString());
		valueView.setText(itemsArrayList.get(position).getPeriode().getSubject());

		// 5. retrn rowView
		return rowView;
	}
}