package Helpers.Schedule.DayFragment;

import java.util.List;
import Model.ScheduleComposite.Cluster;
import Model.ScheduleComposite.Periode;
import Model.ScheduleComposite.SchedulePart;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.spok.android_spok.R;

public class ScheduleStudyCallDayAdapter extends ArrayAdapter<SchedulePart>
{

	private final Context context;
	private List<SchedulePart> itemsArrayList;

	public ScheduleStudyCallDayAdapter(Context context, List<SchedulePart> itemsArrayList, ListView lv)
	{
		super(context, R.layout.day_studycall_listrow, itemsArrayList);

		lv.setDividerHeight(0);

		this.context = context;
		this.itemsArrayList = itemsArrayList;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{

		// 1. Create inflater
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		// 2. Get rowView from inflater
		View rowView = inflater.inflate(R.layout.day_studycall_listrow, parent, false);

		// 3. Get the two text view from the rowView
		TextView labelView = (TextView) rowView.findViewById(R.id.Time);
		TextView valueView = (TextView) rowView.findViewById(R.id.Subject);
		TextView studyHourView = (TextView) rowView.findViewById(R.id.subjectID);
		TextView locationView = (TextView) rowView.findViewById(R.id.location);

		// 4. Set the text for textView
		labelView.setText(itemsArrayList.get(position).getPeriode().timeToString());
		valueView.setText(itemsArrayList.get(position).getPeriode().getSubject());
		studyHourView.setText(String.valueOf(itemsArrayList.get(position).getPeriode().getStudyHour()));
		locationView.setText(itemsArrayList.get(position).getPeriode().getLocation());
		Periode periode = itemsArrayList.get(position).getPeriode();
		View clusterCLine = (View) rowView.findViewById(R.id.clusterLine);
		View clusterBLine = (View) rowView.findViewById(R.id.bottomHr);

		if(periode.getCluster() != null)
		{
			Cluster currentCluster = periode.getCluster();
			clusterCLine.setBackgroundColor(currentCluster.getColorOfCluster());
			// check if next periode is same cluster
			if(itemsArrayList.size() > (position + 1))
			{
				Periode nextPeriode = itemsArrayList.get(position + 1).getPeriode();
				if(nextPeriode.getCluster() != null)
				{
					if(currentCluster.getClusterName().equals(nextPeriode.getCluster().getClusterName()))
					{
						clusterBLine.setVisibility(View.INVISIBLE);
					}
				}
			}
		}
		else
		{

			clusterCLine.setVisibility(View.INVISIBLE);
			clusterBLine.setVisibility(View.INVISIBLE);
		}

		// 5. retrn rowView
		// hide the rows for the clusters that have been turned off
		// if(!periode.getCluster().isDisplay())
		// {
		// return;
		// }
		return rowView;
	}
}