package Helpers.Schedule.FragmentInterface;

import java.io.Serializable;
import java.util.ArrayList;
import Helpers.ActivitySwipeDetector;
import Helpers.Schedule.AppointmentFragment.AppointmentFragment;
import Model.ScheduleComposite.SchedulePart;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.spok.android_spok.OverviewActivity;

public class AppointmentImplem implements ScheduleFragmentInterface
{
	SchedulePart part;

	@Override
	public void setSchedulePart(SchedulePart part)
	{
		this.part = part;
	}

	@Override
	public ArrayList<Fragment> getFragements(ActivitySwipeDetector listener)
	{
		int x = 0;
		ArrayList<Fragment> listAppointmentsFragments = new ArrayList<Fragment>();
		for(SchedulePart p : part.getChilderen())
		{
			Bundle args = new Bundle();
			args.putSerializable("appointment", (Serializable) p.getPeriode());
			args.putSerializable("part", (Serializable) part);
			args.putInt("day", x);
			android.support.v4.app.Fragment appointmentFrag = new AppointmentFragment();
			appointmentFrag.setArguments(args);
			listAppointmentsFragments.add(appointmentFrag);
			x++;
		}
		return listAppointmentsFragments;
	}

	@Override
	public void changePart(OverviewActivity activity, String type)
	{
		if(type.equals("up"))
			activity.appointment++;
		else
			activity.appointment--;

		activity.changeAppointment(type);
	}

	@Override
	public boolean canGoBackFromStack()
	{
		// TODO Auto-generated method stub
		return true;
	}

}
