package Helpers.Schedule.FragmentInterface;

import java.io.Serializable;
import java.util.ArrayList;
import Helpers.ActivitySwipeDetector;
import Helpers.Schedule.DayFragment.DayChangeFragement;
import Model.ScheduleComposite.SchedulePart;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.spok.android_spok.OverviewActivity;

public class DayImplem implements ScheduleFragmentInterface
{
	SchedulePart part;

	@Override
	public void setSchedulePart(SchedulePart part)
	{
		this.part = part;
	}

	@Override
	public ArrayList<Fragment> getFragements(ActivitySwipeDetector listener)
	{
		int x = 0;
		ArrayList<Fragment> listDayFragments = new ArrayList<Fragment>();
		for(SchedulePart p : part.getChilderen())
		{
			Bundle args = new Bundle();
			args.putInt("day", x);
			args.putSerializable("week", (Serializable) part);

			// android.support.v4.app.Fragment dayFrag = new DayChangeFragement();
			DayChangeFragement dayFrag = new DayChangeFragement();
			dayFrag.setListener(listener);
			dayFrag.setArguments(args);
			listDayFragments.add(dayFrag);
			x++;
		}
		return listDayFragments;
	}

	@Override
	public void changePart(OverviewActivity activity, String type)
	{
		if(type.equals("up"))
			activity.day++;
		else
			activity.day--;

		activity.changeDay(type);
	}

	@Override
	public boolean canGoBackFromStack()
	{
		// TODO Auto-generated method stub
		return false;
	}

}
