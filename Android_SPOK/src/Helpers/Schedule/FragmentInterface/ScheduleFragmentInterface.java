package Helpers.Schedule.FragmentInterface;

import java.util.ArrayList;
import Helpers.ActivitySwipeDetector;
import Model.ScheduleComposite.SchedulePart;
import android.support.v4.app.Fragment;
import com.spok.android_spok.OverviewActivity;

public interface ScheduleFragmentInterface
{
	void setSchedulePart(SchedulePart part);

	void changePart(OverviewActivity activity, String type);

	ArrayList<Fragment> getFragements(ActivitySwipeDetector listener);

	// needed for the default back
	boolean canGoBackFromStack();
}
