package Helpers.Schedule.FragmentInterface;

import java.io.Serializable;
import java.util.ArrayList;
import Helpers.ActivitySwipeDetector;
import Helpers.Schedule.WeekFragment.WeekFragment;
import Model.ScheduleComposite.SchedulePart;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.spok.android_spok.OverviewActivity;

public class WeekImplem implements ScheduleFragmentInterface
{
	SchedulePart part;

	@Override
	public void setSchedulePart(SchedulePart part)
	{
		this.part = part;
	}

	@Override
	public void changePart(OverviewActivity activity, String type)
	{
		// we want the user to swipe right and proceed to the day view
		if(type.equals("up"))
			activity.changeToDayView(part.getChild(0), 0);
	}

	@Override
	public ArrayList<Fragment> getFragements(ActivitySwipeDetector listener)
	{
		// create single fragment for now, cause we dont know if we will expand this
		ArrayList<Fragment> listWeekFragments = new ArrayList<Fragment>();

		Bundle args = new Bundle();
		args.putSerializable("week", (Serializable) part);

		android.support.v4.app.Fragment weekFrag = new WeekFragment();
		weekFrag.setArguments(args);
		listWeekFragments.add(weekFrag);

		return listWeekFragments;
	}

	@Override
	public boolean canGoBackFromStack()
	{
		// TODO Auto-generated method stub
		return false;
	}

}
