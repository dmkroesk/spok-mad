package Helpers.Schedule.WeekFragment;

import java.util.List;
import Model.ScheduleComposite.SchedulePart;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.spok.android_spok.R;

public class ScheduleWeekAdapter extends ArrayAdapter<SchedulePart>
{
	private final Context context;
	private final WeekFragment frag;
	private final List<SchedulePart> itemsArrayList;

	public ScheduleWeekAdapter(Context context, List<SchedulePart> itemsArrayList, WeekFragment frag)
	{
		super(context, R.layout.week_listrow_schedule, itemsArrayList);
		this.frag = frag;
		this.context = context;
		this.itemsArrayList = itemsArrayList;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		// 1. Create inflater
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		// 2. Get rowView from inflater
		View rowView = inflater.inflate(R.layout.week_listrow_schedule, parent, false);
		rowView.setTag(position);
		// 3. Get the two text view from the rowView
		TextView labelView = (TextView) rowView.findViewById(R.id.Message);
		TextView numberOfApp = (TextView) rowView.findViewById(R.id.numberOfApp);

		rowView.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				// get the row the clicked button is in
				int position = (Integer) v.getTag();
				ScheduleWeekAdapter.this.frag.onRowClick(position);
			}
		});
		/* ImageButton notification = (ImageButton) rowView.findViewById(R.id.notificationButton);
		 * notification.setOnClickListener(new OnClickListener()
		 * {
		 * @Override
		 * public void onClick(View v)
		 * {
		 * // get the row the clicked button is in
		 * LinearLayout vwParentRow = (LinearLayout) v.getParent();
		 * TextView child = (TextView) vwParentRow.getChildAt(1);
		 * }
		 * }); */
		// 4. Set the text for textView
		labelView.setText(itemsArrayList.get(position).getPeriode().getSubject());
		numberOfApp.setText("" + itemsArrayList.get(position).getChilderen().size());
		// 5. retrn rowView
		return rowView;
	}
}