package Helpers.Schedule.WeekFragment;

import Model.ScheduleComposite.SchedulePart;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import com.spok.android_spok.OverviewActivity;
import com.spok.android_spok.R;

public class WeekFragment extends android.support.v4.app.ListFragment
{
	public View rootView;
	TextView title;
	ListView lv;
	SchedulePart week;
	ScheduleWeekAdapter adapter;

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		this.rootView = inflater.inflate(R.layout.week_fragment_schedule, container, false);
		title = (TextView) this.rootView.findViewById(R.id.title);
		lv = (ListView) this.rootView.findViewById(android.R.id.list);

		lv.setItemsCanFocus(false);

		week = (SchedulePart) getArguments().getSerializable("week");

		populateWeekFragement();
		setRetainInstance(true);
		return this.rootView;
	}

	public void onRowClick(int position)
	{
		OverviewActivity activity = (OverviewActivity) getActivity();
		activity.changeToDayView(week.getChild(position), position);
	}

	private void populateWeekFragement()
	{
		// set title
		title.setText("Temp Week");
		// set the adapter
		adapter = new ScheduleWeekAdapter(rootView.getContext(), week.getChilderen(), this);
		setListAdapter(adapter);
	}
}
