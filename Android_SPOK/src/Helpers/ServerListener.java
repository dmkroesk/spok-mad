package Helpers;

import Controller.DataController;

public class ServerListener {
	// this class will listen for pushing from server
	// when ever this class recieves a messages => display
	// tell Datacontroller to fetch data from server
	
	public ServerListener(){		
	
	}
	
	public void Listen(){		
		DisplayNotificationMessage();
		GetUpdatedData();
	}
	private void DisplayNotificationMessage(){
		
	}
	private void GetUpdatedData(){
		DataController controller = DataController.getInstance();
		controller.GetDataFromServer();
	
	}
	
}
