package Helpers;

public interface Swipe
{
	public void onRightToLeftSwipe();

	public void onLeftToRightSwipe();

	public void onTopToBottomSwipe();

	public void onBottomToTopSwipe();
}
