package Model;

import Helpers.ObserverPattern.Subject;

public class Ad extends Subject
{
	private String header;
	private String content;
	private String hyperlink;
	private boolean selected;

	public Ad()
	{
		header = "";
		content = "";
		hyperlink = "";
		selected = false;
	}

	public Ad(String head, String cont, String link, boolean select)
	{
		header = head;
		content = cont;
		hyperlink = link;
		selected = select;
	}

	public String getHeader()
	{
		return header;
	}

	public void setHeader(String header)
	{
		this.header = header;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public String getHyperlink()
	{
		return hyperlink;
	}

	public void setHyperlink(String hyperlink)
	{
		this.hyperlink = hyperlink;
	}

	public boolean isSelected()
	{
		return selected;
	}

	public void setSelected(boolean selected)
	{
		this.selected = selected;
	}

	public void replaceSpaces()
	{
		header = header.replace(" ", "%20");
		content = content.replace(" ", "%20");
		hyperlink = hyperlink.replace(" ", "%20");
	}

}
