package Model;

import java.util.ArrayList;
import Helpers.ObserverPattern.Subject;

public class AdSubject extends Subject
{
	private ArrayList<Ad> ads;

	public AdSubject()
	{
		ads = new ArrayList<Ad>();
	}

	public void add(Ad ad)
	{
		ads.add(ad);
	}

	public void remove(Ad ad)
	{
		ads.remove(ad);
	}

	public ArrayList<Ad> getAds()
	{
		return ads;
	}

	public void setAds(ArrayList<Ad> ads)
	{
		this.ads = ads;
	}
}
