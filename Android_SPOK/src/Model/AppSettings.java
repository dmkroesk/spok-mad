package Model;

public class AppSettings {
	
	private String regid;
	private String appversion;
	
	public String getRegid()
	{
		return regid;
	}
	public void setRegid(String regid)
	{
		this.regid = regid;
	}
	public String getAppversion()
	{
		return appversion;
	}
	public void setAppversion(String appversion)
	{
		this.appversion = appversion;
	}
	
}
