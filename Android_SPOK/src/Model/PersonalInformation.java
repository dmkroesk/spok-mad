package Model;

import Helpers.ObserverPattern.Subject;

public class PersonalInformation extends Subject
{
	private int studentNumber;
	private String password;
	private String firstName;
	private String lastName;
	private String insertion;
	private String email;
	private String birthDate;
	private String school;

	public PersonalInformation()
	{
		studentNumber = 112;
		password = "test";
		firstName = "Jane";
		lastName = "Doe";
		insertion = "van";
		email = "janedoe@gmail.com";
		birthDate = "2013-5-13";
		school = "Huibeven";
	}

	public PersonalInformation(String given, String surname, String insert, String mail, String date, String school)
	{
		studentNumber = 112;
		password = "test";
		firstName = given;
		lastName = surname;
		insertion = insert;
		email = mail;
		birthDate = date;
		this.school = school;
	}

	public PersonalInformation(int studentNo, String pass, String given, String surname, String insert, String mail, String date, String school)
	{
		studentNumber = studentNo;
		password = pass;
		firstName = given;
		lastName = surname;
		insertion = insert;
		email = mail;
		birthDate = date;
		this.school = school;
	}

	public int getStudentNumber()
	{
		return studentNumber;
	}

	public void setStudentNumber(int studentNumber)
	{
		this.studentNumber = studentNumber;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public String getInsertion()
	{
		return insertion;
	}

	public void setInsertion(String insertion)
	{
		this.insertion = insertion;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getBirthDate()
	{
		return birthDate;
	}

	public void setBirthDate(String birthDate)
	{
		this.birthDate = birthDate;
	}

	public String getSchool()
	{
		return school;
	}

	public void setSchool(String school)
	{
		this.school = school;
	}
}
