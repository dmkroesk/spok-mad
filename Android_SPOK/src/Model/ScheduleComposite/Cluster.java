package Model.ScheduleComposite;

import java.io.Serializable;

public class Cluster implements Serializable
{

	private static final long serialVersionUID = 1L;
	String clusterName;
	int colorOfCluster;

	public boolean isDisplay()
	{
		return display;
	}

	public void setDisplay(boolean display)
	{
		this.display = display;
	}

	boolean display = true;

	public String getClusterName()
	{
		return clusterName;
	}

	public void setClusterName(String clusterName)
	{
		this.clusterName = clusterName;
	}

	public int getColorOfCluster()
	{
		return colorOfCluster;
	}

	public void setColorOfCluster(int colorOfCluster)
	{
		this.colorOfCluster = colorOfCluster;
	}
}
