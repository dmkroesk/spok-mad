package Model.ScheduleComposite;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Periode implements Serializable
{
	// shared props
	Date start;
	Date end;
	String subject;
	// appointment specific
	String teacher;
	String location;
	int studyHour;

	public String getTeacher()
	{
		return teacher;
	}

	public void setTeacher(String teacher)
	{
		this.teacher = teacher;
	}

	public String getLocation()
	{
		return location;
	}

	public void setLocation(String location)
	{
		this.location = location;
	}

	public int getStudyHour()
	{
		return studyHour;
	}

	public void setStudyHour(int studyHour)
	{
		this.studyHour = studyHour;
	}

	Cluster cluster;

	public Cluster getCluster()
	{
		return cluster;
	}

	public void setCluster(Cluster cluster)
	{
		this.cluster = cluster;
	}

	public Periode()
	{

	}

	public Date getStart()
	{
		return start;
	}

	public void setStart(Date start)
	{
		this.start = start;
	}

	public Date getEnd()
	{
		return end;
	}

	public void setEnd(Date end)
	{
		this.end = end;
	}

	public String getSubject()
	{
		return subject;
	}

	public void setSubject(String subject)
	{
		this.subject = subject;
	}

	public String timeToString()
	{
		return new SimpleDateFormat("HH:mm").format(start) + "-" + new SimpleDateFormat("HH:mm").format(end);
	}

	public String startToString()
	{

		return new SimpleDateFormat("HH:mm").format(start);
	}

	public String endToString()
	{
		return new SimpleDateFormat("HH:mm").format(end);
	}

	public String toString()
	{
		return start + "-" + end + " " + subject;
	}

}
