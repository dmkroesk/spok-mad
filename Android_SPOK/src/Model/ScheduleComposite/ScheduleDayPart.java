package Model.ScheduleComposite;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ScheduleDayPart implements SchedulePart, Serializable
{
	Periode periode;
	List<SchedulePart> childeren = new ArrayList<SchedulePart>();

	@Override
	public void add(SchedulePart schedulePart)
	{
		childeren.add(schedulePart);
	}

	@Override
	public void remove(SchedulePart schedulePart)
	{
		childeren.remove(schedulePart);

	}

	@Override
	public SchedulePart getChild(int i)
	{
		return childeren.get(i);
	}

	@Override
	public Periode getPeriode()
	{
		return periode;
	}

	public List<SchedulePart> getChilderen()
	{
		return childeren;
	}

	@Override
	public void setPeriode(Periode periode)
	{
		this.periode = periode;

	}

	@Override
	public String toString()
	{
		return periode.toString();
	}

	public List<SchedulePart> getAppointmentsForCluster(String clusterName)
	{
		ArrayList<SchedulePart> clusterList = new ArrayList<SchedulePart>();
		for(SchedulePart app : this.childeren)
		{
			if(((SchedulePeriodePart) app).isInCluster(clusterName))
				clusterList.add(app);
		}
		return clusterList;
	}
}
