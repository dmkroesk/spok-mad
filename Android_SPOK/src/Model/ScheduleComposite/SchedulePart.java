package Model.ScheduleComposite;

import java.util.List;

public interface SchedulePart
{
	public void add(SchedulePart schedulePart);

	public void remove(SchedulePart schedulePart);

	public SchedulePart getChild(int i);

	public List<SchedulePart> getChilderen();

	// periode
	public Periode getPeriode();

	public void setPeriode(Periode periode);

	public String toString();
}
