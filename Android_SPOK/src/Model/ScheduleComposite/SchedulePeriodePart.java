package Model.ScheduleComposite;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SchedulePeriodePart implements SchedulePart, Serializable
{
	Periode periode;
	List<SchedulePart> childeren = new ArrayList<SchedulePart>();

	@Override
	public void add(SchedulePart schedulePart)
	{
		childeren.add(schedulePart);
	}

	@Override
	public void remove(SchedulePart schedulePart)
	{
		childeren.remove(schedulePart);

	}

	@Override
	public SchedulePart getChild(int i)
	{
		return childeren.get(i);
	}

	@Override
	public Periode getPeriode()
	{
		return periode;
	}

	public List<SchedulePart> getChilderen()
	{
		return childeren;
	}

	@Override
	public String toString()
	{
		return periode.toString();
	}

	@Override
	public void setPeriode(Periode periode)
	{
		this.periode = periode;

	}

	public boolean isInCluster(String clusterName)
	{
		if(periode.getCluster().getClusterName().equals(clusterName))
			return true;

		return false;
	}

}
