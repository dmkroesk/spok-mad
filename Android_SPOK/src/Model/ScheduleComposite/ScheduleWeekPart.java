package Model.ScheduleComposite;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import Helpers.ObserverPattern.Subject;

public class ScheduleWeekPart extends Subject implements SchedulePart, Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	Periode periode;
	List<SchedulePart> childeren = new ArrayList<SchedulePart>();
	List<Cluster> clusterList = new ArrayList<Cluster>();

	@Override
	public void add(SchedulePart schedulePart)
	{
		childeren.add(schedulePart);
	}

	public List<Cluster> getClusterList()
	{
		return clusterList;
	}

	public void setClusterList(List<Cluster> clusterList)
	{
		this.clusterList = clusterList;
	}

	@Override
	public void remove(SchedulePart schedulePart)
	{
		childeren.remove(schedulePart);

	}

	@Override
	public SchedulePart getChild(int i)
	{
		return childeren.get(i);
	}

	@Override
	public Periode getPeriode()
	{
		return periode;
	}

	public List<SchedulePart> getChilderen()
	{
		return childeren;
	}

	@Override
	public void setPeriode(Periode periode)
	{
		this.periode = periode;

	}

	@Override
	public String toString()
	{
		return periode.toString();
	}

	public SchedulePart getDay(String day)
	{
		for(int i = 0; i < this.childeren.size(); i++)
		{
			SchedulePart dayPart = childeren.get(i);
			if(dayPart.getPeriode().getSubject().equals(day))
				return dayPart;

		}
		return null;
	}

	public ScheduleWeekPart deepCopy() throws Exception
	{
		// Serialization of object
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutputStream out = new ObjectOutputStream(bos);
		out.writeObject(this);

		// De-serialization of object
		ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
		ObjectInputStream in = new ObjectInputStream(bis);
		ScheduleWeekPart copied = (ScheduleWeekPart) in.readObject();

		// Verify that object is not corrupt

		// validateNameParts(fName);
		// validateNameParts(lName);

		return copied;
	}
}
