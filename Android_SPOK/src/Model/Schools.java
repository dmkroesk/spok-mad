package Model;

import java.util.ArrayList;
import Helpers.ObserverPattern.Subject;

public class Schools extends Subject
{
	private ArrayList<String> schools;

	public Schools()
	{
		schools = new ArrayList<String>();
	}

	public ArrayList<String> getSchools()
	{
		return schools;
	}

	public void setSchools(ArrayList<String> schools)
	{
		this.schools = schools;
	}
}
