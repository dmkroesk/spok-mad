package com.spok.android_spok;

import Controller.DataController;
import Helpers.AdListAdapter;
import Helpers.DataFetchers.Ad.AdStateUpdater;
import Helpers.ObserverPattern.Observer;
import Helpers.ObserverPattern.Subject;
import Model.Ad;
import Model.AdSubject;
import android.app.ActionBar;
import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;

public class AdActivity extends Activity implements Observer
{
	private ListView adsList;
	private AdSubject ads;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ad);
		ColorDrawable actionBarColor = new ColorDrawable(getResources().getColor(R.color.studycall_actionbar));
		ActionBar actionBar = getActionBar();
		actionBar.setBackgroundDrawable(actionBarColor);
		ads = new AdSubject();
		ads.addListener(this);
		adsList = (ListView) findViewById(R.id.ads_list);
		adsList.setClickable(true);
		adsList.setOnItemClickListener(new AdapterView.OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
				CheckBox adCheckbox = ((CheckBox) findViewById(R.id.ad_checkbox));
				adCheckbox.setChecked(!adCheckbox.isChecked());
				Ad clickedAd = ads.getAds().get(position);
				clickedAd.setSelected(adCheckbox.isChecked());
				AdStateUpdater adUpdater = new AdStateUpdater(clickedAd);
				adUpdater.execute();
			}
		});
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		DataController.getInstance().getAds(ads);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	public void update(Subject subject)
	{
		ArrayAdapter<Ad> schoolListAdapter = new AdListAdapter(this, ((AdSubject) subject).getAds());
		adsList.setAdapter(schoolListAdapter);
	}
}
