package com.spok.android_spok;

import java.util.List;
import Controller.DataController;
import Helpers.Cluster.ClusterListAdapter;
import Helpers.ObserverPattern.Observer;
import Helpers.ObserverPattern.Subject;
import Model.ScheduleComposite.Cluster;
import Model.ScheduleComposite.ScheduleDayPart;
import Model.ScheduleComposite.SchedulePart;
import Model.ScheduleComposite.ScheduleWeekPart;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;

public class ClusterScheduleSettingsActivity extends Activity implements Observer
{

	LinearLayout mainlayout;
	LayoutInflater layoutInflater;
	ScheduleWeekPart week;
	int CurrentSelectedDay;
	ScheduleDayPart day;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cluster_schedule_settings);
		mainlayout = (LinearLayout) findViewById(R.id.clusterlinearlayout);
		layoutInflater = getLayoutInflater();

		// setup the header
		ColorDrawable actionBarColor = new ColorDrawable(getResources().getColor(R.color.studycall_actionbar));
		ActionBar actionBar = getActionBar();
		actionBar.setBackgroundDrawable(actionBarColor);

		actionBar.setIcon(R.drawable.icoonkalender);
		actionBar.setHomeButtonEnabled(true);

		// First we setup Help Cluster
		View helpview;
		helpview = layoutInflater.inflate(R.layout.cluster_option_help_fragment_master, mainlayout, false);

		// now we setup the view
		TextView txtview = (TextView) helpview.findViewById(R.id.clustermasterhelptext);
		txtview.setText("Cluster Help");
		Button button = (Button) helpview.findViewById(R.id.clustermasterhelpbutton);
		button.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Button button = (Button) v;
				button.setText("v");
				// now we need to get parent of the view so we can show the right view
				View viewsub = (View) v.getParent();
				View view = (View) viewsub.getParent();
				showDetailsHelpSubView(view);
			}
		});

		mainlayout.addView(helpview);
		// get current day selected in overview
		CurrentSelectedDay = getIntent().getExtras().getInt("CurrentSelectedDay");
		// get current week schedule ---->will be the same as overview due to caching
		DataController con = DataController.getInstance();
		week = (ScheduleWeekPart) con.getMySchedule(this);
		// get all the clusters ---> these where fetched at the ApiCall and placed on the highest lvl
		View view;

		// get current day
		day = ((ScheduleDayPart) week.getChild(CurrentSelectedDay));
		// for each cluster create a view(master view)
		for(Cluster clus : week.getClusterList())
		{

			// get view so we can get the listview
			view = layoutInflater.inflate(R.layout.cluster_option_fragment_master, mainlayout, false);

			// now we pass the view and the cluster info TODO
			setupMasterSubView(view, clus.getClusterName(), clus);

			// add the view
			mainlayout.addView(view);

		}

	}

	private void showDetailsHelpSubView(View view)
	{
		// first we change the onclick to show to hide:
		Button button = (Button) view.findViewById(R.id.clustermasterhelpbutton);
		button.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Button button = (Button) v;
				button.setText(">");
				View viewsub = (View) v.getParent();
				View view = (View) viewsub.getParent();
				hideDetailsHelpSubView(view);
			}
		});
		// find where we want to add our subview to
		LinearLayout layout = (LinearLayout) view.findViewById(R.id.clustermasterhelplayout);
		LayoutInflater layoutinflater = getLayoutInflater();
		// inflate subview
		View subview = layoutinflater.inflate(R.layout.cluster_option_help_fragment_detail, layout, false);
		// add it
		layout.addView(subview);
	}

	protected void hideDetailsHelpSubView(View view)
	{
		// now we set back the onclick from to hide to show
		Button button = (Button) view.findViewById(R.id.clustermasterhelpbutton);
		button.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Button button = (Button) v;
				button.setText("v");
				// now we need to get parent of the view so we can show the right view
				View viewsub = (View) v.getParent();
				View view = (View) viewsub.getParent();
				showDetailsHelpSubView(view);
			}
		});
		// now we need to find the layout we want to delete the view from
		LinearLayout layout = (LinearLayout) view.findViewById(R.id.clustermasterhelplayout);
		// now we want to delete all views
		layout.removeAllViews();
	}

	private void setupMasterSubView(View view, String info, Cluster clus)
	{
		TextView txtview = (TextView) view.findViewById(R.id.clustermastertext);
		txtview.setText(info);
		txtview.setTextColor(clus.getColorOfCluster());
		Button button = (Button) view.findViewById(R.id.clustermasterbutton);
		button.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Button button = (Button) v;
				button.setText("v");
				// now we need to get parent of the view so we can show the right vie
				View viewsub = (View) v.getParent();
				View view = (View) viewsub.getParent();
				showDetailsSubView(view);
			}
		});

		Switch onOffSwitch = (Switch) view.findViewById(R.id.switchCluster);
		onOffSwitch.setChecked(clus.isDisplay());
		onOffSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener()
		{
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
			{
				View viewsub = (View) buttonView.getParent();
				String cluster = ((TextView) viewsub.findViewById(R.id.clustermastertext)).getText().toString();
				for(Cluster clus : week.getClusterList())
				{
					if(clus.getClusterName().equals(cluster))
					{
						clus.setDisplay(isChecked);
						break;
					}
				}

			}
		});
	}

	private void showDetailsSubView(View view)
	{
		// first we change the onclick to show to hide:
		Button button = (Button) view.findViewById(R.id.clustermasterbutton);
		button.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Button button = (Button) v;
				button.setText(">");
				View viewsub = (View) v.getParent();
				View view = (View) viewsub.getParent();
				hideDetailsSubView(view);
			}
		});
		// find where we want to add our subview to
		LinearLayout layout = (LinearLayout) view.findViewById(R.id.clustermasterlayout);
		LayoutInflater layoutinflater = getLayoutInflater();
		// we need to know what cluster we are expending
		String cluster = ((TextView) view.findViewById(R.id.clustermastertext)).getText().toString();

		View subview = layoutinflater.inflate(R.layout.cluster_option_fragment_detail, layout, false);

		ListView lv = (ListView) subview.findViewById(android.R.id.list);
		// create a list adapter for the list
		List<SchedulePart> periodeWithCluster = day.getAppointmentsForCluster(cluster);
		ClusterListAdapter adapter = new ClusterListAdapter(this, periodeWithCluster, lv);
		lv.setAdapter(adapter);
		// add it
		layout.addView(subview);
	}

	private void hideDetailsSubView(View view)
	{
		// now we set back the onclick from to hide to show
		Button button = (Button) view.findViewById(R.id.clustermasterbutton);
		button.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Button button = (Button) v;
				button.setText("v");
				// now we need to get parent of the view so we can show the right view
				View viewsub = (View) v.getParent();
				View view = (View) viewsub.getParent();
				showDetailsSubView(view);
			}
		});
		// now we need to find the layout we want to delete the view from
		LinearLayout layout = (LinearLayout) view.findViewById(R.id.clustermasterlayout);
		// now we want to delete all views
		layout.removeAllViews();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.general_menu, menu);
		return true;
	}

	@Override
	public void update(Subject subject)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle item selection
		switch(item.getItemId())
		{

			case android.R.id.home:
				onBackPressed();
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onBackPressed()
	{

		// SharedPreferences sPref = getSharedPreferences("RefreshList", MODE_PRIVATE);
		// SharedPreferences.Editor edit = sPref.edit();
		// edit.putBoolean("RefreshList", true);
		// edit.commit();

		Intent i = getIntent();
		setResult(OverviewActivity.RESULT_OK, i);
		finish();
	}
}
