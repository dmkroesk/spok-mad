package com.spok.android_spok;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class LogoffSettingsActivity extends Activity
{

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_logoff_settings);
		ColorDrawable actionBarColor = new ColorDrawable(getResources().getColor(R.color.avans_actionbar));
		ActionBar actionBar = getActionBar();
		actionBar.setBackgroundDrawable(actionBarColor);

		Button logoutBtn = (Button) findViewById(R.id.logout_button);
		logoutBtn.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				Intent logoutIntent = new Intent(LogoffSettingsActivity.this, MainActivity.class);
				logoutIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(logoutIntent);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

}
