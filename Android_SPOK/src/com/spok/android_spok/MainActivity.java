package com.spok.android_spok;

import Controller.DataController;
import Controller.SettingsController;
import Helpers.GCM.GPServices;
import Helpers.GCM.TempGCMVariables;
import Helpers.ObserverPattern.Observer;
import Helpers.ObserverPattern.Subject;
import Model.Schools;
import android.app.ActionBar;
import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;

public class MainActivity extends Activity implements Observer
{
	private ListView schoolList;
	private Schools schools;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		GPServices gps = new GPServices();
		String regid = "";
		// Check device for Play Services APK. If check succeeds, proceed with GCM registration.
		if(gps.checkPlayServices(this))
		{
			regid = gps.getRegistrationId(this);
			Log.e("SPOK", "regid = " + regid);
			if(regid.isEmpty())
			{
				gps.registerInBackground(this);
			}
		}
		else
		{
			Log.i("SPOK", "No working Google Play Services APK found.");
		}

		TempGCMVariables.regid = regid;
		Log.i("SPOK", regid);
		ColorDrawable actionBarColor = new ColorDrawable(getResources().getColor(R.color.studycall_actionbar));
		ActionBar actionBar = getActionBar();
		actionBar.setBackgroundDrawable(actionBarColor);
		schools = new Schools();
		schools.addListener(this);
		schoolList = (ListView) findViewById(R.id.school_list);
		schoolList.setTextFilterEnabled(true);
		schoolList.setOnItemClickListener(new AdapterView.OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
				Intent schoolTypeIntent;
				String schoolName = (String) schoolList.getItemAtPosition(position);
				SettingsController.getInstance().setSchool(schoolName);
				if(schoolName.equalsIgnoreCase(getApplicationContext().getString(R.string.avans)))
				{
					schoolTypeIntent = new Intent(MainActivity.this, LoginActivity.class);
					schoolTypeIntent.putExtra("Type", "Avans_Login");
				}
				else
				{
					schoolTypeIntent = new Intent(MainActivity.this, StudyCallSchoolSelectionActivity.class);
					schoolTypeIntent.putExtra("school", schoolName);
				}
				startActivity(schoolTypeIntent);
			}
		});
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		DataController.getInstance().getSchoolList(schools);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		SearchView searchView = (SearchView) menu.findItem(R.id.schoolSearch).getActionView();
		searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
		return true;
	}

	@Override
	public void update(Subject subject)
	{
		ArrayAdapter<String> schoolListAdapter = new ArrayAdapter<String>(this, R.layout.school_item, ((Schools) subject).getSchools());
		schoolList.setAdapter(schoolListAdapter);
	}
}
