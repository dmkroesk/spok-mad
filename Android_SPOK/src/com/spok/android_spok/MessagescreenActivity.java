package com.spok.android_spok;

import java.util.ArrayList;
import java.util.List;
import Controller.DataController;
import Controller.SettingsController;
import Helpers.ActivitySwipeDetector;
import Helpers.Swipe;
import Helpers.DataFetchers.Schedule.MessageList;
import Helpers.GUI.Factory_GUI;
import Helpers.GUI.Imp_Factory_GUI;
import Helpers.GUI.Initialize_GUI;
import Helpers.GUI.Message.GuiImplInterfaceMessage;
import Helpers.GUI.Overview.BaseOverviewImp;
import Helpers.ObserverPattern.Observer;
import Helpers.ObserverPattern.Subject;
import Model.ScheduleComposite.SchedulePart;
import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class MessagescreenActivity extends FragmentActivity implements Observer, Swipe
{

	Initialize_GUI guiinit;

	// vars
	static final int MIN_DISTANCE = 100;

	public int appointment = 0;
	boolean canSwipeLeft, canSwipeRight;
	Fragment currentSelection;
	// week with days that may contain changes
	SchedulePart changesAppointments;

	List<Fragment> listAppointmentFragments = new ArrayList<Fragment>();
	boolean inDetails = false;
	// for the swipe
	RelativeLayout lowestLayout;
	View.OnTouchListener gestureListener;
	boolean lockSwipe = false;
	private Initialize_GUI GuiImplInterfaceMessage;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		String showToast = getIntent().getStringExtra("notification");

		if(showToast != null && showToast.equals("changed"))
		{
			CharSequence text = "Er zijn wijzigingen gevonden!";
			int duration = Toast.LENGTH_SHORT;

			Toast toast = Toast.makeText(this, text, duration);
			toast.show();

		}

		setContentView(R.layout.activity_messagescreen);
		
		final ActionBar actionBar = getActionBar();
		actionBar.setIcon(R.drawable.icoonkalender);
		actionBar.setHomeButtonEnabled(true);

		// changesAppointments = con.getChangesInSchedule();
		
		// create the message fragments for the current selected school
		SettingsController scon = SettingsController.getInstance();
		String type = "";
		if(scon.getSchool().equals("Avans"))
			type = "AvansMessageActivity";
		else
			type = "StudyCallMessageActivity";

		Factory_GUI factory = Imp_Factory_GUI.getInstance();
		Initialize_GUI guiinit = factory.create(type);
		guiinit.init(this);
		this.guiinit = guiinit;
		
		DataController con = DataController.getInstance();
		con.getChangesInSchedule(this);
		
		// listAppointmentFragments = ((GuiImplInterfaceMessage) guiinit).getAppointmentFragements(changesAppointments);
		//
		// addFirstMessageFragment();
		//
		// currentSelection = listAppointmentFragments.get(appointment);
		//
		// // button stuff -> will be replaced with imaged (Soon)
		// if(listAppointmentFragments.size() > 0)
		// canSwipeRight = true;

		ActivitySwipeDetector activitySwipeDetector = new ActivitySwipeDetector(this);
		lowestLayout = (RelativeLayout) this.findViewById(R.id.lowest);
		lowestLayout.setOnTouchListener(activitySwipeDetector);

	}

	private void addFirstMessageFragment()
	{
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.add(R.id.messageStudyCall, listAppointmentFragments.get(appointment));
		transaction.commitAllowingStateLoss();
	}

	public void appointmentClick()
	{
		// // add appointmentFragment
		//
		// FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		// transaction.setCustomAnimations(R.anim.leaveb, R.anim.enterb);
		//
		// // new appointment fragment
		// Bundle args = new Bundle();
		// args.putSerializable("appointment", (Serializable) ((MessageInterface) listAppointmentFragments.get(appointment)).getPeriode());
		// args.putSerializable("part", (Serializable) ((MessageInterface) listAppointmentFragments.get(appointment)).getDay());
		// args.putInt("day", ((MessageInterface) listAppointmentFragments.get(appointment)).getDaynumber());
		//
		// android.support.v4.app.Fragment appointmentFrag = new AppointmentFragment();
		// appointmentFrag.setArguments(args);
		// transaction.addToBackStack(null);
		// transaction.replace(R.id.message, appointmentFrag);
		// transaction.commit();
		//
		// currentSelection = appointmentFrag;
		// inDetails = true;
		// ((BaseOverviewImp) guiinit).prev.setVisibility(View.INVISIBLE);
		// ((BaseOverviewImp) guiinit).next.setVisibility(View.INVISIBLE);
		// canSwipeRight = false;
		// canSwipeLeft = false;

	}

	public void changeAppointment(String typeChange)
	{
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		if(typeChange.equals("up"))
		{
			appointment++;
			transaction.setCustomAnimations(R.anim.leaveb, R.anim.enterb);
		}
		else
		{
			appointment--;
			transaction.setCustomAnimations(R.anim.enter, R.anim.leave);
		}
		transaction.replace(R.id.messageStudyCall, listAppointmentFragments.get(appointment));

		transaction.commit();
		setButtonsAppointments();
		waitTillAnimationIsDone();
	}

	private void setButtonsAppointments()
	{
		// change buttons vis
		if(appointment == 0)
		{
			canSwipeLeft = false;
			((BaseOverviewImp) guiinit).prev.setVisibility(View.INVISIBLE);
		}
		else
		{
			canSwipeLeft = true;
			((BaseOverviewImp) guiinit).prev.setVisibility(View.VISIBLE);
		}

		if(listAppointmentFragments.size() - 1 == appointment)
		{
			((BaseOverviewImp) guiinit).next.setVisibility(View.INVISIBLE);
			canSwipeRight = false;
		}
		else
		{
			canSwipeRight = true;
			((BaseOverviewImp) guiinit).next.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.general_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle item selection
		switch(item.getItemId())
		{
			case R.id.appointment_layout:
				SettingsController.getInstance().navigateToSettings(this);
				return true;
			case android.R.id.home:
				Intent myIntent = new Intent(MessagescreenActivity.this, OverviewActivity.class);

				// check who send him -> if overview do not add stack
				Intent intent = getIntent();
				String activity = intent.getStringExtra("activity");

				if(activity != null)
				{
					if(activity.equals("overview"))
					{
						// myIntent.setFlags(myIntent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
						finish();
					}
				}
				MessagescreenActivity.this.startActivity(myIntent);

			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void update(Subject subject)
	{
		MessageList listMessages = (MessageList) subject;
		listAppointmentFragments = ((GuiImplInterfaceMessage) guiinit).getAppointmentFragements(listMessages);

		addFirstMessageFragment();

		currentSelection = listAppointmentFragments.get(appointment);

		subject.removeListener(this);
		// button stuff -> will be replaced with imaged (Soon)
		if(listAppointmentFragments.size() > 0)
			canSwipeRight = true;
	}

	public void onRightToLeftSwipe()
	{
		if(!lockSwipe)
		{
			if(canSwipeRight)
				changeAppointment("up");
		}
	}

	public void onLeftToRightSwipe()
	{
		if(!lockSwipe)
		{
			if(canSwipeLeft)
				changeAppointment("down");
		}
	}

	public void onTopToBottomSwipe()
	{
		// TODO Auto-generated method stub

	}

	public void onBottomToTopSwipe()
	{
		// TODO Auto-generated method stub

	}

	private void waitTillAnimationIsDone()
	{
		lockSwipe = true;
		new java.util.Timer().schedule(new java.util.TimerTask()
		{
			@Override
			public void run()
			{
				lockSwipe = false;
			}
		}, 800);

	}

	@Override
	public void onBackPressed()
	{

		setButtonsAppointments();
		super.onBackPressed();

	}
}
