package com.spok.android_spok;

import java.util.ArrayList;
import java.util.List;
import Controller.DataController;
import Controller.SettingsController;
import Helpers.ActivitySwipeDetector;
import Helpers.Swipe;
import Helpers.GUI.Factory_GUI;
import Helpers.GUI.Imp_Factory_GUI;
import Helpers.GUI.Initialize_GUI;
import Helpers.GUI.Overview.BaseOverviewImp;
import Helpers.GUI.Overview.GuiImpInterfaceOverView;
import Helpers.ObserverPattern.Observer;
import Helpers.ObserverPattern.Subject;
import Helpers.Schedule.DayFragment.DayChangeFragement;
import Helpers.Schedule.DayFragment.ScheduleDayAdapter;
import Helpers.Schedule.FragmentInterface.AppointmentImplem;
import Helpers.Schedule.FragmentInterface.DayImplem;
import Helpers.Schedule.FragmentInterface.ScheduleFragmentInterface;
import Helpers.Schedule.FragmentInterface.WeekImplem;
import Model.ScheduleComposite.SchedulePart;
import Model.ScheduleComposite.ScheduleWeekPart;
import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;

public class OverviewActivity extends FragmentActivity implements Observer, Swipe
{
	// UI parts
	// ListView dayList;
	// TextView title, weekView, dayView, appointmentView;
	// ImageButton next, prev;

	Initialize_GUI guiinit;

	// vars
	static final int MIN_DISTANCE = 100;
	public int day = 0;
	public int appointment = 0;
	boolean canSwipeLeft, canSwipeRight;
	SchedulePart week;
	ScheduleDayAdapter adapter;
	List<Fragment> listDayFragments = new ArrayList<Fragment>();
	List<Fragment> listAppointmentFragments = new ArrayList<Fragment>();
	List<Fragment> listWeekFragments = new ArrayList<Fragment>();

	// need to be public cause it needs to be accesable from GUI implementations
	public ScheduleFragmentInterface currentSelection;
	public ScheduleFragmentInterface dayImplem;
	public ScheduleFragmentInterface appointmentImplem;
	public ScheduleFragmentInterface weekImplem;

	// for the swipe
	RelativeLayout lowestLayout;
	View.OnTouchListener gestureListener;
	boolean lockSwipe = false;
	// needed for preventing to continue while fetching schedule is here yet --- public because i am lazy
	public boolean doneLoading = false;

	// set it
	public ActivitySwipeDetector activitySwipeDetector;

	// static
	public static int CLUSTER_INTENT = 101;
	public static int RESULT_OK = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_overview);
		// prepare onswipe
		activitySwipeDetector = new ActivitySwipeDetector(this);
		// Start the fetching procces from the webservice -> in the observer update the rest of the data UI will be filled in.
		DataController con = DataController.getInstance();
		con.getMySchedule(this);
		// in the meanwhile start the GUI building procces
		// action bar
		final ActionBar actionBar = getActionBar();
		actionBar.setIcon(R.drawable.icoonberichten);
		actionBar.setHomeButtonEnabled(true);

		// create the day fragments
		// dayImplem = new DayImplem();
		// dayImplem.setSchedulePart(week);

		// Set UI elements
		SettingsController scon = SettingsController.getInstance();
		String type = "";
		if(scon.getSchool().equals("Avans"))
			type = "AvansOverviewActivity";
		else
			type = "StudyCallOverviewActivity";

		Factory_GUI factory = Imp_Factory_GUI.getInstance();
		Initialize_GUI guiinit = factory.create(type);
		guiinit.init(this);
		this.guiinit = guiinit;

		// listDayFragments = dayImplem.getFragements();
		// addDayFragment();
		// currentSelection = dayImplem;
		// canSwipeRight = true;

		((GuiImpInterfaceOverView) guiinit).setDayHeader();

		// activitySwipeDetector.setIsLongpressEnabled(false);
		lowestLayout = (RelativeLayout) this.findViewById(R.id.lowest);
		lowestLayout.setOnTouchListener(activitySwipeDetector);

	}

	public ArrayAdapter<?> getDayAdapter(List<SchedulePart> itemsArrayList, ListView lv)
	{
		return ((GuiImpInterfaceOverView) guiinit).getAdapter(this, itemsArrayList, lv);
	}

	// ADDS for the start of the app
	private void addDayFragment()
	{
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.add(R.id.day, listDayFragments.get(day));
		transaction.commit();
		// transaction.commitAllowingStateLoss();
	}

	public void dayClick()
	{

		changeToDayView(week.getChild(day), 0);
	}

	public void appointmentClick()
	{
		changeToAppointmentView(week.getChild(day), 0);
	}

	public void weekClick()
	{
		changeToWeekView(week, 0);
	}

	// Fragment replaces
	// day
	public void changeDay(String typeChange)
	{
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		if(typeChange.equals("up"))
			transaction.setCustomAnimations(R.anim.leaveb, R.anim.enterb);
		else
			transaction.setCustomAnimations(R.anim.enter, R.anim.leave);

		transaction.replace(R.id.day, listDayFragments.get(day), "DayFragment");
		// transaction.addToBackStack(null);
		transaction.commit();

		setButtonsDay();
		waitTillAnimationIsDone();
	}

	public void changeAppointment(String typeChange)
	{
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		if(typeChange.equals("up"))
			transaction.setCustomAnimations(R.anim.leaveb, R.anim.enterb);
		else
			transaction.setCustomAnimations(R.anim.enter, R.anim.leave);

		transaction.replace(R.id.day, listAppointmentFragments.get(appointment));
		// transaction.addToBackStack(null);
		// transaction.addToBackStack(null);
		transaction.commit();
		setButtonsAppointments();
		waitTillAnimationIsDone();

	}

	public void changeWeek(String typeChange)
	{
		// todo when expending overview
	}

	private void setButtonsDay()
	{
		// change buttons vis
		if(day == 0)
		{
			canSwipeLeft = false;
			((BaseOverviewImp) guiinit).prev.setVisibility(View.INVISIBLE);
		}
		else
		{
			canSwipeLeft = true;
			((BaseOverviewImp) guiinit).prev.setVisibility(View.VISIBLE);
		}

		if(week.getChilderen().size() - 1 == day)
		{
			canSwipeRight = false;
			((BaseOverviewImp) guiinit).next.setVisibility(View.INVISIBLE);
		}
		else
		{
			canSwipeRight = true;
			((BaseOverviewImp) guiinit).next.setVisibility(View.VISIBLE);
		}

	}

	private void setButtonsAppointments()
	{
		// change buttons vis
		if(appointment == 0)
		{
			canSwipeLeft = false;
			((BaseOverviewImp) guiinit).prev.setVisibility(View.INVISIBLE);
		}
		else
		{
			canSwipeLeft = true;
			((BaseOverviewImp) guiinit).prev.setVisibility(View.VISIBLE);
		}

		if(listAppointmentFragments.size() - 1 == appointment)
		{
			((BaseOverviewImp) guiinit).next.setVisibility(View.INVISIBLE);
			canSwipeRight = false;
		}
		else
		{
			canSwipeRight = true;
			((BaseOverviewImp) guiinit).next.setVisibility(View.VISIBLE);
		}
	}

	private void setButtonsWeek()
	{
		canSwipeRight = true;
		canSwipeLeft = false;
		((BaseOverviewImp) guiinit).next.setVisibility(View.VISIBLE);
		((BaseOverviewImp) guiinit).prev.setVisibility(View.INVISIBLE);
	}

	public void changeToWeekView(SchedulePart week, int weekToSet)
	{
		if(weekImplem == null)
		{
			weekImplem = new WeekImplem();
			weekImplem.setSchedulePart(week);
			listWeekFragments = weekImplem.getFragements(activitySwipeDetector);
		}
		currentSelection = weekImplem;

		final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.setCustomAnimations(R.anim.enter, R.anim.leave);
		ft.replace(R.id.day, listWeekFragments.get(weekToSet));

		ft.commit();
		// change the header -- Avansstuff
		((GuiImpInterfaceOverView) guiinit).setWeekHeader();
		setButtonsWeek();
		waitTillAnimationIsDone();
	}

	public void changeToDayView(SchedulePart day, int dayToSet)
	{
		currentSelection = dayImplem;
		this.day = dayToSet;
		final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.setCustomAnimations(R.anim.enter, R.anim.leave);
		ft.replace(R.id.day, listDayFragments.get(dayToSet));

		ft.commit();
		// change the header -- Avans Stuff
		((GuiImpInterfaceOverView) guiinit).setDayHeader();
		setButtonsDay();
		waitTillAnimationIsDone();
	}

	public void changeToAppointmentView(SchedulePart day, int appointmentToSet)
	{

		appointment = appointmentToSet;
		appointmentImplem = new AppointmentImplem();
		appointmentImplem.setSchedulePart(day);
		// get the appoints from this day
		listAppointmentFragments = appointmentImplem.getFragements(activitySwipeDetector);
		// set current type
		currentSelection = appointmentImplem;
		final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.setCustomAnimations(R.anim.leaveb, R.anim.enterb);
		ft.replace(R.id.day, listAppointmentFragments.get(appointmentToSet));

		// ft.addToBackStack(null);
		ft.commit();
		// change the header -- Avans stuff
		((GuiImpInterfaceOverView) guiinit).setAppointmentHeader();
		setButtonsAppointments();
		waitTillAnimationIsDone();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.general_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle item selection
		switch(item.getItemId())
		{
			case R.id.appointment_layout:
				SettingsController.getInstance().navigateToSettings(this);
				return true;
			case android.R.id.home:
				Intent myIntent = new Intent(OverviewActivity.this, MessagescreenActivity.class);
				myIntent.putExtra("activity", "overview");
				OverviewActivity.this.startActivity(myIntent);
				// myIntent.setFlags(myIntent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
				finish();
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void update(Subject subject)
	{
		// subject in this case will always be schedule
		week = (SchedulePart) subject;

		// removeTurnedOffClusters();
		// create the day fragments
		dayImplem = new DayImplem();
		dayImplem.setSchedulePart(week);

		listDayFragments = dayImplem.getFragements(activitySwipeDetector);
		addDayFragment();
		currentSelection = dayImplem;
		canSwipeRight = true;
		// remove this from observers
		((ScheduleWeekPart) week).removeListener(this);
		doneLoading = true;
	}

	public void onRightToLeftSwipe()
	{
		if(!lockSwipe)
		{
			if(canSwipeRight)
				currentSelection.changePart(this, "up");
		}
	}

	public void onLeftToRightSwipe()
	{
		if(!lockSwipe)
		{
			if(canSwipeLeft)
				currentSelection.changePart(this, "down");
		}
	}

	public void onTopToBottomSwipe()
	{
		// TODO Auto-generated method stub

	}

	public void onBottomToTopSwipe()
	{
		// TODO Auto-generated method stub

	}

	private void waitTillAnimationIsDone()
	{
		lockSwipe = true;
		new java.util.Timer().schedule(new java.util.TimerTask()
		{
			@Override
			public void run()
			{
				lockSwipe = false;
			}
		}, 800);

	}

	@Override
	public void onBackPressed()
	{
		if(currentSelection.canGoBackFromStack())
		{
			changeToDayView(week.getChild(day), day);
		}
		else
		{
			super.onBackPressed();
		}
	}

	@Override
	public void onResume()
	{
		// TODO Auto-generated method stub
		super.onResume();

	}

	public void refresh()
	{
		// removeTurnedOffClusters();

		for(Fragment frag : listDayFragments)
		{
			((DayChangeFragement) frag).setDataIsChanged(true);
		}

		((DayChangeFragement) listDayFragments.get(day)).refresh();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == CLUSTER_INTENT)
		{
			refresh();
		}
	}

	private void removeTurnedOffClusters()
	{
		try
		{
			week = (ScheduleWeekPart) ((ScheduleWeekPart) week).deepCopy();

			week = new ScheduleWeekPart();

			for(SchedulePart day : week.getChilderen())
			{
				List<SchedulePart> toRemove = new ArrayList<SchedulePart>();
				for(SchedulePart periode : day.getChilderen())
				{
					if(!periode.getPeriode().getCluster().isDisplay())
					{
						toRemove.add(periode);
					}
				}
				for(SchedulePart remove : toRemove)
				{
					day.remove(remove);
				}
			}
		}
		catch(Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
