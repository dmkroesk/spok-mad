package com.spok.android_spok;

import Controller.SettingsController;
import Helpers.DataFetchers.Profile.Registrator;
import Model.PersonalInformation;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class PersonalInformationActivity extends Activity
{
	private static final String PROFILE_INCOMPLETE = "Please fill in the missing fields";
	private static final String CONFIRMED = "Registering student";

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_personal_information);
		ColorDrawable actionBarColor = new ColorDrawable(getResources().getColor(R.color.studycall_actionbar));
		ActionBar actionBar = getActionBar();
		actionBar.setBackgroundDrawable(actionBarColor);

		Button registerBtn = (Button) findViewById(R.id.personal_continue_button);
		registerBtn.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				if(processProfileDetails())
				{
					Intent register = new Intent(PersonalInformationActivity.this, MessagescreenActivity.class);
					startActivity(register);
				}
			}
		});

		Button helpBtn = (Button) findViewById(R.id.personal_cancel_button);
		helpBtn.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				Intent helpcenter = new Intent(PersonalInformationActivity.this, MainActivity.class);
				startActivity(helpcenter);
			}
		});
	}

	private boolean processProfileDetails()
	{
		boolean returnValue = false;
		String studentNoString = ((EditText) findViewById(R.id.student_number)).getText().toString();
		String password = ((EditText) findViewById(R.id.password)).getText().toString().trim();
		String firstName = ((EditText) findViewById(R.id.first_name)).getText().toString().trim();
		String insertion = ((EditText) findViewById(R.id.insertion)).getText().toString().trim();
		String lastName = ((EditText) findViewById(R.id.last_name)).getText().toString().trim();
		String email = ((EditText) findViewById(R.id.email)).getText().toString().trim();
		String birthDate = ((EditText) findViewById(R.id.birth_date)).getText().toString().trim();
		String school = ((EditText) findViewById(R.id.school_type)).getText().toString().trim();
		if(TextUtils.isEmpty(studentNoString) || TextUtils.isEmpty(password) || TextUtils.isEmpty(firstName) || TextUtils.isEmpty(lastName) || TextUtils.isEmpty(email) || TextUtils.isEmpty(birthDate)
				|| TextUtils.isEmpty(school))
		{
			Toast failure = Toast.makeText(getApplicationContext(), PROFILE_INCOMPLETE, Toast.LENGTH_LONG);
			failure.show();
		}
		else
		{
			int studentNumber = Integer.parseInt(studentNoString);
			Toast success = Toast.makeText(getApplicationContext(), CONFIRMED, Toast.LENGTH_SHORT);
			success.show();
			PersonalInformation profile = new PersonalInformation(studentNumber, password, firstName, lastName, insertion, email, birthDate, school);
			Registrator registrator = new Registrator(profile);
			registrator.execute();
			returnValue = true;
		}
		return returnValue;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.general_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle item selection
		switch(item.getItemId())
		{
			case R.id.appointment_layout:
				SettingsController.getInstance().navigateToSettings(this);
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

}
