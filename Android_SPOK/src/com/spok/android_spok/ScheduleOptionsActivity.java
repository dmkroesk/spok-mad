package com.spok.android_spok;

import java.util.ArrayList;
import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

public class ScheduleOptionsActivity extends Activity
{
	ArrayList<String> clusterList = new ArrayList<String>();
	TextView txtView1;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_schedule_options);
		Spinner spinner = (Spinner) findViewById(R.id.spinner1);

		clusterList.add("Gesch");
		clusterList.add("Aardr");
		clusterList.add("Wisku");
		clusterList.add("Econo");

		// Create the ArrayAdapter
		ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(ScheduleOptionsActivity.this, android.R.layout.simple_spinner_item, clusterList);
		// Set the Adapter
		spinner.setAdapter(arrayAdapter);
	}
}
