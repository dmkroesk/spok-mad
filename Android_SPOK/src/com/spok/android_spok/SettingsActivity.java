package com.spok.android_spok;

import Helpers.GUI.Factory_GUI;
import Helpers.GUI.Imp_Factory_GUI;
import Helpers.GUI.Initialize_GUI;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;

public class SettingsActivity extends Activity
{

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);

		String type = getIntent().getStringExtra("Type");
		Factory_GUI factory = Imp_Factory_GUI.getInstance();
		Initialize_GUI guiinit = factory.create(type);
		guiinit.init(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

}
