package com.spok.android_spok;

import Helpers.DataFetchers.Profile.ProfileUpdater;
import Model.PersonalInformation;
import android.app.ActionBar;
import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class StudyCallProfileSettingsActivity extends Activity
{
	private static final String PROFILE_INCOMPLETE = "Please fill in the missing fields";
	private static final String CONFIRMED = "Updating profile";

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_study_call_profile_settings);
		ColorDrawable actionBarColor = new ColorDrawable(getResources().getColor(R.color.studycall_actionbar));
		ActionBar actionBar = getActionBar();
		actionBar.setBackgroundDrawable(actionBarColor);
		actionBar.setDisplayHomeAsUpEnabled(true);

		Button confirmButton = (Button) findViewById(R.id.personal_confirm_button);
		confirmButton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if(processProfileDetails())
					finish();
			}
		});
	}

	private boolean processProfileDetails()
	{
		boolean returnValue = false;
		String firstName = ((EditText) findViewById(R.id.first_name)).getText().toString().trim();
		String insertion = ((EditText) findViewById(R.id.insertion)).getText().toString().trim();
		String lastName = ((EditText) findViewById(R.id.last_name)).getText().toString().trim();
		String email = ((EditText) findViewById(R.id.email)).getText().toString().trim();
		String birthDate = ((EditText) findViewById(R.id.birth_date)).getText().toString().trim();
		String school = ((EditText) findViewById(R.id.school_type)).getText().toString().trim();
		if(TextUtils.isEmpty(firstName) || TextUtils.isEmpty(lastName) || TextUtils.isEmpty(email) || TextUtils.isEmpty(birthDate) || TextUtils.isEmpty(school))
		{
			Toast failure = Toast.makeText(getApplicationContext(), PROFILE_INCOMPLETE, Toast.LENGTH_LONG);
			failure.show();
		}
		else
		{
			Toast success = Toast.makeText(getApplicationContext(), CONFIRMED, Toast.LENGTH_SHORT);
			success.show();
			PersonalInformation profile = new PersonalInformation(firstName, lastName, insertion, email, birthDate, school);
			ProfileUpdater updater = new ProfileUpdater(profile);
			updater.execute();
			returnValue = true;
		}
		return returnValue;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle item selection
		switch(item.getItemId())
		{
			case android.R.id.home:
				finish();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

}
