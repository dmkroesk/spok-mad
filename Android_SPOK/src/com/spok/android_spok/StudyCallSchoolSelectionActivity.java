package com.spok.android_spok;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class StudyCallSchoolSelectionActivity extends Activity
{

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_study_call_school_selection);

		ColorDrawable actionBarColor = new ColorDrawable(getResources().getColor(R.color.studycall_actionbar));
		ActionBar actionBar = getActionBar();
		actionBar.setBackgroundDrawable(actionBarColor);

		((TextView) findViewById(R.id.studycall_login_password)).setText(getIntent().getStringExtra("school"));

		Button yesbutton = (Button) findViewById(R.id.studycall_yes_account);
		yesbutton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Intent myIntent = new Intent(StudyCallSchoolSelectionActivity.this, LoginActivity.class);
				myIntent.putExtra("Type", "StudyCall_Login");
				startActivity(myIntent);
			}
		});

		Button nobutton = (Button) findViewById(R.id.studycall_no_account);
		nobutton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Intent myIntent = new Intent(StudyCallSchoolSelectionActivity.this, PersonalInformationActivity.class);
				startActivity(myIntent);
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

}
