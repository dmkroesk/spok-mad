<?php
class Account {
    //private $id = null;
    private $user = null;
    private $session = null;
    //private $lastActivity = null;
    private $authorization = null;
    
    //private $validUser = 'Elle';
    //private $validPass = 'Elle';
	
    public function __construct(){
        
    }
    
    public function login($user, $pass){
        global $core;
        /*
        if($user==$this->validUser && $pass=$this->validPass){
            $session = '1234';
            setcookie('session', $session, time()+3600*24*30, '/', cookie_domain);
            return true;
        }*/
        
        
        $db = $core->getDatabase();
        $result = $db->select('SELECT * FROM [dbo].[users] WHERE [studentnummer]=\''.$user.'\' AND [wachtwoord]=\''.$pass.'\'');
        if($result){
            $acc = $result[0];
            
            if($this->user != null && $user==$this->user){
                return true;
            }
            
            $now = getTimestamp();
            if(!$db->edit('UPDATE [dbo].[users] SET last_activity=\''.$now.'\' WHERE [studentnummer]=\''.$user.'\''))
                return false;
            
            do {
                $session = generateRandomString(40);
            } while($db->select('SELECT * FROM [dbo].[session] WHERE [token]=\''.$session.'\''));
            
            if(!$db->edit('INSERT INTO [dbo].[session] (studentnummer, token, last_activity) VALUES(\''.$acc->studentnummer.'\', \''.$session.'\', \''.$now.'\')'))
            {
                return false;
            }
            
            //$this->id = $acc['id'];
            $this->user = $acc->studentnummer;
            $this->session = $session;
            setcookie('session', $session, time()+3600*24*30, '/', cookie_domain);
            $this->lastActivity = $now;
            $this->authorization = Authorization::Members;//$this->convertAuthorization($result[0]['authorization']);
            return true;
        }
        return false;
    }
    
    public function initWithSession($session){
        global $core;
        /*
	if($session=='1234'){
            $this->id = $this->validUser;
            $this->user = $this->validPass;
            $this->session = $session;
            setcookie('session', $session, time()+3600*24*30, '/', cookie_domain);
            $this->authorization = $this->convertAuthorization('member');
            return true;
	} else {
            setcookie ("session", "", time() - 3600, '/', cookie_domain);
        }
        */
        
        $db = $core->getDatabase();
        $result = $db->select('SELECT [studentnummer] FROM [dbo].[session] WHERE token=\''.$session.'\'');
        if($result!=false){
            //$result = $db->select('SELECT [*] FROM [dbo].[users] WHERE [studentnummer]=\''.$result[0]->studentnummer.'\'');
            
            $now = getTimestamp();
            $db->edit('UPDATE [dbo].[users] SET [last_activity]=\''.$now.'\' WHERE studentnummer=\''.$result[0]->studentnummer.'\'');
            $db->edit('UPDATE [dbo].[session] SET [last_activity]=\''.$now.'\' WHERE token=\''.$session.'\'');
            
            $acc = $result[0];
            //$this->id = $result[0]['id'];
            $this->user = $acc->studentnummer;
            $this->session = $session;
            setcookie('session', $session, time()+3600*24*30, '/', cookie_domain);
            $this->lastActivity = $now;
            $this->authorization = Authorization::Members;
            return true;
        }
		
        return false;
    }
    
    private function convertAuthorization($string){
        if($string=='banned'){
            return Authorization::Banned;
        } elseif ($string=='member') {
            return Authorization::Members;
        } elseif ($string=='admin') {
            return Authorization::Admin;
        }
    }
    
    public function logout() {
        global $core;
        if($this->session != null){
            $db = $core->getDatabase();
            $db->edit('DELETE FROM [dbo].[session] WHERE token=\''.$this->session.'\'');
            setcookie ("session", "", time() - 3600, '/', cookie_domain);
            return true;
        } else {
            return false;
        }
    }
    
    public function getId(){
        return $this->id;
    }
    
    public function getUser(){
        return $this->user;
    }
    
    public function getSession(){
        return $this->session;
    }
    
    public function getLastActivity(){
        return $this->lastActivity;
    }
    
    public function getAuthorization(){
        return $this->authorization;
    }
}