<?php
final class Authorization
{
    const Banned = -1;
    const Everyone = 0;
    const Members = 1;
    const Admins = 2;
}

define('cookie_domain', '');
//define('cookie_domain', 'dev.wkict.nl');


function getTimestamp(){ 
    return date("Y-m-d H:i:s");
}

function generateRandomString($length = 10) {
    $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $str = '';
    for ($i = 0; $i < $length; $i++) {
        $str .= $chars[rand(0, strlen($chars) - 1)];
    }
    return $str;
}