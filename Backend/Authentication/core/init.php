<?php
header('Content-type: text/json');
$dir = 'core/';

include($dir . 'enums.php');

include($dir . 'classes/Core.php');
include($dir . 'classes/Account.php');
include($dir . 'classes/Database.php');

$core = new Core();