<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
$json = new stdClass;

if(isset($_GET['user']) && isset($_GET['pass'])){
    include('core/init.php');
    $core->load();

    $user = $_GET['user'];
    $pass = $_GET['pass'];

    if($core->getAccount()->login($user, $pass)){
        $json->status = 'Logged in';

		if(isset($_GET['iosDeviceToken']) && $_GET['iosDeviceToken']!=''){
			//Found a iosDeviceToken, so store it for push notifications!
			$deviceToken = $_GET['iosDeviceToken'];

			
			$db = $core->getDatabase();
			$result = $db->select('SELECT * FROM [dbo].[pushdevice] WHERE [registration_id_android]=\''.$deviceToken.'\'');
			if($result){
				//Already exists
			} else {
				//proceed
				if(!$db->edit('INSERT INTO [dbo].[pushdevice] (android_id, registration_id_android, device_type) VALUES(\''.$core->getAccount()->getUser().'\', \''.$deviceToken.'\', \'ios\')'))
				{
					echo 'failed to signup ios device for push notifications';
				} else {
					//echo 'succes!';
				}
			}
		}
    } else {
        $json->error = 'Foute gebruikersnaam of wachtwoord';
    }

    $core->unload();
} else {
    $json->error = 'Missing user and pass parameters';
}

echo json_encode($json);