<?php
$json = new stdClass;
include('core/init.php');
$core->load();

if($core->getAccount()->getUser() == null){
    $json->error = "U bent al uitgelogd";
} else {
    if($core->getAccount()->logout()){
        $json->success = true;
		
		
		if(isset($_GET['iosDeviceToken']) && $_GET['iosDeviceToken']!=''){
			//Found a iosDeviceToken, so store it for push notifications!
			$deviceToken = $_GET['iosDeviceToken'];

			
			$db = $core->getDatabase();
			$result = $db->select('SELECT * FROM [dbo].[pushdevice] WHERE [registration_id_android]=\''.$deviceToken.'\' AND [device_type]=\'ios\'');
			if($result){
				//proceed
				if(!$db->edit('DELETE FROM [dbo].[pushdevice] WHERE [registration_id_android]=\''.$deviceToken.'\' AND [device_type]=\'ios\''))
				{
					echo 'Failed to unsubscribe ios device for push notifications';
				} else {
					//echo 'succes!';
				}
			} else {
				//Is not registered as push device
				//echo 'Is not registered for ios push notifications';
			}
		}
    } else {
        $json->error = "Inloggen mislukt";
    }
}

$core->unload();
echo json_encode($json);