<html>
    <head>
        <title>Push berichten demo</title> 
		<link rel="stylesheet" href="push_notification.css" />
    </head>
    <body>
		<div class="header">
			<img id="studycall_logo" height="75px" src="images/studycall_logo.png" />
			<h1>Push berichten demo</h1>
		</div>
		
		<div class="credits">
			<img height="75px" src="images/brilliantnet_logo.png" />
			<img height="65px" src="images/avans_logo.png" />
			<img height="71px" src="images/itworkz_logo.png" />
		</div>

		<div id="ios_push">
			<h2>iOS</h2>
			<form method="post" action="index.php">
				<input type="hidden" name="type" value="ios"/>
				<input type="submit" value="Push notificatie 1" />
			</form>
			<form method="post" action="index.php">
				<input type="hidden" name="type" value="ios2"/>
				<input type="submit" value="Push notificatie 2" />
			</form>
		</div>
		
		<div id="android_push">
			<h2>Android</h2>
			<form method="post" action="index.php">
				<input type="hidden" name="type" value="android"/>
				<input type="submit" value="Push notificatie 1" />
			</form>
			<form method="post" action="index.php">
				<input type="hidden" name="type" value="android2"/>
				<input type="submit" value="Push Notificatie 2" />
			</form>
		</div>
        
		<?php
        if (isset($_POST["type"])) {
			echo '
				<div class="progress">
				<div>';
				
			echo 'Push notificatie wordt verzonden... <br/>';
		$pushMessage1 = "BARI op lesuur 5 is gewijzigd in het vak ENGELS, in lokaal b120";
		$pushMessage2 = "ENGELS op lesuur 5 is gewijzigd in het vak BARI, in lokaal c201";
		
            if ($_POST["type"] == "ios") {
                updateQueryOne($pushMessage1);
                pushNotificationiOS($pushMessage1);
            } else if ($_POST["type"] == "ios2") {
                updateQueryTwo($pushMessage2);
                pushNotificationiOS($pushMessage2);
            } else if ($_POST["type"] == "android") {

                updateQueryOne($pushMessage1);
                pushNotificationAndroid($pushMessage1);
            } else if ($_POST["type"] == "android2") {

                updateQueryTwo($pushMessage2);
                pushNotificationAndroid($pushMessage2);
            }
        }

        function updateQueryOne($pushMessage) {
            $serverName = "localhost"; //serverName\instanceName
            $connectionInfo = array("Database" => "STUDYCALL", "UID" => "sa", "PWD" => "Donderdag123");
            $conn = sqlsrv_connect($serverName, $connectionInfo);


            if ($conn) {
                $sql1 = "UPDATE [dbo].[rooster] SET [Vak]='ENGELS', [Lokaal]='c201' WHERE [Studentnummer]='40825' AND [Vak]='BARI' AND [Dag]='1' AND [Lokaal]='b120'";
		$sql2 = "INSERT INTO [dbo].[berichten] (studentnummer, bericht,datum) VALUES ('40825', '$pushMessage','2013-01-01 11:05:00')"; 
                $stmt2 = sqlsrv_query($conn, $sql1);
				
                $stmt3 = sqlsrv_query($conn, $sql2);
                if ($stmt2 === false || $stmt3 === false) {
                    die(print_r(sqlsrv_errors(), true));
                }
            } else {
                echo "Error: Connection could not be established.<br />";
                die(print_r(sqlsrv_errors(), true));
            }
        }

        function updateQueryTwo($pushMessage) {
            $serverName = "localhost"; //serverName\instanceName
            $connectionInfo = array("Database" => "STUDYCALL", "UID" => "sa", "PWD" => "Donderdag123");
            $conn = sqlsrv_connect($serverName, $connectionInfo);


            if ($conn) {
                $sql1 = "UPDATE [dbo].[rooster] SET [Vak]='BARI' , [Lokaal]='b120' WHERE [Studentnummer]='40825' AND [Vak]='ENGELS' AND [Dag]='1'AND [Lokaal]='c201'";
		$sql2 = "INSERT INTO [dbo].[berichten] (studentnummer, bericht,datum) VALUES ('40825', '$pushMessage', '2013-01-01 11:05:00')"; 
                $stmt2 = sqlsrv_query($conn, $sql1);
		$stmt3 = sqlsrv_query($conn, $sql2);
                if ($stmt2 === false || $stmt3 === false) {
                    die(print_r(sqlsrv_errors(), true));
                }
            } else {
                echo "Error: Connection could not be established.<br />";
                die(print_r(sqlsrv_errors(), true));
            }
        }

        function pushNotificationAndroid($pushMessage) {
            $apiKey = "AIzaSyCnnAJpqWigKh5R7a1V4CJRZ4LV6F2WH14";

            $registrationIDs = array("APA91bHIqGULNVbHudsnMNZnFneP3YsOFXR0U2XAvGmsS2K8_IfIhyKCnkEtzXa5CKpMj0__icEJf2MGTxq1un7eAAmWzruCUa1rogyes_jb1fHEiUyCd8hhezjQUhsZGj41X67QUsrhqe95CPwVOyS8VlZ7Jmevtg", "APA91bEXHXP1UB_bgBXXpKjH86Q10UtldIKgA07jMp-YxE2RUXaj9xproINZhsgAFM-X5Nt_CG2Bd1NZ4jibbqfvgI2cBdGhvRfjdD-ia-_tRuqSTTVfJOP7E7tGcw4Eo2lMEF2_6zPKx-myG9coVrblhTaoYGjGEg", "APA91bFOQqZ02qQHkwHV9f2ri7CGT73w_wJhb6uAnjZ0u6UtvBywsQyoOBEb5mzOYscIJEPs_Ae0gRJttb-DcQ8AVysN0K0RBcYuUVWeBmOieTleYLz6WiFxcifexFa1e1hlvwjh9mgAXWzEAqkwIdvItwsyZgvTBg", "APA91bFNk7JRLlOad_c3aWUH5ZivftZOl-EOBVWgCnfjidq5AVFrkCeUiNZf9NSKEfIIT7lOPmDOyXzxMexV93in8bCyruRa68tuaxwFD0jkfv--jHCYb9aD8DiOfBZa5xhNbJfWfEex4-TOM_0cLZuZKDDAYLqXcQ", "APA91bFT1aH_D4LymLwIXUvuy22qYDOyjCpIs_5XzeD4EyXkFMkTmCYeh-D97xQl3CkxO5EMKtKAU4-Bs3GOxdJJ4Qi9UQV-pqJxqaiRNtFszLBe1NaMaSXfMKBzaFPfDIVBr2dVyUODL2Z_S69oqCX__5XmiC6J9A", "APA91bHOAa4xl8tjmO4Dr6Bh4H0clsTlO7WfcWivCf3loi1ldOydyoExB-jFe9ywCC1_E-v3lN3QbJ4gYetwQCjKR_jzR65gtJq485uUrenvNETB2n1wXwc-u9TfPkXLG8w5H7lh9V5FM0JODnX4tEwXSwYohuq_cg", "APA91bGGmwwed4D54u0-U5zKJAd5ZbxSX2juR1gVIdJKyPLjbMIu-3Yr8rrqTZlByVGkEYOgDRz1Fdt3omNwUVLx5uI5tOuRfWSX2HECRW5P347X5CXTZSyiGZMGNi7WxISreSwk6Ma8pDMVsVOLDLY_fsLLG4eC9w");

            $message = "Wijziging gevonden";

            $url = 'https://android.googleapis.com/gcm/send';

            $fields = array(
                'registration_ids' => $registrationIDs,
                'data' => array("message" => $message),
            );

            $headers = array(
                'Authorization: key=' . $apiKey,
                'Content-Type: application/json'
            );

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            //curl_setopt($ch, CURLOPT_POST, true);
            //curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            $result = curl_exec($ch);

            curl_close($ch);
            echo $result;
			echo '<br/>';
            //print_r($result);
            //var_dump($result);
            echo "Android push notification is verzonden.<br/>";
        }

        function pushNotificationiOS($pushMessage) {
		
            if (isset($_GET['message']))
                $message = $_GET['message'];
            else
                $message = $pushMessage;

            // Read device token here (without spaces):
			/*
            $fh = fopen('devices.txt', 'r');
            $deviceToken = fread($fh, filesize('devices.txt'));
            echo 'Device: ' . $deviceToken . '<br/>';
            fclose($fh);*/

            // Put your device token here (without spaces):
            //$deviceToken = 'f4267a809f4e71f13e8c671c96421e7bd6ee9a16355c988a682c0de45adc11ca';
            // Put your private key's passphrase here:
            $passphrase = 'spok'; //'tijdelijk';
            ////////////////////////////////////////////////////////////////////////////////

            $ctx = stream_context_create();
            stream_context_set_option($ctx, 'ssl', 'local_cert', 'ck.pem');
            stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

            // Open a connection to the APNS server
            $fp = stream_socket_client(
                    'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

            if (!$fp) {
				print_r($err);
				echo '//';
				print_r($errstr);
				echo '//';
                exit("Failed to connect: " . PHP_EOL);
			}

            echo 'Connected to APNS' . PHP_EOL;
			echo '<br/>';

            $deviceTokens = array(
				"810d475d8a10ab82b042c74e01cdf051d99eacb1fad19bf57b5692bc5c165ab7"
				//,"810d475d8a10ab82b042c74e01cdf051d99eacb1fad19bf57b5692bc5c165ab2"
				);
				
			$deviceTokens = getSubscribedIOSDevices();
				
            for ($i = 0; $i < count($deviceTokens); $i++) {

                // Put your alert message here:
                $datetime = new DateTime();
                //$message = 'The time is ' . $datetime->format('Y/m/d H:i:s') . '.';
                //$message = $pushMessages;
                // Create the payload body
                $body['aps'] = array(
                    'alert' => $message,
                    'sound' => 'default'
                );

                // Encode the payload as JSON
                $payload = json_encode($body);

                // Build the binary notification
                $msg = chr(0) . pack('n', 32) . pack('H*', $deviceTokens[$i]) . pack('n', strlen($payload)) . $payload;

                // Send it to the server
                $result = fwrite($fp, $msg, strlen($msg));

                if (!$result)
                    echo 'Message not delivered' . PHP_EOL . '<br/>';
                else
                    echo 'Message "' . $message . '" successfully delivered' . PHP_EOL . ' to device: ' . $deviceTokens[$i] . '<br/>';

                //sleep(1);
            }

            // Close the connection to the server
            fclose($fp);
			
			echo '
			</div></div>';
        }
		
        function getSubscribedIOSDevices() {
            $serverName = "localhost"; //serverName\instanceName
            $connectionInfo = array("Database" => "STUDYCALL", "UID" => "sa", "PWD" => "Donderdag123");
            $conn = sqlsrv_connect($serverName, $connectionInfo);
			
			$deviceTokens = array();

            if ($conn) {
                $sql = "SELECT * FROM [dbo].[pushdevice] WHERE [device_type]='ios'";
                $stmt = sqlsrv_query($conn, $sql);
                if ($stmt === false) {
                    die(print_r(sqlsrv_errors(), true));
                }
				
				while( $obj = sqlsrv_fetch_object( $stmt)) {
					array_push($deviceTokens, $obj->registration_id_android);
				}
				
            } else {
                echo "Error: Connection could not be established.<br />";
                die(print_r(sqlsrv_errors(), true));
            }
			
			//print_r($deviceTokens);
			return $deviceTokens;
        }
        ?>
    </body>
</html>