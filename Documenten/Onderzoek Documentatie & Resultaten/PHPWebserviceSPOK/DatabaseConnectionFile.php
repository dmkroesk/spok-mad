<?php
 
class DatabaseConnectionFile {
    private $link;
  
    
     function getStations(){
         $stmt = $this->link->prepare("SELECT * FROM stations");                
         $stmt->execute();        
     
             $meta = $stmt->result_metadata();
             $array = $this->resultToArray($stmt,$meta);
             $stmt->close();
             return json_encode($array);                       
      }
                     
    
    
    function getTrainsForStation($Station){
        
     $stmt = $this->link->prepare("SELECT * FROM stations WHERE Name = ?");    
     $stmt->bind_param("s", $Station); 
    
      $stmt->execute();             

             $meta = $stmt->result_metadata();
             $array = $this->resultToArray($stmt,$meta);
             $stmt->close();
             return json_encode($array);     
    
    }
    function resultToArray($stmt,$meta){
        
        while ($field = $meta->fetch_field()) 
            { 
                $params[] = &$row[$field->name]; 
            } 

            call_user_func_array(array($stmt, 'bind_result'), $params); 
            
            $array =array();
            while ($stmt->fetch()) { 
                foreach($row as $key => $val) 
                { 
                    $c[$key] = $val; 
                } 
                $array[] = $c; 
            } 
        return $array;
    }
     function makeConnection(){
       
            $user_name = "root";
            $password = "";
            $database = "Spok";
            $server = "localhost";
            
            //$link = mysql_connect($server, $user_name, $password);
            // mysql_select_db($database);
            $this ->link = new mysqli($server, $user_name, $password, $database);
            if (! $this ->link) {
                    die('Could not connect: ' . mysqli_error());
            }     
    }
}
?>