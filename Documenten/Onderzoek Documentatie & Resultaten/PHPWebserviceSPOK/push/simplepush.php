<?php

if($_GET['push'] == true){
	if(isset($_GET['amount']))
		$amount = $_GET['amount'];
	else
		$amount = 1;
	
	// Read device token here (without spaces):
	$fh = fopen('devices.txt', 'r');
	$deviceToken = fread($fh, filesize('devices.txt'));
	echo 'Device: ' . $deviceToken;
	fclose($fh);
	
	// Put your device token here (without spaces):
	//$deviceToken = 'f4267a809f4e71f13e8c671c96421e7bd6ee9a16355c988a682c0de45adc11ca';

	// Put your private key's passphrase here:
	$passphrase = 'testtest';

	////////////////////////////////////////////////////////////////////////////////

	$ctx = stream_context_create();
	stream_context_set_option($ctx, 'ssl', 'local_cert', 'ck.pem');
	stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

	// Open a connection to the APNS server
	$fp = stream_socket_client(
		'ssl://gateway.sandbox.push.apple.com:2195', $err,
		$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

	if (!$fp)
		exit("Failed to connect: $err $errstr" . PHP_EOL);

	echo 'Connected to APNS' . PHP_EOL;

	
	for($i=0; $i<$amount; $i++){

		// Put your alert message here:
		$datetime = new DateTime(); 
		$message = 'The time is ' . $datetime->format('Y/m/d H:i:s') . '.';
		
		// Create the payload body
		$body['aps'] = array(
			'alert' => $message,
			'sound' => 'default'
			);

		// Encode the payload as JSON
		$payload = json_encode($body);

		// Build the binary notification
		$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

		// Send it to the server
		$result = fwrite($fp, $msg, strlen($msg));

		if (!$result)
			echo 'Message not delivered' . PHP_EOL;
		else
			echo 'Message "'.$message.'" successfully delivered' . PHP_EOL;
		
		sleep(1);
	}

	// Close the connection to the server
	fclose($fp);
}