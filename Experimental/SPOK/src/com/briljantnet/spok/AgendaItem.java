package com.briljantnet.spok;

import org.json.JSONException;
import org.json.JSONObject;

public class AgendaItem
{

	private String startTime;
	private String endTime;
	private String activity;

	// private String dayOfWeek;

	public AgendaItem(String beginTijd, String eindTijd, String activiteit/* , String weekDag */)
	{
		startTime = beginTijd;
		endTime = eindTijd;
		activity = activiteit;
		// dayOfWeek = weekDag;
	}

	public AgendaItem(JSONObject data)
	{
		try
		{
			startTime = data.getString("BEGIN_TIJD");
			endTime = data.getString("EIND_TIJD");
			activity = data.getString("ACTIVITEIT");
			// dayOfWeek = data.get("DAG");
		}
		catch(JSONException jsone)
		{
			jsone.printStackTrace();
		}
	}

	public String getBegintijd()
	{
		return startTime;
	}

	public void setBegintijd(String begintijd)
	{
		this.startTime = begintijd;
	}

	public String getEindtijd()
	{
		return endTime;
	}

	public void setEindtijd(String eindtijd)
	{
		this.endTime = eindtijd;
	}

	public String getTitel()
	{
		return activity;
	}

	public void setTitel(String titel)
	{
		this.activity = titel;
	}

	@Override
	public String toString()
	{
		return this.activity + "  van " + this.startTime + "   tot " + this.endTime;
	}
}
