package com.briljantnet.spok;

import java.util.ArrayList;
 
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
 
public class CustomAdapter extends ArrayAdapter<AgendaItem>{
    private Activity activity;
    private ArrayList<AgendaItem> entries = new ArrayList<AgendaItem>();
    
    public CustomAdapter(Activity a, int textViewResourceId, ArrayList<AgendaItem> agendaitems) {
        super(a, textViewResourceId, agendaitems);
        this.entries = agendaitems;
        this.activity = a;
    }
 
    public static class ViewHolder{
        public TextView item1;
        public TextView item2;
    }
 
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder;
        if (v == null) {
            LayoutInflater vi =
                (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.agendaitemlayout, null);

            //v.setMinimumHeight(300);
            holder = new ViewHolder();
            holder.item1 = (TextView) v.findViewById(R.id.firstLine);
            holder.item2 = (TextView) v.findViewById(R.id.secondLine);
            v.setTag(holder);
        }
        else
            holder=(ViewHolder)v.getTag();
 
        final AgendaItem custom = entries.get(position);
        if (custom != null) {
            holder.item1.setText(custom.getTitel());
            holder.item2.setText(custom.getBegintijd().substring(11, 16) + "-" + custom.getEindtijd().substring(11, 16));
        }
        return v;
    }
 
}
