package com.briljantnet.spok;

import java.util.ArrayList;

public interface DataCompletedListener
{
	void OnDataCompleted(ArrayList<AgendaItem> result);
}
