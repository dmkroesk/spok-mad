package com.briljantnet.spok;

public interface DataListener
{
	void onDataChanged();
}
