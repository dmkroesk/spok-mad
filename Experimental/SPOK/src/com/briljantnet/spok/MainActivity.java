package com.briljantnet.spok;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.Display;
import android.view.Menu;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout.LayoutParams;

public class MainActivity extends Activity implements DataListener
{
	private ProgressDialog refreshing;
	private WebService webService;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		fancyAnimationStartenNuMan();
		webService = new WebService(this);
		webService.updateRooster();
		refreshing = new ProgressDialog(this);
		refreshing.setMessage("Please wait...");
		refreshing.setCancelable(false);
		refreshing.show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	private void fancyAnimationStartenNuMan()
	{
		Display display = getWindowManager().getDefaultDisplay();
		Point screenSize = new Point();
		display.getSize(screenSize);

		Rect rectgle = new Rect();
		Window window = getWindow();
		window.getDecorView().getWindowVisibleDisplayFrame(rectgle);
		screenSize.y = rectgle.height();
		screenSize.x = rectgle.width();

		ImageView splash = (ImageView) findViewById(R.id.splash_image);
		LayoutParams params = new LayoutParams(screenSize.x, screenSize.y * 2);
		params.setMargins(0, -screenSize.y, 0, 0);
		splash.setLayoutParams(params);

		Animation anim1 = new TranslateAnimation(Animation.RELATIVE_TO_PARENT, 0, Animation.RELATIVE_TO_SELF, screenSize.y);
		Animation anim2 = new AlphaAnimation(1f, 0f);

		AnimationSet animationSet = new AnimationSet(true);
		animationSet.addAnimation(anim1);
		animationSet.addAnimation(anim2);
		animationSet.setDuration(1000);
		animationSet.setFillEnabled(true);
		animationSet.setFillAfter(true);
		try
		{
			Thread.sleep(200);
			splash.startAnimation(animationSet);
		}
		catch(InterruptedException ie)
		{
			ie.printStackTrace();
		}
	}

	@Override
	public void onDataChanged()
	{
		final CustomAdapter adapter = new CustomAdapter(MainActivity.this, R.id.agendalist, webService.getRoosterItems());
		ListView myListView = (ListView) findViewById(R.id.agendalist);
		myListView.setAdapter(adapter);
		adapter.notifyDataSetChanged();
		refreshing.hide();
	}
}
