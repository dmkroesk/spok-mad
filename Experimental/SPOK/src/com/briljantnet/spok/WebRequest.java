package com.briljantnet.spok;

import java.io.IOException;
import java.util.ArrayList;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import android.os.AsyncTask;
import android.util.Log;

public class WebRequest extends AsyncTask<String, Void, Void>
{
	private DataCompletedListener dataCompletedListener;
	private ArrayList<AgendaItem> roosterItems = new ArrayList<AgendaItem>();

	public WebRequest(DataCompletedListener dataRequestCompletedListener)
	{
		dataCompletedListener = dataRequestCompletedListener;
	}

	@Override
	protected void onPostExecute(Void result)
	{
		dataCompletedListener.OnDataCompleted(roosterItems);
	}

	@Override
	protected Void doInBackground(String... url)
	{
		String requestUrl = url[0];
		requestData(requestUrl);
		return null;
	}

	private void requestData(String url)
	{
		HttpClient client = new DefaultHttpClient();
		try
		{
			HttpGet getRequest = new HttpGet(url);
			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			String httpResponse = client.execute(getRequest, responseHandler);
			Log.d("JSONRequest: ", httpResponse);
			JSONArray dagRooster = new JSONArray(httpResponse);
			for(int i = 0; i < dagRooster.length(); i++)
			{
				roosterItems.add(new AgendaItem(dagRooster.getJSONObject(i)));
			}
		}
		catch(IOException ioe)
		{
			Log.e("WebRequest", ioe.getMessage());
		}
		catch(JSONException jsone)
		{
			Log.e("WebRequest", jsone.getMessage());
		}
	}
}
