package com.briljantnet.spok;

import java.util.ArrayList;

public class WebService implements DataCompletedListener
{
	private ArrayList<AgendaItem> roosterItems = new ArrayList<AgendaItem>();
	private DataListener activity;

	public WebService(DataListener mainActivity)
	{
		this.activity = mainActivity;
	}

	public void updateRooster()
	{
		/* Avans URL: http://145.48.6.37/index.php/api/?method=Rooster&User_id=2&format=json
		 * Test URL: http://84.24.21.108/spok/index.php/api/?method=Rooster&User_id=2&format=json */
		new WebRequest(this).execute("http://84.24.21.108/spok/index.php/api/?method=Rooster&User_id=2&format=json");
	}

	@Override
	public void OnDataCompleted(ArrayList<AgendaItem> result)
	{
		roosterItems = result;
		activity.onDataChanged();
	}

	public ArrayList<AgendaItem> getRoosterItems()
	{
		return roosterItems;
	}

	public void setRoosterItems(ArrayList<AgendaItem> roosterItems)
	{
		this.roosterItems = roosterItems;
	}
}
