

#import <UIKit/UIKit.h>

// Delegates

@protocol MenuTabbarScrollViewDelegate <UIScrollViewDelegate>
@required
    -(void)menuTappedAtIndex:(NSUInteger)index;
@optional
@end


@interface MenuTabbarScrollView : UIScrollView

@property(nonatomic, strong) NSMutableArray *images;

@property (nonatomic, assign) id<MenuTabbarScrollViewDelegate> delegate;

@end
