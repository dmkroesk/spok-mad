

#import "MenuTabbarScrollView.h"

static const NSUInteger TAGBASE = 2324;

@implementation MenuTabbarScrollView

@synthesize images = _images;
@dynamic delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initView];
    }
    return self;
}

-(void) initView
{
    CGFloat imageWidth = 103.0f;
    CGFloat imageHeight = 85.0f;
    
    // Moet dit buiten dit object ??
    _images = [[NSMutableArray alloc] init];
    for (int idx = 0; idx < 10; idx++)
    {
        NSString *imgName = [NSString stringWithFormat:@"btn-%d", idx];
        [_images addObject:[UIImage imageNamed:imgName]];
    }
    
    //
    for (int idx = 0; idx < [_images count]; idx++)
    {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(idx*imageWidth, 0, imageWidth, imageHeight)];
        imageView.tag = TAGBASE + idx;
        imageView.image = [_images objectAtIndex:idx];
        imageView.userInteractionEnabled = YES;
            
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                              action:@selector(scrollBarImageTap:)];
        [imageView addGestureRecognizer:tap];
        
        [self addSubview:imageView];
    }
    [self setContentSize:CGSizeMake(700, 100)];
    [self setShowsHorizontalScrollIndicator:NO];
}

//
- (void)scrollBarImageTap:(UITapGestureRecognizer*)sender
{
    // Get index (tag) of tapped image
    NSInteger tag = [sender view].tag - TAGBASE;
    
    // send to delegate
    if( [self.delegate respondsToSelector:@selector(menuTappedAtIndex:)] )
    {
        [self.delegate menuTappedAtIndex:tag];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
