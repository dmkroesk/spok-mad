//
//  PlanningCell.h
//  SpokMainApp
//
//  Created by Jan on 9/26/13.
//  Copyright (c) 2013 Jan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoosterItem.h"

@interface PlanningCell : UITableViewCell
@property RoosterItem * item;

@property (nonatomic, weak) IBOutlet UILabel *activityLabel;
@property (nonatomic, weak) IBOutlet UILabel *timeLabel;
@end
