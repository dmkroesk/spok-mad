//
//  PlanningCell.m
//  SpokMainApp
//
//  Created by Jan on 9/26/13.
//  Copyright (c) 2013 Jan. All rights reserved.
//

#import "PlanningCell.h"

@implementation PlanningCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
