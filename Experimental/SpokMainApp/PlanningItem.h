//
//  PlanningItem.h
//  SpokMainApp
//
//  Created by Jan on 9/26/13.
//  Copyright (c) 2013 Jan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PlanningItem : NSObject

@property NSString *activity;
@property NSString *time;

@end
