//
//  RoosterItem.h
//  9292API
//
//  Created by Remy Baratte on 9/26/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RoosterItem : NSObject

@property (nonatomic, strong) NSString *activity;
@property (nonatomic, strong) NSString *startTime;
@property (nonatomic, strong) NSString *dayOfWeek;
@property (nonatomic, strong) NSString *endTime;

-(id)initWithData:(NSDictionary *)data;

@end
