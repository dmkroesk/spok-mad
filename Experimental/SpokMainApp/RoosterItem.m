//
//  RoosterItem.m
//  9292API
//
//  Created by Remy Baratte on 9/26/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "RoosterItem.h"

@implementation RoosterItem

@synthesize activity, startTime, dayOfWeek, endTime;

-(id)initWithData:(NSDictionary *)data
{
    activity = [data valueForKey:@"ACTIVITEIT"];
    startTime = [data valueForKey:@"BEGIN_TIJD"];
    //dayOfWeek = [data valueForKey:@"DAG"];
    endTime = [data valueForKey:@"EIND_TIJD"];
    return self;
}

@end
