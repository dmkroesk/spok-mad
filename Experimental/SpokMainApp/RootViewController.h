

#import <UIKit/UIKit.h>
#import "MenuTabbarScrollView.h"

@protocol RootViewControllerDelegate <NSObject>

@optional
-(BOOL)handle;
@required
@end

@interface RootViewController : UIViewController <MenuTabbarScrollViewDelegate>

@property (nonatomic, copy) NSArray *viewControllers;
@property (nonatomic, weak) id<RootViewControllerDelegate> delegate;

-(void) setSelectedIndex:(NSUInteger)index animated:(BOOL)animated;
-(void) setSelectedViewController:(UIViewController *)selectedViewController animated:(BOOL)animated;


@end
