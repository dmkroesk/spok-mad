//
//  RootViewController.m
//  ScrollTabbar
//
//  Created by Diederich Kroeske on 9/13/13.
//  Copyright (c) 2013 Diederich Kroeske. All rights reserved.
//

#import "RootViewController.h"
#import "ViewController.h"
#import "MenuTabbarScrollView.h"

static const NSUInteger TAGBASE = 2324;

@interface RootViewController ()
@property (nonatomic, weak)     UIViewController *selectedViewController;
@property (nonatomic, assign)   NSUInteger selectedIndex;
@end

@implementation RootViewController
{
    MenuTabbarScrollView *menubar;    
    UIView *contentContainerView;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // Create tapable scollview with images
    CGRect rect = CGRectMake(0, self.view.bounds.size.height-83,
                             self.view.bounds.size.width, 100);

    menubar = [[MenuTabbarScrollView alloc] initWithFrame:rect];
    menubar.delegate = self;
    [self.view addSubview:menubar];
    
    // Create contentview
    rect.origin.y = 0;
    rect.size.height = self.view.bounds.size.height - 83;
    contentContainerView = [[UIView alloc] initWithFrame:rect];
    contentContainerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    contentContainerView.backgroundColor = [UIColor greenColor];
    [self.view addSubview:contentContainerView];
    
    
    // Load eerste view controller in content
    ViewController *visibleViewController = [_viewControllers objectAtIndex:0];
    visibleViewController.view.frame = contentContainerView.bounds;
    [self addChildViewController:visibleViewController];
    [self.view addSubview:visibleViewController.view];
    [visibleViewController didMoveToParentViewController:self];
    
    _selectedIndex = 0;
    
}

-(void)menuTappedAtIndex:(NSUInteger)index
{
    NSLog(@"menuTappedAtIndex with index %d tapped", index);

    // Fake index omdat er maar 2 ChildViewContollers zijn
    index %= 2;
    
   // ViewController *visibleViewController = [_viewControllers objectAtIndex:0];
    
    //[visibleViewController goToPlanning];
    
    [self setSelectedIndex:index animated:YES];
}


-(void) setSelectedIndex:(NSUInteger)index animated:(BOOL)animated
{
    // Check of huidige viewcontroller al is geselecteerd (meerdere
    // taps op dezelde knop). Dan geen actie
    if( index == _selectedIndex )
        return;
    
    // Andere index geselecteerd, animeer
    ViewController *newViewController = [_viewControllers objectAtIndex:index];

    // Animatie effecten. Kies er maar 1
//    newViewController.view.frame = CGRectMake(100,100,10,10); // Effect 1
    CGRect outsideScreen = contentContainerView.bounds; // Effect 2 - sliding
    outsideScreen.origin.x += contentContainerView.bounds.size.width;
    newViewController.view.frame = outsideScreen;
    
    //
    [self addChildViewController:newViewController];
    
    ViewController *oldViewController = [_viewControllers objectAtIndex:_selectedIndex];
    [oldViewController willMoveToParentViewController:nil];
    
    [self transitionFromViewController:oldViewController
                      toViewController:newViewController
                              duration:0.3
                               options:UIViewAnimationOptionCurveEaseInOut
                            animations:^{
                                [oldViewController.view removeFromSuperview];
                                [self.view addSubview:newViewController.view];
                                newViewController.view.frame = contentContainerView.bounds;
                            }
                            completion:^(BOOL finished){
                                [newViewController didMoveToParentViewController:self];
                                [oldViewController removeFromParentViewController];
                                _selectedIndex = index;
                            }
     ];
    //[self performSegueWithIdentifier:@"ToPlanning" sender:self];
}

-(void) setSelectedViewController:(UIViewController *)selectedViewController animated:(BOOL)animated
{
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
