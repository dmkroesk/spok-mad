//
//  AppDelegate.h
//  SpokMainApp
//
//  Created by Jan on 9/24/13.
//  Copyright (c) 2013 Jan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property NSString *deviceToken;
@end
