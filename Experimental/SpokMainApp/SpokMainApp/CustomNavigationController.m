//
//  CustomNavigationController.m
//  SpokMainApp
//
//  Created by wkroos on 10/1/13.
//  Copyright (c) 2013 Jan. All rights reserved.
//

#import "CustomNavigationController.h"

@interface CustomNavigationController ()

@end

@implementation CustomNavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    //// SPLASH ANIMATION
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenH = screenRect.size.height;
    CGFloat screenW = screenRect.size.width;
    
    //Create SplashImage overlay for transition
    CGRect myImageRect = CGRectMake(0.0f, -screenH, screenW, (screenH)*2);
    UIImageView *myImage = [[UIImageView alloc] initWithFrame:myImageRect];
    [myImage setImage:[UIImage imageNamed:@"AnimatedSplash"]];
    myImage.contentMode = UIViewContentModeScaleToFill;
    [self.view addSubview:myImage];
    
    //Animate from splash screen
    [UIView animateWithDuration: 1.5
                          delay: 0.5
                        options: UIViewAnimationOptionCurveEaseIn
                     animations: ^{
                         myImage.transform = CGAffineTransformMakeTranslation(0,screenH);
                     }
                     completion:nil
     ];
    [UIView animateWithDuration: 0.5
                          delay: 1.0
                        options: UIViewAnimationOptionCurveEaseIn
                     animations: ^{
                         myImage.alpha = 0;
                     }
                     completion:nil
     ];
    
    
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
