//
//  PlanningController.h
//  SpokMainApp
//
//  Created by Jan on 9/26/13.
//  Copyright (c) 2013 Jan. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "PlanningCell.h"
#import "WebService.h"

@interface PlanningController : UIViewController <UITableViewDelegate, UITableViewDataSource,RoosterDelegate>

@property PlanningCell *cell;
@property NSMutableArray * roosterItems;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)settingsBtnClicked:(id)sender;
- (IBAction)backBtnClicked:(id)sender;
- (IBAction)refreshBtnClicked:(id)sender;
@property BOOL isRefreshing;

- (void)refreshTable;

@end
