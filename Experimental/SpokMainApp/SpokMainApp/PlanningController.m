//
//  PlanningController.m
//  SpokMainApp
//
//  Created by Jan on 9/26/13.
//  Copyright (c) 2013 Jan. All rights reserved.
//

#import "PlanningController.h"
#import "AppDelegate.h"
#import "WebService.h"

@interface PlanningController ()
@property (strong, nonatomic) IBOutlet UITextView *Day;
@property (weak, nonatomic) IBOutlet UIButton *BackButton;
@property (weak, nonatomic) IBOutlet UIButton *SettingsButton;
@property (weak, nonatomic) IBOutlet UIButton *RefreshButton;
@property (strong, nonatomic) WebService *webService;

@end

@implementation PlanningController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    //// BACKGROUND IMAGE
    self.tableView.backgroundColor = [UIColor colorWithRed:1 green:0 blue:0 alpha:0];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]];
    
    /*
    [_BackButton setImage:[UIImage imageNamed:@"BackIconHighlight"] forState:UIControlStateSelected];
    [_SettingsButton setImage:[UIImage imageNamed:@"SettingsIconHighlight"] forState:UIControlStateSelected | UIControlStateHighlighted];
    [_RefreshButton setImage:[UIImage imageNamed:@"RefreshIconHighlight"] forState: UIControlStateHighlighted];
    */
    
    _webService = [[WebService alloc]init];
    _webService.roosterDelegate = self;
    [_webService getRoosterWithUserId :DUMMY_ID];
        
    //Signed up for push notifications
    [[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(appDidSignInForPush) name:@"signedUpForPushNotifications" object:nil];
    
    //Display the received push notification
    [[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(didReceivePushNotification:) name:@"receivedPushNotification" object:nil];
    
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section

{
    return [_roosterItems count];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    PlanningCell *cell = (PlanningCell *)[tableView cellForRowAtIndexPath:indexPath];
    _cell = cell;
    
    
    //[self performSegueWithIdentifier:@"Map" sender:self];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 48;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    static NSString *simpleTableIdentifier = @"PlanningCellNib";
    
    
    PlanningCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    
    if (cell == nil) {
        
        cell = [[PlanningCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PlanningInterfaceCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        
    }
    
   // RoosterItem *item  =
    cell.item = [_roosterItems objectAtIndex:indexPath.row];
    
    //cell.textLabel.text = [NSString stringWithFormat:@" %@",cell.item.activity];
    cell.activityLabel.text =cell.item.activity;
    
    NSString *startTime = [[cell.item.startTime substringFromIndex:11]substringToIndex:5];
    NSString *endTime = [[cell.item.endTime substringFromIndex:11]substringToIndex:5];
    
    cell.timeLabel.text = [NSString stringWithFormat:@" %@ - %@", startTime, endTime];
    //cell.dateLabel.text = cell.item.dayOfWeek;
    
    
    //Create background for selected state
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context,
                                   [[UIColor colorWithRed:1 green:1 blue:1 alpha:0] CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    cell.selectedBackgroundView = [[UIImageView alloc] initWithImage:img];
    return cell;
    
}

- (void) reloadTableView{
    
    [self.tableView reloadData];
}


- (void) appDidSignInForPush
{
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    NSString *deviceToken = appDelegate.deviceToken;
    
    
    NSString *str = [NSString stringWithFormat:@"http://84.24.21.108/spok/push/signup.php?device_token=%@", deviceToken];
    NSURL *url = [NSURL URLWithString:(str)];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url
                                                cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                            timeoutInterval:30];
    // Fetch the JSON response
    NSData *urlData;
    NSURLResponse *response;
    NSError *error;
    
    // Make synchronous request
    urlData = [NSURLConnection sendSynchronousRequest:urlRequest
                                    returningResponse:&response
                                                error:&error];
    
    // Construct a String around the Data from the response
    NSString *result = [[NSString alloc] initWithData:urlData encoding:NSUTF8StringEncoding];
    NSLog(@"Signup result: %@", result);
    
    if([result isEqualToString:@"Signed up"]){
        NSLog(@"Signed up");
    } else {
        NSString *errorStr = [error localizedDescription];
        
        NSLog(@"Failed to sign up your device: %@ Error: %@", result, errorStr);
    }
}

- (void)didReceivePushNotification:(NSNotification *) notification
{
    NSLog(@"%@", notification.object);
    NSDictionary *userInfo = (NSDictionary*) notification.object;
    
    NSDictionary* aps = (NSDictionary*) [userInfo objectForKey:@"aps"];
    NSString* alert = (NSString*) [aps objectForKey:@"alert"];
    //NSDictionary *userInfo = [notification userInfo];
    NSLog(@"Received push notification: %@ ", alert);
    
    
    [self refreshTable];
}

- (void)updateTable:(id)sender {
    [_RefreshButton.imageView.layer removeAllAnimations];
    
    _roosterItems = ((WebService*)sender ).roosteritems;
    [self.tableView reloadData];
}

- (IBAction)settingsBtnClicked:(id)sender {
    [_RefreshButton.imageView stopAnimating];
    //[_RefreshButton.imageView ]
    //_isRefreshing = NO;
}

- (IBAction)backBtnClicked:(id)sender {
}

- (IBAction)refreshBtnClicked:(id)sender {
    [self refreshTable];
}

- (void)refreshTable {
    
    _isRefreshing = YES;
    
    [UIView animateWithDuration:3.0
                          delay:0
                        options:( UIViewAnimationOptionCurveLinear |
                                 UIViewAnimationOptionRepeat)
                     animations:^{
                         //_RefreshButton.alpha = 0.0;
                         _RefreshButton.imageView.transform = CGAffineTransformMakeRotation(179*0.0174532925);
                         
                     }
                     completion:^(BOOL finished){
                         //return;
                     }
     ];
    
    //[_RefreshButton.imageView startAnimating];
    
    //[_RefreshButton.imageView addAnimation:refreshAnimation forKey:@"refreshAnimation"];
    
    [_webService getRoosterWithUserId :DUMMY_ID];
}

     
@end
