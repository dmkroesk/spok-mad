//
//  ViewController.h
//  SpokMainApp
//
//  Created by Jan on 9/24/13.
//  Copyright (c) 2013 Jan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
-(void)goToPlanning;

@end
