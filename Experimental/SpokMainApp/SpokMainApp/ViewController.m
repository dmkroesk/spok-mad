//
//  ViewController.m
//  SpokMainApp
//
//  Created by Jan on 9/24/13.
//  Copyright (c) 2013 Jan. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

-(void)willMoveToParentViewController:(UIViewController *)parent
{
    NSLog( @"%@ (%p) - %@", NSStringFromClass([self class]), self, NSStringFromSelector(_cmd) );
}

-(void)didMoveToParentViewController:(UIViewController *)parent
{
    NSLog( @"%@ (%p) - %@", NSStringFromClass([self class]), self, NSStringFromSelector(_cmd) );
}

-(void)viewDidAppear:(BOOL)animated
{
    NSLog( @"%@ (%p) - %@", NSStringFromClass([self class]), self, NSStringFromSelector(_cmd) );
}

-(void)viewWillAppear:(BOOL)animated
{
    NSLog( @"%@ (%p) - %@", NSStringFromClass([self class]), self, NSStringFromSelector(_cmd) );
    
    //[super viewWillAppear:animated];
    
    NSLog(@"gradient");
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.view.bounds;
    UIColor *top = [UIColor colorWithRed:(56/255.0) green:(99/255.0) blue:(126/255.0) alpha:1.0];
    UIColor *bottom = [UIColor colorWithRed:(10/255.0)  green:(18/255.0)  blue:(23/255.0)  alpha:1.0];
    gradient.colors = [NSArray arrayWithObjects:(id)top.CGColor, (id)bottom.CGColor, nil];
    [self.view.layer addSublayer:gradient];
    
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    NSLog( @"%@ (%p) - %@", NSStringFromClass([self class]), self, NSStringFromSelector(_cmd) );
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    NSLog( @"%@ (%p) - %@", NSStringFromClass([self class]), self, NSStringFromSelector(_cmd) );
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    /// SPLASH ANIMATION
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenH = screenRect.size.height;
    CGFloat screenW = screenRect.size.width;
    
    //Create SplashImage overlay for transition
    CGRect myImageRect = CGRectMake(0.0f, -screenH-20, screenW, (screenH)*2);
    UIImageView *myImage = [[UIImageView alloc] initWithFrame:myImageRect];
    [myImage setImage:[UIImage imageNamed:@"animated-splash.png"]];
    myImage.contentMode = UIViewContentModeScaleToFill;
    [self.view addSubview:myImage];
    
    //Animate from splash screen
    [UIView animateWithDuration: 1.5
                          delay: 0
                        options: UIViewAnimationOptionCurveEaseIn
                     animations: ^{
                         myImage.transform = CGAffineTransformMakeTranslation(0,screenH);                     }
                     completion:nil
     ];
    [UIView animateWithDuration: 0.5
                          delay: 0.5
                        options: UIViewAnimationOptionCurveEaseIn
                     animations: ^{
                         myImage.alpha = 0;
                     }
                     completion:nil
     ];
    
    //END OF SPLASH ANIMATION

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
}

-(void)goToPlanning{
[self performSegueWithIdentifier:@"ToPlanning" sender:self];
}


@end
