//
//  WebService.h
//  9292API
//
//  Created by Remy Baratte on 9/26/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <Foundation/Foundation.h>

//#define ROOSTER_URL @"http://145.48.6.37/index.php/api/?method=Rooster&format=json"
//#define ROOSTER_USER_URL @"http://145.48.6.37/index.php/api/?method=Rooster&User_id=%@&format=json"

#define ROOSTER_URL @"http://84.24.21.108/spok/index.php/api/?method=Rooster&format=json"
#define ROOSTER_USER_URL @"http://84.24.21.108/spok/index.php/api/?method=Rooster&User_id=%@&format=json"
#define DUMMY_ID @"2"

@protocol RoosterDelegate <NSObject>
- (void)updateTable:(id)sender;

@end

@interface WebService : NSObject <NSURLConnectionDelegate>

-(void)getRoosterWithUserId:(NSString *)userId;
-(void)getRooster;
@property NSMutableArray * roosteritems;
@property (nonatomic, weak) id<RoosterDelegate>  roosterDelegate;

@end
