//
//  WebService.m
//  9292API
//
//  Created by Remy Baratte on 9/26/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "WebService.h"
#import "RoosterItem.h"
@implementation WebService

-(id)init
{
    if(self = [super init])
    {
        return self;
    }
    return nil;
}

-(void)getRoosterWithUserId:(NSString *)userId
{
    NSString *string = [NSString stringWithFormat:ROOSTER_USER_URL, userId];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:string]];
    NSURLConnection *roosterConnection = [NSURLConnection connectionWithRequest:request delegate:self];
    [roosterConnection start];
}

-(void)getRooster
{
    NSString *string = [NSString stringWithFormat:ROOSTER_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:string]];
    NSURLConnection *roosterConnection = [NSURLConnection connectionWithRequest:request delegate:self];    [roosterConnection start];
}

#pragma mark NSURLConnectionDelegate

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSLog(@"%@", [response description]);
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    _roosteritems = [[NSMutableArray alloc]init];
    
    NSError *error;
    NSArray *jsonData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    if(error != nil)
    {
        NSLog(@"Error: %@", error);
    }
    else
    {
        NSLog(@"Data: %@", [jsonData description]);
        for(NSDictionary *dict in jsonData)
        {
            RoosterItem *temp = [[RoosterItem alloc] initWithData:dict];
            [ _roosteritems addObject:temp];
        }
        [self.roosterDelegate  updateTable:self];
    }
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"Error: %@", [error description]);
    
    
    
    //activity = [data valueForKey:@"ACTIVITEIT"];
    //startTime = [data valueForKey:@"BEGIN_TIJD"];
    //dayOfWeek = [data valueForKey:@"DAG"];
    //endTime = [data valueForKey:@"EIND_TIJD"];
    
    _roosteritems = [[NSMutableArray alloc]init];
    NSDictionary *item1Dict = [[NSDictionary alloc] initWithObjectsAndKeys:@"Kapper", @"ACTIVITEIT", @"2013-10-06 14:00:00", @"BEGIN_TIJD", @"Donderdag", @"DAG", @"2013-10-06 14:20:00", @"EIND_TIJD",  nil];
    
    RoosterItem *item1 = [[RoosterItem alloc] initWithData:item1Dict];
    [ _roosteritems addObject:item1];
    [self.roosterDelegate  updateTable:self];
}

@end
