<?php
class Account {
    private $id = null;
    private $user = null;
    private $session = null;
    //private $lastActivity = null;
    private $authorization = null;
    
    private $validUser = 'Elle';
    private $validPass = 'Elle';
	
    public function __construct(){
        
    }
    
    public function login($user, $pass){
        global $core;
        if($user==$this->validUser && $pass=$this->validPass){
            $session = '1234';
            setcookie('session', $session, time()+3600*24*30, '/', cookie_domain);
            return true;
        }
        
        /*
        $db = $core->getDatabase();
        $result = $db->select('SELECT * FROM account WHERE user="'.$user.'" AND pass="'.$pass.'"');
        if($result){
            $acc = $result[0];
            
            $now = getTimestamp();
            if(!$db->edit('UPDATE account SET last_activity="'.$now.'" WHERE user="'.$user.'" '))
                return false;
            
            do {
                $session = generateRandomString(40);
            } while($db->select('SELECT * FROM account_session WHERE session="'.$session.'"'));
            
            if(!$db->edit('INSERT INTO account_session (account_id, session) VALUES('.$acc['id'].', "'.$session.'")'))
                    return false;
            
            $this->id = $acc['id'];
            $this->user = $acc['user'];
            $this->session = $session;
            setcookie('session', $session, time()+3600*24*30, '/', 'dev.wkict.nl');
            $this->lastActivity = $now;
            $this->authorization = $this->convertAuthorization($result[0]['authorization']);
            return true;
        }*/
        return false;
    }
    
    public function initWithSession($session){
        //global $core;
	if($session=='1234'){
            $this->id = $this->validUser;
            $this->user = $this->validPass;
            $this->session = $session;
            setcookie('session', $session, time()+3600*24*30, '/', cookie_domain);
            $this->authorization = $this->convertAuthorization('member');
            return true;
	} else {
            setcookie ("session", "", time() - 3600, '/', cookie_domain);
        }
		/*
        $db = $core->getDatabase();
        $result = $db->select('SELECT account_id FROM account_session WHERE session="'.$session.'"');
        if($result!=false){
            $result = $db->select('SELECT * FROM account WHERE id='.$result[0]['account_id']);
            
            $now = getTimestamp();
            if(!$db->edit('UPDATE account SET last_activity="'.$now.'" WHERE user="'.$result[0]['user'].'" '))
                return false;
            
            $this->id = $result[0]['id'];
            $this->user = $result[0]['user'];
            $this->session = $session;
            setcookie('session', $session, time()+3600*24*30, '/', 'dev.wkict.nl');
            $this->lastActivity = $now;
            $this->authorization = $this->convertAuthorization($result[0]['authorization']);
            return true;
        }
		*/
        return false;
    }
    
    private function convertAuthorization($string){
        if($string=='banned'){
            return Authorization::Banned;
        } elseif ($string=='member') {
            return Authorization::Members;
        } elseif ($string=='admin') {
            return Authorization::Admin;
        }
    }
    
    public function logout() {
        global $core;
        if($this->session != null){
            //$db = $core->getDatabase();
            //$db->edit('DELETE FROM account_session WHERE session="'.$this->session.'"');
            setcookie ("session", "", time() - 3600, '/', cookie_domain);
            return true;
        } else {
            return false;
        }
    }
    
    public function getId(){
        return $this->id;
    }
    
    public function getUser(){
        return $this->user;
    }
    
    public function getSession(){
        return $this->session;
    }
    
    public function getLastActivity(){
        return $this->lastActivity;
    }
    
    public function getAuthorization(){
        return $this->authorization;
    }
}