<?php
class Core {
    private $status = array(
                            'loaded'=>false, 
                            'unloaded'=>false
                        );
    private $database = null;
    private $settings = null;
    private $account = null;
    
    public function __construct(){
        $this->account = new Account();
        
        $base_dir = '/SPoK/WebService';
        $this->settings = array(
            'access' => Authorization::Everyone
         );
    }
    
    public function getSetting($name){
        if(isset($this->settings[$name])){
            return $this->settings[$name];
        } else {
            $this->throwError('Setting "' . $name . " doesn\'t exist.");
        }
    }
    
    public function setSetting($name, $value){
        if(gettype($this->settings[$name]) == gettype($value)){
            $this->settings[$name] = $value;
        } else {
            $this->throwError('ERROR: Setting "' . $name
                    . '" can\'t be change to a value of type "'
                    . gettype($value) . '" because it is instantiated '
                    . 'with a value of another type "'
                    . gettype($this->settings[$name]) . '".');
        }
    }
    
    public function load(){
        if($this->status['loaded']){
            $this->throwError('You can\'t call the load function twice.');
        }
        
        if(isset($_COOKIE['session'])){
            $this->account->initWithSession($_COOKIE['session']);
        }
        $this->checkAccess();
        
        /*
        switch ($this->settings['access']){
            case Authorization::Members:
                break;
            case Authorization::Admins:
                break;
            case Authorization::Everyone:
            case Authorization::Banned:
                break;
        }
        */
        $this->status['loaded'] = true;
    }
    
    private function checkAccess(){
        switch ($this->settings['access']){
            case Authorization::Members:
                if(!($this->account->getAuthorization()>=Authorization::Members)){
                    $this->throwError('Only members can view this information.');
                }
                break;
            case Authorization::Admins:
                if(!($this->account->getAuthorization()==Authorization::Admins)){
                    $this->throwError('Only admins can view this information.');
                }
                break;
            case Authorization::Everyone:
                //Everyone can access this page so it's fine
                break;
            case Authorization::Banned:
                if(!($this->account->getAuthorization()==Authorization::Banned)){
                    $this->throwError('You have been banned');
                }
                break;
            default:
                $this->throwError('This page has an unknown authorization level');
        }
    }
    
    public function unload(){
		if(!$this->status['loaded']){
			$this->throwError('Core has to be loaded first');
		}
	
        if($this->database != null){
            $this->database->close();
        }
        $this->status['unloaded'] = true;
    }
    
    public function getDatabase(){
        if($this->database == null){
            $this->database = new Database();
        }
        return $this->database;
    }
    
    public function getAccount(){
        return $this->account;
    }
    
    public function setAccount($account){
        $this->account = $account;
    }
	
    public function throwError($text){
            $json = new stdClass();
            $json->error =  $text;
            die(json_encode($json));
    }
}