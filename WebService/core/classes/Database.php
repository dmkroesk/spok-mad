<?php
class Database {
	public $host = 'localhost';
	public $user = 'spok';
	public $pass = 'spokdbpass';
	public $name = 'spok';
	public $conn;
	
	public function Database() {
		$this->conn = mysqli_connect($this->host,$this->user,$this->pass);
		if (!$this->conn) {
			die('Could not connect: ' . $this->conn->error());
		}
		
		$this->conn->select_db($this->name) 
		or die('Could not connect: ' . $this->conn->error());
	}
	
	public function close() {
		$this->conn->close();
	}
	
	/*** SELECT ***/
	
	public function select($query) {
		$query = $query;
		$results = array();
		$result = $this->conn->query($query);
		if($result){
			while($row = mysqli_fetch_array($result)){
				array_push($results, $row);
			}
		}
		
		if(count($results)!=0){
			return $results;
		} else {
			return false;
		}
	}
	
	
	/*** EDIT ***/
	
	public function edit($query) {
		$query = $query;
		$result = $this->conn->query($query);
		if($result){
			return true;
		} else {
			return false;
		}
	}
}
?>