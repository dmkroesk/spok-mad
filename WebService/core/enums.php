<?php
final class Authorization
{
    const Banned = -1;
    const Everyone = 0;
    const Members = 1;
    const Admins = 2;
}

define('cookie_domain', '');
//define('cookie_domain', 'dev.wkict.nl');