<?php
$json = new stdClass;

if(isset($_GET['user']) && isset($_GET['pass'])){
    include('core/init.php');
    $core->load();

    $user = $_GET['user'];
    $pass = $_GET['pass'];

    if($core->getAccount()->login($user, $pass)){
        $json->logged_in = true;
    } else {
        $json->logged_in = 'Wrong username and/or password';
    }

    $core->unload();
} else {
    $json->error = 'Missing user and pass parameters';
}

echo json_encode($json);