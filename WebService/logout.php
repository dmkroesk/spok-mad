<?php
$json = new stdClass;
include('core/init.php');
$core->load();

if($core->getAccount()->getId() == null){
    $json->error = "Already logged out";
} else {
    if($core->getAccount()->logout()){
        $json->success = true;
    } else {
        $json->error = "Couldn't logout";
    }
}

$core->unload();
echo json_encode($json);