<?php
$json = new stdClass;
include('core/init.php');
$core->setSetting('access', Authorization::Members);
$core->load();

$json->rooster = array();

$les1 = new stdClass();
$les1->uur = 1;
$les1->les = 'Biologie';
$les1->lokaal = 'HA101';
$les1->leraar = 'D. Kroeske';
array_push($json->rooster, $les1);

$les2 = new stdClass();
$les2->uur = 2;
$les2->les = 'Wiskunde';
$les2->lokaal = 'HE523';
$les2->leraar = 'D. Kroeske';
array_push($json->rooster, $les2);

$core->unload();

echo json_encode($json);