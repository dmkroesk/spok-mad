
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// AFNetworking
#define COCOAPODS_POD_AVAILABLE_AFNetworking
#define COCOAPODS_VERSION_MAJOR_AFNetworking 0
#define COCOAPODS_VERSION_MINOR_AFNetworking 9
#define COCOAPODS_VERSION_PATCH_AFNetworking 1

// DCIntrospect-ARC
#define COCOAPODS_POD_AVAILABLE_DCIntrospect_ARC
#define COCOAPODS_VERSION_MAJOR_DCIntrospect_ARC 0
#define COCOAPODS_VERSION_MINOR_DCIntrospect_ARC 0
#define COCOAPODS_VERSION_PATCH_DCIntrospect_ARC 6

