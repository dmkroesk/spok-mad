//
//  AppDelegate.m
//  SpoK
//
//  Created by atmstudent on 08/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "AppDelegate.h"
#import <DCIntrospect.h>
#import "StudyCallBerichtenViewController.h"
#import "StudyCallKalenderViewController.h"

@implementation AppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSLog(@"Method: didFinishLaunchingWithOptions");
    
    // Let the device know we want to receive push notifications
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
     (UIRemoteNotificationTypeNewsstandContentAvailability | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    
    // Override point for customization after application launch.
#if TARGET_IPHONE_SIMULATOR
    [[DCIntrospect sharedIntrospector] start];
#endif
   /* if([self checkLogin])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        StudyCallBerichtenViewController *berichtenViewController = [storyboard instantiateViewControllerWithIdentifier:@"BerichtenCentrum"];
        UINavigationController *navigationController=[[UINavigationController alloc] initWithRootViewController:berichtenViewController];
        self.window.rootViewController =nil;
        self.window.rootViewController = navigationController;
    }*/
    bool berichtstart = [[NSUserDefaults standardUserDefaults]boolForKey:@"ShouldStartAtBerichten"];
    NSLog(@"Start at berichtenscherm %d", berichtstart);
    
    if(berichtstart)
    {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            StudyCallBerichtenViewController *berichtenViewController = [storyboard instantiateViewControllerWithIdentifier:@"BerichtenCentrum"];
            UINavigationController *navcontroller = (UINavigationController*)self.window.rootViewController;
            [navcontroller pushViewController:berichtenViewController animated:NO];
            //NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            //[defaults setBool:NO forKey:@"ShouldStartAtBerichten"];
            //[defaults synchronize];
    }
    return YES;
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    NSLog(@"Method: applicationWillEnterForeground");
    /*
    bool berichtstart = [[NSUserDefaults standardUserDefaults]boolForKey:@"ShouldStartAtBerichten"];
    NSLog(@"Start at berichtenscherm %d", berichtstart);
    
    
    if(berichtstart)
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        StudyCallBerichtenViewController *berichtenViewController = [storyboard instantiateViewControllerWithIdentifier:@"BerichtenCentrum"];
        UINavigationController *navcontroller = (UINavigationController*)self.window.rootViewController;
        [navcontroller pushViewController:berichtenViewController animated:NO];
        //NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        //[defaults setBool:NO forKey:@"ShouldStartAtBerichten"];
        //[defaults synchronize];
    }*/
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult result))handler
{
    NSLog(@"Method: didReceiveRemoteNotification");
    
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    
    if (state == UIApplicationStateBackground){
        NSLog(@"State: UIApplicationStateBackground");
    }
    if (state == UIApplicationStateInactive){
        NSLog(@"State: UIApplicationStateInactive");
    }
    
    if (state == UIApplicationStateBackground || state == UIApplicationStateInactive)
    {
        //Retrieve the last latest available stuff and set a flag that we need to navigate to the message center's last msg
        NSLog(@"Received push notification in background mode");
        //ga naar het berichtenscherm
       // NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
       // [defaults setBool:YES forKey:@"ShouldStartAtBerichten"];
       // [defaults synchronize];
        //Success+
        handler(UIBackgroundFetchResultNewData);
        
        //// GA NAAR BERICHTENSCHERMS ALS WE ZIJN INGELOGD, ANDERS KUNNEN WE GEEN NOTIFICATIES ONTVANGENS
        
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        StudyCallBerichtenViewController *berichtenViewController = [storyboard instantiateViewControllerWithIdentifier:@"BerichtenCentrum"];
        UINavigationController *navcontroller = (UINavigationController*)self.window.rootViewController;
        [navcontroller pushViewController:berichtenViewController animated:NO];
    }
    else{
        NSLog(@"Received push notification in active mode");
        UINavigationController *navController = (UINavigationController *)self.window.rootViewController;
        UIViewController *activeView = [navController topViewController];
        NSDictionary *alertValues = [userInfo valueForKey:@"aps"];
        NSString *pushMessage = [alertValues valueForKey:@"alert"];
        
        if([activeView class] == [StudyCallKalenderViewController class])
        {
             UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            StudyCallBerichtenViewController *berichtenViewController = [storyboard instantiateViewControllerWithIdentifier:@"BerichtenCentrum"];
            [berichtenViewController getNewRoosterDataRequest];
            
            _kalenderView = (StudyCallKalenderViewController *) activeView;
            
            [self showAlert:pushMessage];
        }else if([activeView class] == [StudyCallBerichtenViewController class])
        {
            StudyCallBerichtenViewController *berichtenController = (StudyCallBerichtenViewController *)activeView;
            [berichtenController viewDidAppear:NO]; //Zorgt ervoor dat de berichten herladen worden?
            [self showAlert:pushMessage];
            
        }
        else{
            [self showAlert:pushMessage];
        }
    
    }

}

-(void)showAlert:(NSString *)message
{
    UIAlertView *pushAlert = [[UIAlertView alloc]initWithTitle:@"Roosterwijziging" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [pushAlert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [_kalenderView initView];
    }
}

-(BOOL)checkLogin
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults objectForKey:@"username"];
    if([username length] == 0 )
    {
        return NO;
    }
    else {
        return YES;
    }
}


- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
	NSLog(@"My token is: %@", deviceToken);
    NSString *tokenAsString = [[[deviceToken description]
                                 stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]]
                                stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:tokenAsString forKey:@"deviceToken"];
    [defaults synchronize];
    
    /*NSString *str = [NSString stringWithFormat:@"http://84.24.21.108/spok/push_studycall/signup.php?device_token=%@", tokenAsString];
    NSURL *url = [NSURL URLWithString:(str)];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url
                                                cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                            timeoutInterval:30];
    // Fetch the JSON response
    NSData *urlData;
    NSURLResponse *response;
    NSError *error;
    
    // Make synchronous request
    urlData = [NSURLConnection sendSynchronousRequest:urlRequest
                                    returningResponse:&response
                                                error:&error];
    
    // Construct a String around the Data from the response
    NSString *result = [[NSString alloc] initWithData:urlData encoding:NSUTF8StringEncoding];
    NSLog(@"Signup result: %@", result);
    
    if([result isEqualToString:@"Signed up"]){
        NSLog(@"Signed up device succesful");
    } else {
        NSString *errorStr = [error localizedDescription];
        
        NSLog([[[@"Failed to sign up your device: " stringByAppendingString:result] stringByAppendingString:@" Error: "] stringByAppendingString:errorStr]);
    }*/
}

- (void)sendNotification
{
    NSLog(@"Sended push notification");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"signedUpForPus	hNotifications" object:nil];
}




- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
	NSLog(@"Failed to get token, error: %@", error);
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    NSLog(@"Method: applicationWillResignActive");
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    NSLog(@"Method: applicationDidEnterBackground");
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    NSLog(@"Method: applicationDidBecomeActive");
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    NSLog(@"Method: applicationWillTerminate");
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [self saveContext];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"StudyCallData" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"test.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


@end
