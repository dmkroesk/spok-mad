//
//  AdvertManager.h
//  SpoK
//
//  Created by atmstudent on 16/12/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AdvertManager : NSObject

@property (nonatomic, strong) NSArray *adCategories;

+ (AdvertManager*)sharedManager;

- (NSURL*)getAdvertURLForStudentId:(NSString *)studentId;
- (void)changeCategoryEnabledAtIndex:(NSInteger)index toValue:(BOOL)newValue;

@end
