//
//  AdvertManager.m
//  SpoK
//
//  Created by atmstudent on 16/12/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "AdvertManager.h"
#import "TestDataGetter.h"
#import "AdCategory.h"
#import "ServerDataGetter.h"
#import "WebRequestHandler.h"
#import "ChangeAdSettingsWebRequest.h"

@implementation AdvertManager
@synthesize adCategories;

+ (AdvertManager*)sharedManager {
    static AdvertManager *sharedDataManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedDataManager = [[self alloc] init];
    });
    return sharedDataManager;
}

- (id)init {
    if (self = [super init]) {
        adCategories = [TestDataGetter getAdvertentieCategorien];
    }
    return self;
}

- (NSURL*)getAdvertURLForStudentId:(NSString *)studentId
{
    return [NSURL URLWithString:@"http://wapkaimage.com/100071/100071617_6b0fde43eb.gif"];
}

- (void)changeCategoryEnabledAtIndex:(NSInteger)index toValue:(BOOL)newValue
{
    AdCategory *category = [adCategories objectAtIndex:index];
    [category setEnabled:newValue];
    ChangeAdSettingsWebRequest *request = [ChangeAdSettingsWebRequest initWithStudentId:@"112" category:category.name enabled:newValue];
    [[WebRequestHandler sharedWebRequestHandler] executeGET:request callBack:nil];
}
@end
