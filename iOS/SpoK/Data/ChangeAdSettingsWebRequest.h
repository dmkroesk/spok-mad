//
//  ChangeAdSettingsWebRequest.h
//  SpoK
//
//  Created by Tim  on 1/6/14.
//  Copyright (c) 2014 atmstudent. All rights reserved.
//

#import "WebRequest.h"

@interface ChangeAdSettingsWebRequest : WebRequest

+ (id)initWithStudentId:(NSString*)studentId category:(NSString*)category enabled:(BOOL)enabled;

@end
