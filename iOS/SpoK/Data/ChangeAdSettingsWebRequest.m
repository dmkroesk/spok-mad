//
//  ChangeAdSettingsWebRequest.m
//  SpoK
//
//  Created by Tim  on 1/6/14.
//  Copyright (c) 2014 atmstudent. All rights reserved.
//

#import "ChangeAdSettingsWebRequest.h"

@implementation ChangeAdSettingsWebRequest

+ (id)initWithStudentId:(NSString*)studentId category:(NSString*)category enabled:(BOOL)enabled
{
    WebRequest *request = [[WebRequest alloc] initWithURL: [NSURL URLWithString:[NSString stringWithFormat:@"https://145.48.6.12/updateadverts.php?studentnummer=%@&categorie_naam=%@&enabled=%d", studentId, category, enabled]]];
    if(request)
    {
        //customization

    }
    
    return request;
}

@end
