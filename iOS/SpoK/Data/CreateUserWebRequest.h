//
//  CreateUserWebRequest.h
//  SpoK
//
//  Created by Tim  on 1/6/14.
//  Copyright (c) 2014 atmstudent. All rights reserved.
//

#import "WebRequest.h"

@interface CreateUserWebRequest : WebRequest

+ (id)initWithStudentId:(NSString*)studentId firstname:(NSString*)firstname insertion:(NSString*)insertion lastname:(NSString*)lastname email:(NSString*)email dateOfBirth:(NSString*)dob school:(NSString*)school password:(NSString*)password;

@end
