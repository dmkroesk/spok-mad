//
//  CreateUserWebRequest.m
//  SpoK
//
//  Created by Tim  on 1/6/14.
//  Copyright (c) 2014 atmstudent. All rights reserved.
//

#import "CreateUserWebRequest.h"

@implementation CreateUserWebRequest

+ (id)initWithStudentId:(NSString*)studentId firstname:(NSString*)firstname insertion:(NSString*)insertion lastname:(NSString*)lastname email:(NSString*)email dateOfBirth:(NSString*)dob school:(NSString*)school password:(NSString*)password
{
    WebRequest *request = [[WebRequest alloc] initWithURL: [NSURL URLWithString:[[NSString stringWithFormat:@"https://145.48.6.12/createuser.php?studentnummer=%@&voornaam=%@&tussenvoegsel=%@&achternaam=%@&email=%@&geboortedatum=%@&school=%@&wachtwoord=%@",studentId, firstname, insertion, lastname, email, dob, school, password] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    NSLog(@"%@", request);
    if(request)
    {
        //customization
        
    }
    
    return request;
}

@end
