//
//  DataInterface.h
//  SpoK
//
//  Created by atmstudent on 08/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DataGetter <NSObject>

@required
-(NSDictionary*)getData;

@optional
-(void)getStudyCallDataFromStudentID:(NSString *)StudentID;
-(void)getStudyCallScholen;
-(void)logInToStudyCallWithStudentnummer:(NSString *)studentnummer andPassword:(NSString *)password;
-(void)setSchoolData;
-(NSMutableArray *)getDataIndex;
-(NSMutableDictionary *)getDataObjects;

-(NSArray *)getLocalStudyCallDataFromCore:(NSString *)StudentID;
-(NSArray *)getLocalClusterData:(NSString *)StudentID;
-(NSArray *)GetLocalScheduleFromClusters:(NSString *)StudentID clusterID:(NSArray *)clusterList;
-(NSArray *)getSingleClusterItems:(NSArray *)roosterItems group:(NSString *)groupName;


//Test Methods
-(NSMutableArray *)getDataIndexTest;
-(NSMutableDictionary *)getDataObjectsTest;
+(NSArray *) getAdvertentieCategorien;
+(NSArray *) getAvansKlassen;
+(NSArray *) getStudyCallBerichten;
+(NSArray *) getStudyCallKalender;
+(NSString *)getDayOfWeek;
+(NSArray *)getClusters;
+(NSString *)getAdvertURL;

@end
