//
//  DataManager.h
//  SpoK
//
//  Created by atmstudent on 08/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServerDataGetter.h"
#import "AppDelegate.h"
#import "LocalDataGetter.h"


@interface DataManager : NSObject <DataGetterDelegate>


//-(void)saveAccountDataWithStudentnummer:(NSString *)studentnummer andSessionToken:(NSString *)token;
-(void)fillLocalStudyCallData:(RoosterItem *)roosterData;
-(void)fillLocalStudyCallScholen:(NSString *)schoolItem;
+ (DataManager*)sharedManager;

@property ServerDataGetter *serverData;
@property LocalDataGetter *localData;
@property (nonatomic, strong) NSManagedObject *loggedUser;



@end
