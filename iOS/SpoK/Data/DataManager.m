//
//  DataManager.m
//  SpoK
//
//  Created by atmstudent on 08/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "DataManager.h"
#import "DataGetter.h"
#import "RoosterItem.h"

@implementation DataManager

@synthesize serverData,localData;

+ (DataManager*)sharedManager {
    static DataManager *sharedDataManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedDataManager = [[self alloc] init];
    });
    return sharedDataManager;
}

- (id)init {
    if (self = [super init]) {
        serverData = [[ServerDataGetter alloc]init];
        localData = [[LocalDataGetter alloc]init];
        serverData.delegate = self;
    }
    return self;
}


-(void)fillLocalStudyCallData:(RoosterItem *)roosterItem
{
    AppDelegate *appDelegate =
    [[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context =
    [appDelegate managedObjectContext];
    NSManagedObject *newRoosterItem;
    
    newRoosterItem = [NSEntityDescription
                  insertNewObjectForEntityForName:@"Rooster"
                  inManagedObjectContext:context];
    
    [newRoosterItem setValue: roosterItem.studentNr forKey:@"studentnummer"];
    [newRoosterItem setValue: roosterItem.vak forKey:@"vak"];
    [newRoosterItem setValue: roosterItem.lokaal forKey:@"lokaal"];
    [newRoosterItem setValue: roosterItem.groep forKey:@"groep"];
    [newRoosterItem setValue: roosterItem.lesuur forKey:@"lesuur"];
    [newRoosterItem setValue: roosterItem.beginDatumTijd forKey:@"begindatumtijd"];
    [newRoosterItem setValue: roosterItem.eindDatumTijd forKey:@"einddatumtijd"];
    [newRoosterItem setValue: roosterItem.docentNaam forKey:@"docent"];
    
        NSError *error;
    [context save:&error];

}


-(void)fillLocalStudyCallScholen:(NSString *)schoolItem
{
    AppDelegate *appDelegate =
    [[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context =
    [appDelegate managedObjectContext];
    NSManagedObject *schoolEntity;
    
    schoolEntity = [NSEntityDescription
                      insertNewObjectForEntityForName:@"School"
                      inManagedObjectContext:context];
    
    [schoolEntity setValue:schoolItem forKey:@"naam"];

    
    NSError *error;
    [context save:&error];
    
}


@end
