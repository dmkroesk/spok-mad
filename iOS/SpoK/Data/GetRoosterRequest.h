//
//  GetRoosterRequest.h
//  SpoK
//
//  Created by Dennis Keldermans on 1/7/14.
//  Copyright (c) 2014 atmstudent. All rights reserved.
//

#import "WebRequest.h"

@interface GetRoosterRequest : WebRequest

+ (id) initWithStudentId:(NSString*)studentId;

@end
