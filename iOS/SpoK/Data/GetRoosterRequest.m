//
//  GetRoosterRequest.m
//  SpoK
//
//  Created by Dennis Keldermans on 1/7/14.
//  Copyright (c) 2014 atmstudent. All rights reserved.
//

#import "GetRoosterRequest.h"

@implementation GetRoosterRequest

+ (id) initWithStudentId:(NSString*)studentId
{
    WebRequest *request = [[WebRequest alloc] initWithURL: [NSURL URLWithString:[NSString stringWithFormat:@"https://145.48.6.12/getrooster.php?studentnummer=%@", studentId]]];
    if(request)
    {
        //customization
        
    }
    
    return request;

}

@end
