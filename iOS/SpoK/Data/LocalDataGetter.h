//
//  LocalDataGetter.h
//  SpoK
//
//  Created by atmstudent on 08/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataGetter.h"

@interface LocalDataGetter : NSObject <DataGetter>

@property NSMutableArray *arrayOfCharacters;
@property NSMutableDictionary *objectsForCharacters;

@end
