//
//  LocalDataGetter.m
//  SpoK
//
//  Created by atmstudent on 08/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "LocalDataGetter.h"
#import "RoosterItem.h"
#import "AppDelegate.h"


@implementation LocalDataGetter

@synthesize arrayOfCharacters,objectsForCharacters;

-(NSDictionary*) getData
{
    return nil;
}

-(NSArray *)getLocalStudyCallDataFromCore:(NSString *)StudentID
{
    NSMutableArray *roosterItems = [[NSMutableArray alloc]init];
    
    AppDelegate *appDelegate =
    [[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *contextAdd =
    [appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Rooster"
                inManagedObjectContext:contextAdd];

    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSSortDescriptor *sortyByBegintijd = [[NSSortDescriptor alloc] initWithKey:@"begindatumtijd" ascending:YES];
    [request setSortDescriptors:[NSArray arrayWithObject:sortyByBegintijd]];
    
    NSManagedObject *matches = nil;
    
    NSError * error = nil;
    
    NSArray *objects = [contextAdd executeFetchRequest:request
                                              error:&error];
    
    if ([objects count] == 0) {
        NSLog(@"No matches");
    } else {
        int day = 0;
        for(matches in objects)
        {
            RoosterItem *item = [[RoosterItem alloc]init];
            item.studentNr = [matches valueForKey:@"studentnummer"];
            item.docentNaam = [matches valueForKey:@"docent"];
            item.vak = [matches valueForKey:@"vak"];
            item.lesuur = [matches valueForKey:@"lesuur"];
            item.groep = [matches valueForKey:@"groep"];
            
            item.eindDatumTijd = [matches valueForKey:@"einddatumtijd"];
            item.beginDatumTijd = [matches valueForKey:@"begindatumtijd"];
            item.lokaal = [matches valueForKey:@"lokaal"];
            
            
            
            if(roosterItems.count == day || ![self isSameDay:((RoosterItem *)[[roosterItems objectAtIndex:day] objectAtIndex:0]) as:item] ){
                day = roosterItems.count;
                [roosterItems addObject:[[NSMutableArray alloc]init]];
            }
            [[roosterItems objectAtIndex:day] addObject:item];
        }
    }
    
    return roosterItems;
}

-(NSArray *)getLocalClusterData:(NSString *)StudentID
{
    NSMutableArray *clusterList = [[NSMutableArray alloc]init];
    
    AppDelegate *appDelegate =
    [[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *contextAdd =
    [appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Rooster"
                inManagedObjectContext:contextAdd];
    
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    [request setResultType:NSDictionaryResultType];
    [request setPropertiesToFetch:[NSArray arrayWithObjects:@"groep", nil]];
    [request setReturnsDistinctResults:YES];
    
    
    NSManagedObject *matches = nil;
    
    NSError * error = nil;
    
    NSArray *objects = [contextAdd executeFetchRequest:request
                                                 error:&error];
    if (objects == nil) {
        abort();
    }
    if ([objects count] == 0) {
        NSLog(@"No matches");
    } else {
        for(matches in objects)
        {
            NSString *cluster = [matches valueForKey:@"groep"];
            [clusterList addObject:cluster];
        }
    
    }
    return clusterList;
}

-(NSArray *)GetLocalScheduleFromClusters:(NSString *)StudentID clusterID:(NSArray *)clusterList
{
    NSMutableArray *clusterRoosterItems = [[NSMutableArray alloc]init];
    
    AppDelegate *appDelegate =
    [[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *contextAdd =
    [appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Rooster"
                inManagedObjectContext:contextAdd];
    
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    [request setResultType:NSDictionaryResultType];

    NSString *whereQuery = [clusterList componentsJoinedByString:@"\" OR groep == \""];
    NSMutableString *whereQuery3 = [[NSMutableString alloc]init];
    whereQuery3 = [NSMutableString stringWithFormat:@"groep == \""];
    NSMutableString *whereQuery4 = [[NSMutableString alloc]init];
    whereQuery4 = [NSMutableString stringWithFormat:@"\""];
    [whereQuery3 appendString:whereQuery];
    [whereQuery3 appendString:whereQuery4];
    NSPredicate *groepCodesPredicate = [NSPredicate predicateWithFormat:whereQuery3];

    [request setPredicate:groepCodesPredicate];
    NSManagedObject *matches = nil;
    
    NSError * error = nil;
    
    NSArray *objects = [contextAdd executeFetchRequest:request error:&error];
    if (objects == nil) {abort();}
    if ([objects count] == 0) {
        NSLog(@"No matches");
    } else {
        for(matches in objects)
        {
            RoosterItem *item = [[RoosterItem alloc]init];
            item.studentNr = [matches valueForKey:@"studentnummer"];
            item.docentNaam = [matches valueForKey:@"docent"];
            item.vak = [matches valueForKey:@"vak"];
            item.lesuur = [matches valueForKey:@"lesuur"];
            item.groep = [matches valueForKey:@"groep"];
            
            item.eindDatumTijd = [matches valueForKey:@"einddatumtijd"];
            item.beginDatumTijd = [matches valueForKey:@"begindatumtijd"];
            item.lokaal = [matches valueForKey:@"lokaal"];

            [clusterRoosterItems addObject:item];
        }
    }
    return clusterRoosterItems;
}


- (Boolean)isSameDay:(RoosterItem*) item1 as: (RoosterItem*) item2
{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MMddyyyy"];
    
    NSString *item1Begindag = [dateFormat stringFromDate:item1.beginDatumTijd];
    NSString *item2Begindag = [dateFormat stringFromDate:item2.beginDatumTijd];
    
    if([item2Begindag isEqualToString:item1Begindag]){
        return true;
    }
    
    return false;
}

-(NSArray *)getSingleClusterItems:(NSArray *)roosterItems group:(NSString *)groupName
{
    NSMutableArray *singleClusterItems = [[NSMutableArray alloc]init];
    
    for(RoosterItem *item in roosterItems)
    {
        if([item.groep isEqualToString:groupName])
        {
            [singleClusterItems addObject:item];
        }
    }
    
    return singleClusterItems;
}

//Get the scholen from coredata
//Put them in a array and sort them based on the alphabet.
-(void)setSchoolData
{
    arrayOfCharacters = [[NSMutableArray alloc]init];
    objectsForCharacters = [[NSMutableDictionary alloc]init];
    NSMutableArray *schoolList = [[NSMutableArray alloc]init];
    
    AppDelegate *appDelegate =
    [[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *contextAdd =
    [appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"School"
                inManagedObjectContext:contextAdd];
    
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    [request setResultType:NSDictionaryResultType];
    [request setPropertiesToFetch:[NSArray arrayWithObjects:@"naam", nil]];
    
    for(char c='A';c<='Z';c++)
    {
        [arrayOfCharacters addObject:[NSString stringWithFormat:@"%c",c]];
    }
    
    for(char c='A';c<='Z';c++)
    {
            NSString *character = [NSString stringWithFormat:@"%c", c];
            NSPredicate *myPredicate = [NSPredicate predicateWithFormat:@"naam LIKE[cd] %@", [NSString stringWithFormat:@"%@*",character]];
            [request setPredicate:myPredicate];
            NSManagedObject *matches = nil;
            
            NSError * error = nil;
            
            NSArray *objects = [contextAdd executeFetchRequest:request
                                                         error:&error];
            if (objects == nil) {
                abort();
            }
            if ([objects count] == 0) {
                NSLog(@"No matches");
            } else {
                for(matches in objects)
                {
                    NSString *school = [matches valueForKey:@"naam"];
                    [schoolList addObject:school];
                }
                    if([schoolList count] > 0)
                    {
                        NSMutableArray *finalArray = [[NSMutableArray alloc]init];
                        [finalArray addObjectsFromArray:schoolList];
                        [schoolList removeAllObjects];
                        [objectsForCharacters setObject:finalArray forKey:[NSString stringWithFormat:@"%c",c]];
                    }
                
        }
        
        
    }
    
}

-(NSMutableArray *)getDataIndex
{
    return arrayOfCharacters;
}

-(NSMutableDictionary *)getDataObjects
{
    return objectsForCharacters;
}

@end
