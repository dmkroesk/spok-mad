//
//  LoginUserWebRequest.h
//  SpoK
//
//  Created by Dennis Keldermans on 1/15/14.
//  Copyright (c) 2014 atmstudent. All rights reserved.
//

#import "WebRequest.h"

@interface LoginUserWebRequest : WebRequest

+ (id)initWithStudentId:(NSString*)studentId password:(NSString*)password iosDeviceToken:(NSString*)iosDeviceToken;
@end
