//
//  LoginUserWebRequest.m
//  SpoK
//
//  Created by Dennis Keldermans on 1/15/14.
//  Copyright (c) 2014 atmstudent. All rights reserved.
//

#import "LoginUserWebRequest.h"

@implementation LoginUserWebRequest

+ (id)initWithStudentId:(NSString*)studentId password:(NSString*)password iosDeviceToken:(NSString*)iosDeviceToken
{
    WebRequest *request = [[WebRequest alloc] initWithURL: [NSURL URLWithString:[[NSString stringWithFormat:@"https://145.48.6.12/Authentication/login.php?user=%@&pass=%@&iosDeviceToken=%@", studentId, password, iosDeviceToken] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    NSLog(@"%@", request);
    if(request)
    {
        //customization
        
    }
    
    return request;
}

@end
