//
//  LogoutUserWebRequest.h
//  SpoK
//
//  Created by Dennis Keldermans on 1/15/14.
//  Copyright (c) 2014 atmstudent. All rights reserved.
//

#import "WebRequest.h"

@interface LogoutUserWebRequest : WebRequest

+ (id)initWithDeviceToken: (NSString*)deviceToken;
@end
