//
//  LogoutUserWebRequest.m
//  SpoK
//
//  Created by Dennis Keldermans on 1/15/14.
//  Copyright (c) 2014 atmstudent. All rights reserved.
//

#import "LogoutUserWebRequest.h"

@implementation LogoutUserWebRequest

+ (id)initWithDeviceToken: (NSString*)deviceToken;
{
    WebRequest *request = [[WebRequest alloc] initWithURL: [NSURL URLWithString:[[NSString stringWithFormat:@"https://145.48.6.12/Authentication/logout.php?iosDeviceToken=%@", deviceToken] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    NSLog(@"%@", request);
    if(request)
    {
        //customization
        
    }
    
    return request;
}
@end
