//
//  MessagesWebRequest.h
//  SpoK
//
//  Created by atmstudent on 08/01/14.
//  Copyright (c) 2014 atmstudent. All rights reserved.
//

#import "WebRequest.h"

@interface MessagesWebRequest : WebRequest

+ (id) initWithStudentId:(NSString*)studentId;

@end
