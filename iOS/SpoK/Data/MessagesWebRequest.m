//
//  MessagesWebRequest.m
//  SpoK
//
//  Created by atmstudent on 08/01/14.
//  Copyright (c) 2014 atmstudent. All rights reserved.
//

#import "MessagesWebRequest.h"

@implementation MessagesWebRequest

+ (id) initWithStudentId:(NSString*)studentId
{
    WebRequest *request = [[WebRequest alloc] initWithURL: [NSURL URLWithString:[NSString stringWithFormat:@"https://145.48.6.12/getberichten.php?studentnummer=%@", studentId]]];
    if(request)
    {
        //customization
        
    }
    
    return request;
}


@end
