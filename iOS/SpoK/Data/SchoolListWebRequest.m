//
//  SchoolListWebRequest.m
//  SpoK
//
//  Created by Dennis Keldermans on 12/20/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "SchoolListWebRequest.h"

@implementation SchoolListWebRequest

- (id)init
{
    self = [super initWithURL:[NSURL URLWithString:@"https://145.48.6.12/getschool.php"]];
    if(self)
    {
        //customization
    }
    
    return self;
}

@end
