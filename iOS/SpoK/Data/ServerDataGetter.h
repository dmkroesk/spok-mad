//
//  ServerDataGetter.h
//  SpoK
//
//  Created by atmstudent on 08/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServerDataGetter.h"
#import "DataGetter.h"
#import "RoosterItem.h"
#import "WebRequestHandler.h"


@protocol DataGetterDelegate <NSObject>

@required

@optional
-(void)fillLocalStudyCallData:(RoosterItem *)roosterItem;
-(void)fillLocalStudyCallScholen:(NSString *)schoolItem;
-(void)saveAccountDataWithStudentnummer:(NSString *)studentnummer andSessionToken:(NSString *)token;
-(void)didLogIn:(NSString *)status;
-(void)failedToLogIn:(NSString *)error;

@end

@interface ServerDataGetter : NSObject <DataGetter, WebRequestHandlerCallback>

@property (nonatomic,assign) id<DataGetterDelegate> delegate;

-(void)clearTable:(NSString *)EntityName;

@end
