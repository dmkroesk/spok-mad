//
//  ServerDataGetter.m
//  SpoK
//
//  Created by atmstudent on 08/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "ServerDataGetter.h"
#import "AFNetworking.h"
#import "DataConverter.h"
#import "RoosterItem.h"
#import "AppDelegate.h"
#import "SchoolListWebRequest.h"

@implementation ServerDataGetter

@synthesize delegate;

-(NSDictionary*) getData
{
    return nil;
}

-(void)logInToStudyCallWithStudentnummer:(NSString *)studentnummer andPassword:(NSString *)password
{
    
    // do our long running process here
    //[NSThread sleepForTimeInterval:3];
    
    //ServerDateGetter part
    
    NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:@"https://145.48.6.12/authentication/login.php?user=%1@&pass=%2@",studentnummer, password]];
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        
        NSArray *jsonAnswer = JSON;
        NSString *error = [jsonAnswer valueForKeyPath:@"error"];
        
        if(error==nil){
            NSString *status = [jsonAnswer valueForKeyPath:@"status"];
            NSLog(@"Logged in: %@", status);
            
            //Retrieve token from cookie
            //[self.delegate saveLoginSessionToken:token];
            
            NSHTTPURLResponse *HTTPResponse = response;
            NSDictionary *fields = [HTTPResponse allHeaderFields];
            NSString *cookie = [fields valueForKey:@"Set-Cookie"]; // It is your cookie
            
            NSString *sessionToken = [cookie componentsSeparatedByString:@"="][1];
            
            [self.delegate saveAccountDataWithStudentnummer:studentnummer andSessionToken:sessionToken];
            
            [self.delegate didLogIn:status];
        } else {
            //NSLog(@"Login error: %@", error);
            [self.delegate failedToLogIn:error];
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        //NSLog(@"Error: %@", error);
        [self.delegate failedToLogIn:error.description];
    }];
    
    [operation start];
/*
    
    NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:@"https://145.48.6.12/authentication/login.php?user=%1@&pass=%2@",studentnummer, password]];
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        
        NSArray *jsonAnswer = JSON;
        NSString *error = [jsonAnswer valueForKeyPath:@"error"];
        
        if(error==nil){
            NSString *status = [jsonAnswer valueForKeyPath:@"status"];
            NSLog(@"Logged in: %@", status);
            
            //Retrieve token from cookie
            //[self.delegate saveLoginSessionToken:token];
        } else {
            NSLog(@"Login error: %@", error);
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        NSLog(@"Error: %@", error);
    }];
    
    [operation start];*/
}

-(void)clearTable:(NSString *)EntityName
{
    AppDelegate *appDelegate =
    [[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context =
    [appDelegate managedObjectContext];
    
    NSFetchRequest * allRoosterItems = [[NSFetchRequest alloc] init];
    [allRoosterItems setEntity:[NSEntityDescription entityForName:EntityName inManagedObjectContext:context]];
    [allRoosterItems setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    NSError * error = nil;
    NSArray * items = [context executeFetchRequest:allRoosterItems error:&error];
    
    //error handling goes here
    for (NSManagedObject * roosterItem in items) {
        [context deleteObject:roosterItem];
    }
    NSError *saveError = nil;
    [context save:&saveError];
}

- (void)requestCompletedWithData:(NSDictionary *)theData request:(WebRequest *)originalRequest
{
    NSLog(@"Done loading data: %@", theData);
}

- (void)requestFailedWithError:(NSError *)error request:(WebRequest *)originalRequest
{
    
}




@end
