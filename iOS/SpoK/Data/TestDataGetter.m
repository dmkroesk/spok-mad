//
//  TestDataGetter.m
//  SpoK
//
//  Created by atmstudent on 08/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "TestDataGetter.h"
#import "AdCategory.h"

@implementation TestDataGetter

@synthesize arrayOfCharacters,objectsForCharacters;

-(NSDictionary*) getData
{
    return nil;
}

-(void)setSchoolData
{
    arrayOfCharacters = [[NSMutableArray alloc]init];
    objectsForCharacters = [[NSMutableDictionary alloc]init];
    NSArray *schoolList = [[NSArray alloc]init];
    schoolList = @[@"Avans Hogeschool",@"MBO School"];
    
    NSArray *dummyData = [[NSArray alloc]init];
    dummyData = @[@"Avans Hogeschool",@"",@"Anton Hogeschool",@"Bonkel Hogschool",@"Breede A",@"Contictee Hogeschool",@"Contrans College",@"Devaux College",@"Dimitri Hogeschool",@"Edwards College",@"Eduardus College",@"Fonkel Hogeschol",@"Fiscuc College",@"Geerts College",@"Gerardus Hogeschool",@"Hillebrands College",@"Heeften Hogeschool",@"Ivans Hogeschool",@"Ieper College",@"Jantin College",@"Jannes Hogeschool",@"Kompers College",@"Kampen College",@"Lampers College",@"Limpdte Hogeschool",@"Masters College",@"Montizaan Hogeschool",@"Nimmer Hogeschool",@"Niemandszoon Hogeschool",@"Open Hogeschool",@"Orgels Hogeschool",@"Petrus Hogeschool",@"Paulus Hogeschool",@"Quartz Hogeschool",@"Quarts College",@"Reens Hogeschool",@"Rivat Hogeschool",@"Steevens Hogeschool",@"Schoppen Hogeschool",@"Teeven Hogeschool",@"Tritt College",@"Uven Hogeschool",@"Uden Hogeschool",@"Veens Hogeschool",@"Vaawen Hogeschool",@"Waals Hogeschool",@"Waarheid College",@"Xilom Hogeschool",@"Xetera College",@"Yzom Hogeschool",@"Yzers College",@"Zelfers Hogeschool",@"Zelom College"];
    
    NSMutableArray *tempArray = [[NSMutableArray alloc]init];
    
        for(char c='A';c<='Z';c++)
            {
                [arrayOfCharacters addObject:[NSString stringWithFormat:@"%c",c]];
            }
            for(char c='A';c<='Z';c++)
                {
                for(NSString *school in dummyData)
                    {
                    if(![school length] == 0)
                    {
                        NSString *tempString = [school substringToIndex:1];
                        [tempString uppercaseString];
                            if([tempString isEqualToString:[NSString stringWithFormat:@"%c",c]])
                            {
                                [tempArray addObject:school];
                            }
                        
                        }
          
                    }
                        if([tempArray count] > 0)
                        {
                            NSMutableArray *endArray = [[NSMutableArray alloc]init];
                            [endArray addObjectsFromArray:tempArray];
                            [objectsForCharacters setObject:endArray forKey:[NSString stringWithFormat:@"%c",c]];
                            [tempArray removeAllObjects];
                        }
                }
}


-(NSMutableArray *)getDataIndexTest
{
    return arrayOfCharacters;
}

-(NSMutableDictionary *)getDataObjectsTest
{
    return objectsForCharacters;
}

+(NSArray *) getAdvertentieCategorien
{
    NSMutableArray *array = [[NSMutableArray alloc]init];
    [array addObject: [AdCategory categoryWithName:@"Bijbanen" description:@"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam quam risus, ultricies consequat enim ac, cursus aliquet mi. Nulla facilisi." defaultEnabled:false]];
    [array addObject: [AdCategory categoryWithName:@"Diensten" description:@"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam quam risus, ultricies consequat enim ac, cursus aliquet mi. Nulla facilisi." defaultEnabled:FALSE]];
    [array addObject: [AdCategory categoryWithName:@"Evenementen" description:@"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam quam risus, ultricies consequat enim ac, cursus aliquet mi. Nulla facilisi." defaultEnabled:YES]];
    [array addObject: [AdCategory categoryWithName:@"Vodafone" description:@"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam quam risus, ultricies consequat enim ac, cursus aliquet mi. Nulla facilisi." defaultEnabled:NO]];
    [array addObject: [AdCategory categoryWithName:@"Heineken" description:@"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam quam risus, ultricies consequat enim ac, cursus aliquet mi. Nulla facilisi." defaultEnabled:TRUE]];
    [array addObject: [AdCategory categoryWithName:@"Bavaria" description:@"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam quam risus, ultricies consequat enim ac, cursus aliquet mi. Nulla facilisi." defaultEnabled:true]];
    [array addObject: [AdCategory categoryWithName:@"Rabobank" description:@"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam quam risus, ultricies consequat enim ac, cursus aliquet mi. Nulla facilisi." defaultEnabled:1]];
    [array addObject: [AdCategory categoryWithName:@"ING" description:@"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam quam risus, ultricies consequat enim ac, cursus aliquet mi. Nulla facilisi." defaultEnabled:0]];
    
    return array;
}

+(NSArray *) getAvansKlassen
{
    NSMutableArray *array = [[NSMutableArray alloc]init];
    
    for(int i = 0; i<=80; i++)
    {
        [array addObject:[NSString stringWithFormat:@"Klas %d", i]];
    }
    
    return array;
}

+(NSArray *) getStudyCallKalender
{
    NSMutableArray *lesuren = [[NSMutableArray alloc]init];
    //Data: Studentnummer, Docent, Lesuur(1tm9), Groep(meerdere groepen met studentnr = meerdere clusters,vak, Lokaal, tijdsduur, begintijd, eindtijd
    
    [lesuren addObject:@[@"125822",@"rs02",@"1",@"CCOM1A",@"PROJ-HOT",@"c102",@"45",@"2013-01-14 08:30:00.000",@"2013-01-14 09:15:00.000"]];
    [lesuren addObject:@[@"125822",@"rs02",@"2",@"CCOM1A",@"PROJ-HOT",@"c102",@"45",@"2013-01-14 09:15:00.000",@"2013-01-14 10:00:00.000"]];
    
    [lesuren addObject:@[@"125822",@"sg01",@"3",@"CCOM1A",@"NEDE",@"c103",@"45",@"2013-01-14 10:00:00.000",@"2013-01-14 10:45:00.000"]];
    //pauze van 20 minuten
    // cluster frans
    [lesuren addObject:@[@"125822",@"le1e",@"4",@"CCOM1A-FR",@"FRANS",@"c306",@"45",@"2013-01-14 11:05:00.000",@"2013-01-14 11:50:00.000"]];
    [lesuren addObject:@[@"125822",@"le1e",@"5",@"CCOM1A-FR",@"FRANS",@"c306",@"45",@"2013-01-14 11:50:00.000",@"2013-01-14 12:35:00.000"]];
    //middag pauze van 35 minuten
    //Tussenuur
    [lesuren addObject:@[@"125822",@"k1c",@"7",@"CCOM1A",@"BREIN",@"c103",@"45",@"2013-01-14 13:55:00.000",@"2013-01-14 14:40:00.000"]];


    
    return lesuren;
}

+(NSArray *)getClusters
{
    NSMutableArray *clusters = [[NSMutableArray alloc]init];
    
    [clusters addObject:@[@"CCOM1A"]];
    [clusters addObject:@[@"CCOM1A-FR"]];
    
    return clusters;
}

+(NSArray *) getStudyCallBerichten {
    NSMutableArray *berichten = [[NSMutableArray alloc]init];
    [berichten addObject: @[@"Dinsdag 31,\nSeptember", @"Creatieve technieken", @"Vervalt"]];
    [berichten addObject: @[@"Vrijdag 31,\nNovember", @"Handarbeit", @"Verplaats naar zaterdag 20:00"]];
    [berichten addObject: @[@"Dinsdag 31,\nSeptember", @"Creatieve technieken", @"Vervalt"]];
    [berichten addObject: @[@"Vrijdag 31,\nNovember", @"Handarbeit", @"Verplaats naar zaterdag 20:00"]];
    [berichten addObject: @[@"Dinsdag 31,\nSeptember", @"Creatieve technieken", @"Vervalt"]];
    [berichten addObject: @[@"Vrijdag 31,\nNovember", @"Handarbeit", @"Verplaats naar zaterdag 20:00"]];
    return berichten;
}

@end
