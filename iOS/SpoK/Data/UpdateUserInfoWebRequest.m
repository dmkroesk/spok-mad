//
//  UpdateUserInfoWebRequest.m
//  SpoK
//
//  Created by Tim  on 1/6/14.
//  Copyright (c) 2014 atmstudent. All rights reserved.
//

#import "UpdateUserInfoWebRequest.h"

@implementation UpdateUserInfoWebRequest

+ (id)initWithStudentId:(NSString*)studentId firstname:(NSString*)firstname insertion:(NSString*)insertion lastname:(NSString*)lastname email:(NSString*)email dateOfBirth:(NSString*)dob school:(NSString*)school
{
    WebRequest *request = [[WebRequest alloc] initWithURL: [NSURL URLWithString:[NSString stringWithFormat:@"https://145.48.6.12/updateuserinfo.php?studentnummer=%@&voornaam=%@&tussenvoegsel=%@&achternaam=%@&email=%@&geboortedatum=%@&school=%@",studentId, firstname, insertion, lastname, email, dob, school]]];
    NSLog(@"%@", request);
    if(request)
    {
        //customization
        
    }
    
    return request;
}


@end
