//
//  WebRequest.h
//  SpoK
//
//  Created by Dennis Keldermans on 12/20/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WebRequest : NSObject

@property (nonatomic, strong) NSURL *requestURL;

- (id)initWithURL:(NSURL *)theURL;

@end
