//
//  WebRequest.m
//  SpoK
//
//  Created by Dennis Keldermans on 12/20/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "WebRequest.h"

@implementation WebRequest

- (id)initWithURL:(NSURL *)theURL
{
    self = [super init];
    if(self)
    {
        _requestURL = theURL;
    }
    
    return self;
}

@end
