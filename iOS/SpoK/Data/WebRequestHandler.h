//
//  WebRequestHandler.h
//  SpoK
//
//  Created by Dennis Keldermans on 12/20/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebRequest.h"

@protocol WebRequestHandlerCallback <NSObject>

@required
- (void)requestCompletedWithData:(id)theData request:(WebRequest *)originalRequest;
- (void)requestFailedWithError:(NSError *)error request:(WebRequest *)originalRequest;

@optional

@end

@interface WebRequestHandler : NSObject

- (void)executePOST:(WebRequest *)toExecute callback:(id<WebRequestHandlerCallback>) callback;
- (void)executeGET:(WebRequest *)toExecute callBack:(id<WebRequestHandlerCallback>) callback;
+ (id)sharedWebRequestHandler;

@end
