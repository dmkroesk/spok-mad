//
//  WebRequestHandler.m
//  SpoK
//
//  Created by Dennis Keldermans on 12/20/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "WebRequestHandler.h"
#import <AFNetworking.h>

@implementation WebRequestHandler

+ (id)sharedWebRequestHandler {
    static WebRequestHandler *sharedDataManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedDataManager = [[self alloc] init];
    });
    return sharedDataManager;
}

- (void)executePOST:(WebRequest *)toExecute callback:(id<WebRequestHandlerCallback>) callback
{
    NSURLRequest *request =[[NSURLRequest alloc] initWithURL:toExecute.requestURL];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        // Do something if you want, not needed
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [callback requestFailedWithError:error request:toExecute];
    }];
    
    [operation start];
}

- (void)executeGET:(WebRequest *)toExecute callBack:(id<WebRequestHandlerCallback>) callback
{
    NSURLRequest *request =[[NSURLRequest alloc] initWithURL:toExecute.requestURL];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                                                                        success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
    {
        // Parse the json
        NSLog(@"SUCCES");
        [callback requestCompletedWithData:JSON request:toExecute];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        NSLog(@"FAILED");
        [callback requestFailedWithError:error request:toExecute];
    }];
    
    [operation start];
    NSLog(@"started op for: %@", toExecute.requestURL );
}

@end
