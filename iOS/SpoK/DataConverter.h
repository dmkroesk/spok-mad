//
//  DataConverter.h
//  SpoK
//
//  Created by wkroos on 10/12/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataConverter : NSObject

+(NSArray*) jsonToObjectRooster:(NSArray*) jsonArray;
@end
