//
//  DataConverter.m
//  SpoK
//
//  Created by wkroos on 10/12/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "DataConverter.h"
#import "RoosterItem.h"

@implementation DataConverter
+(NSArray*) jsonToObjectRooster:(NSArray*) jsonArray
{
    NSMutableArray *roosterArray = [[NSMutableArray alloc] init];
    
    for(id jsonObject in jsonArray){
        RoosterItem *roosterItem = [[RoosterItem alloc]init];
        
        roosterItem.beginDatumTijd = [self jsonToObjectDatum:[jsonObject valueForKeyPath:@"BeginDatumtijd"]];
        roosterItem.eindDatumTijd = [self jsonToObjectDatum:[jsonObject valueForKeyPath:@"EindDatumTijd"]];
        roosterItem.docentNaam = [jsonObject valueForKeyPath:@"Docent"];
        roosterItem.groep = [jsonObject valueForKeyPath:@"Groep"];
        roosterItem.studentNr = [jsonObject valueForKeyPath:@"studentnummer"];
        roosterItem.lokaal = [jsonObject valueForKeyPath:@"Lokaal"];
        roosterItem.vak = [jsonObject valueForKeyPath:@"Vak"];
        roosterItem.lesuur = [jsonObject valueForKeyPath:@"Lesuur"];
        
        [roosterArray addObject:roosterItem];
    }
    
    return roosterArray;
}

+(NSDate*) jsonToObjectDatum:(NSArray *) jsonArray
{
    if(!jsonArray){
        return nil;
    }
    
    NSDateFormatter *jsonFormatter = [[NSDateFormatter alloc] init];
    jsonFormatter.timeStyle = NSDateFormatterNoStyle;
    jsonFormatter.dateFormat = @"yyyy-mm-dd HH:mm:ss";
    NSDate *date = [jsonFormatter dateFromString:[jsonArray valueForKeyPath:@"date"]];
    return date;
}
@end
