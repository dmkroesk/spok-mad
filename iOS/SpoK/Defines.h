//
//  Defines.h
//  SpoK
//
//  Created by atmstudent on 29/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#ifndef SpoK_Defines_h
#define SpoK_Defines_h

#define COLOR_AVANSRED [UIColor colorWithRed:198.0/255.0 green:0.0/255.0 blue:42.0/255.0 alpha:255.0/255.0]

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define STUDYCALL_API_KEY                           @"STUDYCALL_KEY"
#define STUDYCALL_API_SECRET                        @"STUDYCALL_SECRET"
#define STUDYCALL_STUDENTNUMMER                     @"40825"

#endif
