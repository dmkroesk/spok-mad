//
//  AdCategory.h
//  SpoK
//
//  Created by atmstudent on 18/12/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AdCategory : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *description;
@property (nonatomic, readwrite) BOOL enabled;

+ (AdCategory*)categoryWithName:(NSString*)name description:(NSString*)description defaultEnabled:(BOOL)defaultEnabled;

@end
