//
//  AdCategory.m
//  SpoK
//
//  Created by atmstudent on 18/12/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "AdCategory.h"

@implementation AdCategory

+ (AdCategory*)categoryWithName:(NSString*)name description:(NSString*)description defaultEnabled:(BOOL)defaultEnabled
{
    AdCategory* cat = [[AdCategory alloc] init];
    cat.name = name;
    cat.description = description;
    cat.enabled = defaultEnabled;
    
    return cat;
}

@end
