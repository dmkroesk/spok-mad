//
//  DayObject.h
//  SpoK
//
//  Created by atmstudent on 27/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DayObject : NSObject

@property (nonatomic, strong) NSString *time;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *location;

@end
