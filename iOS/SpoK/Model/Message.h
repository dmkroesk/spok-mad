//
//  Message.h
//  SpoK
//
//  Created by atmstudent on 08/01/14.
//  Copyright (c) 2014 atmstudent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Message : NSObject

@property (nonatomic, strong) NSString *theMessage;
@property (nonatomic, strong) NSString *dateString;
@property (nonatomic, strong) NSNumber *ai_value;

@end
