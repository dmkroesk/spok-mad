//
//  Message.m
//  SpoK
//
//  Created by atmstudent on 08/01/14.
//  Copyright (c) 2014 atmstudent. All rights reserved.
//

#import "Message.h"

@implementation Message

- (NSComparisonResult)compare:(Message *)otherObject {
    
    if (self.ai_value > otherObject.ai_value) {
        return (NSComparisonResult)NSOrderedAscending;
    } else if (otherObject.ai_value > self.ai_value) {
        return (NSComparisonResult)NSOrderedDescending;
    }
    return(NSComparisonResult)NSOrderedSame;
}

@end
