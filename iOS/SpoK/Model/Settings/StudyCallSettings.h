//
//  StudyCallSettings.h
//  SpoK
//
//  Created by atmstudent on 08/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SettingsModel.h"

@interface StudyCallSettings : NSObject <SettingsModel>

@end
