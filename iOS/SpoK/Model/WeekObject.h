//
//  WeekObject.h
//  SpoK
//
//  Created by atmstudent on 29/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WeekObject : NSObject

@property (nonatomic, strong) NSString *dayName;
@property (nonatomic, strong) NSString *messageAmount;

@end
