//
//  RoosterItem.h
//  SpoK
//
//  Created by wkroos on 10/12/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RoosterItem : NSObject

@property NSNumber *studentNr;
@property NSString *docentNaam;
@property NSNumber *lesuur;
@property NSString *vak;
@property NSString *groep;
@property NSString *lokaal;
@property NSDate *beginDatumTijd;
@property NSDate *eindDatumTijd;

@end
