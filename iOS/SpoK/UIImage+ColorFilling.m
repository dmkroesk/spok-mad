//
//  UIImage+ColorFilling.m
//  SpoK
//
//  Created by atmstudent on 27/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "UIImage+ColorFilling.h"

@implementation UIImage (ColorFilling)

+ (UIImage *)coloredImage:(UIColor *)color dimension:(CGSize)dimension {
    CGRect rect = CGRectMake(0, 0, dimension.width, dimension.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end
