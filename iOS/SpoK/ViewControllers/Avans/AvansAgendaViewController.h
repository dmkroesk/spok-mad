//
//  AvansDayViewController.h
//  SpoK
//
//  Created by atmstudent on 15/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMPageView.h"
#import "DayView.h"
#import "WeekView.h"
#import "MeetingView.h"

@interface AvansAgendaViewController : UIViewController <MMPageViewDataSource>

@property (weak, nonatomic) IBOutlet UIButton *weekButton;
@property (weak, nonatomic) IBOutlet UIButton *dayButton;
@property (weak, nonatomic) IBOutlet UIButton *meetingButton;

@property (nonatomic, strong) MMPageView *m_pageView;
@property (nonatomic, strong) WeekView *weekView;
@property (nonatomic, strong) DayView *dayView;
@property (nonatomic, strong) MeetingView *meetingView;

- (IBAction)weekButtonPressed:(id)sender;
- (IBAction)dayButtonPressed:(id)sender;
- (IBAction)meetingButtonPressed:(id)sender;


@end
