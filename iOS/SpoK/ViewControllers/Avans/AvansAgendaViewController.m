//
//  AvansDayViewController.m
//  SpoK
//
//  Created by atmstudent on 15/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "AvansAgendaViewController.h"
#import "UIImage+ColorFilling.h"
#import "DayObject.h"

@interface AvansAgendaViewController ()
{
    int selectedButton;
}

@end

@implementation AvansAgendaViewController

@synthesize m_pageView, meetingButton, dayButton, weekButton, weekView, dayView, meetingView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view
    
    dayView = [[DayView alloc] initWithFrame:CGRectMake(0, 0, 320, 213)];
    [dayView.m_pageView setContentViewAlignment:AlignmentCenter];
    [dayView.m_pageView setTitleViewAlignment:AlignmentCenter];
    [dayView.m_pageView.pageControl setHidden:YES];
    
    weekView = [[WeekView alloc] initWithFrame:CGRectMake(0, 0, 320, 213)];
    [weekView.m_pageView setContentViewAlignment:AlignmentCenter];
    [weekView.m_pageView setTitleViewAlignment:AlignmentCenter];
    [weekView.m_pageView.pageControl setHidden:YES];
    
    meetingView = [[MeetingView alloc] initWithFrame:CGRectMake(0, 0, 320, 213)];
    
    [self setupTestData];
    
    //set back button
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *backBtnImage = [UIImage imageNamed:@"Avans Berichten Icon"];
    
    [backBtn setBackgroundImage:backBtnImage forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(goback) forControlEvents:UIControlEventTouchUpInside];
    backBtn.frame = CGRectMake(0, 0, 22, 22);
    
    UIBarButtonItem *backButn = [[UIBarButtonItem alloc] initWithImage:backBtnImage style:UIBarButtonItemStylePlain target:self action:@selector(goback)];
    
    self.navigationItem.leftBarButtonItem = backButn;
    
    //Put the names of our image files in our array.
    m_pageView = [MMPageView pageViewWithFrame:CGRectMake(0, 69, 320, 250) leftButtonImage:[UIImage imageNamed:@"AvansPageViewButton Left"] rightButtonImage:[UIImage imageNamed:@"AvansPageViewButton Right"] pageControlTint:[UIColor colorWithRed:64.0/255.0 green:64.0/255.0 blue:64.0/255.0 alpha:255.0/255.0]];
    m_pageView.dataSource = self;
    [m_pageView.pageControl setHidden:YES];
    [m_pageView setArrowStyle:ArrowStyleNone];
    [self.view addSubview:m_pageView];
    [m_pageView disableManualScrolling];
}

- (void)setupTestData
{
    DayPage *page = [[DayPage alloc] initWithFrame:dayView.frame];
    [page setTitle:@"Maandag 11/11/2013"];
    
    DayPage *page2 = [[DayPage alloc] initWithFrame:dayView.frame];
    [page2 setTitle:@"Dinsdag 12/11/2013"];
    
    DayObject *dayobject = [[DayObject alloc] init];
    dayobject.time = @"11:00 - 12:00";
    dayobject.name = @"CreaTech";
    dayobject.location = @"OB-202A";
    
    DayObject *dayobject1 = [[DayObject alloc] init];
    dayobject1.time = @"11:00 - 12:00";
    dayobject1.name = @"CreaTech";
    dayobject1.location = @"OB-202A";
    
    [page addDataObject:dayobject];
    [page addDataObject:dayobject1];
    
    [page2 addDataObject:dayobject];
    [page2 addDataObject:dayobject1];
    
    [dayView addDayPage:page];
    [dayView addDayPage:page2];

    WeekPage *wPage = [[WeekPage alloc] initWithFrame:weekView.frame];
    [wPage setTitle:@"Week 46 - 11/11/13"];
    
    WeekPage *wPage2 = [[WeekPage alloc] initWithFrame:weekView.frame];
    [wPage2 setTitle:@"Week 47 - 18/11/13"];
    
    WeekObject *weekObject = [[WeekObject alloc] init];
    weekObject.dayName = @"Maandag";
    weekObject.messageAmount = @"2";
    
    WeekObject *weekObject2 = [[WeekObject alloc] init];
    weekObject2.dayName = @"Dinsdag";
    weekObject2.messageAmount = @"3";
    
    [wPage addDataObject:weekObject];
    [wPage addDataObject:weekObject2];
    
    [wPage2 addDataObject:weekObject];
    [wPage2 addDataObject:weekObject2];
    
    [weekView addWeekPage:wPage];
    [weekView addWeekPage:wPage2];
}

- (void)viewWillAppear:(BOOL)animated
{
    UIImage *normalBackground = [UIImage coloredImage:[UIColor colorWithRed:63.0/255.0 green:63.0/255.0 blue:63.0/255.0 alpha:255.0/255.0] dimension:weekButton.frame.size];
    UIImage *selectedBackground = [UIImage coloredImage:COLOR_AVANSRED dimension:weekButton.frame.size];
    
    [weekButton setBackgroundImage:normalBackground forState:UIControlStateNormal];
    [dayButton setBackgroundImage:normalBackground forState:UIControlStateNormal];
    [meetingButton setBackgroundImage:normalBackground forState:UIControlStateNormal];
    [weekButton setBackgroundImage:selectedBackground forState:UIControlStateSelected];
    [dayButton setBackgroundImage:selectedBackground forState:UIControlStateSelected];
    [meetingButton setBackgroundImage:selectedBackground forState:UIControlStateSelected];
    
    [self dayButtonPressed:dayButton];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - PageViewDataSource
- (NSInteger)numberOfPagesInPageView:(MMPageView *)pageView
{
    return 3;
}

- (UIView*)pageView:(MMPageView *)pageView contentViewForIndex:(NSInteger)index
{
    switch (index)
    {
        case 0:
        {
            return weekView;
            break;
        }
        case 1:
        {
            return dayView;
            break;
        }
        case 2:
        {
            return meetingView;
            break;
        }
    }
    return nil;
}

- (UIView*)pageview:(MMPageView*)pageView titleViewForIndex:(NSInteger)index
{
    return nil;
}

#pragma mark - Button Handlers

- (IBAction)weekButtonPressed:(id)sender
{
    [weekButton setSelected:YES];
    [dayButton setSelected:NO];
    [meetingButton setSelected:NO];
    selectedButton = 0;
    [m_pageView goToPage:0];
}

- (IBAction)dayButtonPressed:(id)sender
{
    [weekButton setSelected:NO];
    [dayButton setSelected:YES];
    [meetingButton setSelected:NO];
    selectedButton = 1;
    [m_pageView goToPage:1];
}

- (IBAction)meetingButtonPressed:(id)sender
{
    [weekButton setSelected:NO];
    [dayButton setSelected:NO];
    [meetingButton setSelected:YES];
    selectedButton = 2;
    [m_pageView goToPage:2];
}

- (void)goback
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
