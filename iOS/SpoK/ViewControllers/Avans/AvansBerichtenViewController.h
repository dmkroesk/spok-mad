//
//  AvansBerichtenViewController.h
//  SpoK
//
//  Created by wkroos on 25/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMPageView.h"

@interface AvansBerichtenViewController : UIViewController <MMPageViewDataSource>

@property NSArray *berichten;

@end
