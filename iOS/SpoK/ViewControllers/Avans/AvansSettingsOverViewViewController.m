//
//  AvansSettingsOverViewViewController.m
//  SpoK
//
//  Created by Dennis Keldermans on 11/22/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "AvansSettingsOverViewViewController.h"

@interface AvansSettingsOverViewViewController ()

@end

@implementation AvansSettingsOverViewViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self setSelectedCellBackgroundColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setSelectedCellBackgroundColor
{
    UIView *backgroundSelectedCell = [[UIView alloc] init];
    [backgroundSelectedCell setBackgroundColor:[UIColor colorWithRed:0/255.0 green:181/255.0 blue:247/256.0 alpha:1.0]];
    
    for (int section = 0; section < [self.tableView numberOfSections]; section++)
        for (int row = 0; row < [self.tableView numberOfRowsInSection:section]; row++)
        {
            NSIndexPath* cellPath = [NSIndexPath indexPathForRow:row inSection:section];
            UITableViewCell* cell = [self.tableView cellForRowAtIndexPath:cellPath];
            
            [cell setSelectedBackgroundView:backgroundSelectedCell];
            //cell.textLabel.highlightedTextColor

            
        }
}



@end
