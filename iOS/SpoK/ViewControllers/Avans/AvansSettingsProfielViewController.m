//
//  AvansSettingsProfielViewController.m
//  SpoK
//
//  Created by Dennis Keldermans on 11/20/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "AvansSettingsProfielViewController.h"

@interface AvansSettingsProfielViewController ()

@end

@implementation AvansSettingsProfielViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [[self view] endEditing:TRUE];
    
}

@end
