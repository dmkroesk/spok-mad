//
//  AvansSettingsRoosterViewController.m
//  SpoK
//
//  Created by Dennis Keldermans on 11/22/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "AvansSettingsRoosterViewController.h"
#import "TestDataGetter.h"

@interface AvansSettingsRoosterViewController ()
@end

@implementation AvansSettingsRoosterViewController
@synthesize klassen;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    klassen = [TestDataGetter getAvansKlassen];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if(klassen != nil){
        return [klassen count];
    } else {
        return 0;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if(klassen != nil && klassen.count > row){
        return [NSString stringWithFormat:@"%@", [klassen objectAtIndex:row]];
    } else {
        return @"?";
    }
}

@end
