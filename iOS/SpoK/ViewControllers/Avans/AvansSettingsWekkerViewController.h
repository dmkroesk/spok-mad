//
//  AvansSettingsWekkerViewController.h
//  SpoK
//
//  Created by Dennis Keldermans on 11/22/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TestDataGetter.h"

@interface AvansSettingsWekkerViewController : UIViewController <UIPickerViewDelegate>
@end
