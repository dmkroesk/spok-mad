//
//  AvansSettingsWekkerViewController.m
//  SpoK
//
//  Created by Dennis Keldermans on 11/22/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "AvansSettingsWekkerViewController.h"

@interface AvansSettingsWekkerViewController ()

@end

@implementation AvansSettingsWekkerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    switch(component){
        case 0:
            return 24;
        case 1:
        case 2:
            return 60;
        default:
            return 0;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [NSString stringWithFormat:@"%d",row];
}


@end
