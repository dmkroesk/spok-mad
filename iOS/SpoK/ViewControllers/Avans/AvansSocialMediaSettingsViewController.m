//
//  AvansSocialMediaSettingsViewController.m
//  SpoK
//
//  Created by atmstudent on 05/12/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "AvansSocialMediaSettingsViewController.h"

@interface AvansSocialMediaSettingsViewController ()

@end

@implementation AvansSocialMediaSettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
