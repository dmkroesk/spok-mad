//
//  StartupViewController.h
//  SpoK
//
//  Created by atmstudent on 14/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TestDataGetter.h"
#import "ServerDataGetter.h"
#import "DataManager.h"
#import "WebRequestHandler.h"

@interface StartupViewController : UIViewController <UITableViewDelegate,UITableViewDataSource, WebRequestHandlerCallback,UISearchDisplayDelegate,UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *tableScholen;


@property (nonatomic,strong)UISearchDisplayController *searchController;
@property NSMutableDictionary *scholenList;
@property NSArray *letterList;
@property TestDataGetter *getTestData;
@property NSArray *lastIndexes;
@property UIActivityIndicatorView *spinner;
@property NSIndexPath *indexPathSegue;
@property NSMutableArray *filteredNames;
@property BOOL checkifSearch;



@end
