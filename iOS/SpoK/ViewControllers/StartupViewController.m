//
//  StartupViewController.m
//  SpoK
//
//  Created by atmstudent on 14/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "StartupViewController.h"
#import "SchoolListWebRequest.h"
#import "StudyCallKeuzeViewController.h"

@interface StartupViewController ()

@end

@implementation StartupViewController

@synthesize searchBar, scholenList,letterList,getTestData,spinner,filteredNames,searchController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.navigationItem.hidesBackButton = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.hidesBackButton = YES;
    
    [_tableScholen setTag:1];
    
    filteredNames = [[NSMutableArray alloc]init];
    
    searchController = [[UISearchDisplayController alloc]init];
    searchController.searchResultsDataSource = self;
    
    searchBar.translucent = YES;
    [searchBar setBackgroundImage:[UIImage imageNamed:@"Search Background"]];
    searchBar.scopeBarBackgroundImage = [UIImage new];
    
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spinner.center = CGPointMake(160, 240);
    [self.view addSubview:spinner];
    
    [spinner startAnimating];
    SchoolListWebRequest *request = [[SchoolListWebRequest alloc] init];
    [[WebRequestHandler sharedWebRequestHandler] executeGET:request callBack:self];
    
    
    
    [self applyBarColor:[UIColor colorWithRed:35.0/255.0 green:35.0/255.0 blue:35.0/255.0 alpha:255.0/255.0] withTextColor:[UIColor colorWithRed:255.0/255.0 green:192.0/255.0 blue:0.0/255.0 alpha:255.0/255.0]];
    
    [self.navigationController.navigationBar setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                                      [UIColor colorWithRed:255.0/255.0 green:192.0/255.0 blue:0.0/255.0 alpha:255.0/255.0], NSForegroundColorAttributeName,
                                                                      [UIFont fontWithName:@"Rockwell" size:18.0], NSFontAttributeName, nil]];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView.tag == 1)
    {
        return [letterList count];
    }
    else{
        return 1;
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView.tag == 1)
    {
    return [[scholenList objectForKey:[letterList objectAtIndex:section]]count];
    }
    else{
        return [filteredNames count];
    }
}




-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        tableView.sectionIndexBackgroundColor = [UIColor clearColor];
        
    }
    else
    {
        
    }
    
    tableView.sectionIndexColor = [UIColor colorWithRed:236.0/255.0 green:176.0/255.0 blue:0.0/255.0 alpha:255.0/255.0];
    
    [cell.textLabel setTextColor:[UIColor whiteColor]];
    if(tableView.tag ==1)
    {
    [cell.textLabel setText:[[scholenList objectForKey:[letterList objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row]];
    }
    else {
        cell.textLabel.text = filteredNames[indexPath.row];
        [cell.textLabel setTextColor:[UIColor blackColor]];
    }

    return cell;
}




- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"segueStudyCall"])
    {
        if(!_checkifSearch == YES)
        {
            NSString *school = [[scholenList objectForKey:[letterList objectAtIndex:_indexPathSegue.section]]objectAtIndex:_indexPathSegue.row];
            
            StudyCallKeuzeViewController *destViewController = segue.destinationViewController;
            
            destViewController.schoolText = school;
        }
        else{
        NSString *school = [filteredNames objectAtIndex:_indexPathSegue.row];
        
        StudyCallKeuzeViewController *destViewController = segue.destinationViewController;
        
        destViewController.schoolText = school;
        }
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(tableView.tag ==1)
    {
    if(indexPath.row != 26)
    {
        _indexPathSegue = indexPath;
        [self performSegueWithIdentifier:@"segueStudyCall" sender:self];
        [self applyBarColor:[UIColor colorWithRed:35.0/255.0 green:35.0/255.0 blue:35.0/255.0 alpha:255.0/255.0] withTextColor:[UIColor colorWithRed:255.0/255.0 green:192.0/255.0 blue:0.0/255.0 alpha:255.0/255.0]];
        [self.navigationController.navigationBar setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                                          [UIColor colorWithRed:255.0/255.0 green:192.0/255.0 blue:0.0/255.0 alpha:255.0/255.0], NSForegroundColorAttributeName,
                                                                          [UIFont fontWithName:@"Rockwell" size:18.0], NSFontAttributeName, nil]];
        
    

    }
    else {
        
        [self performSegueWithIdentifier:@"segueAvans" sender:self];
        [self applyBarColor:[UIColor colorWithRed:192.0/255.0 green:0.0/255.0 blue:42.0/255.0 alpha:255.0/255.0] withTextColor:[UIColor whiteColor]];
        [self.navigationController.navigationBar setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                                          [UIColor whiteColor], NSForegroundColorAttributeName,
                                                                          [UIFont fontWithName:@"FrutigerLTW01-55Roman" size:18.0], NSFontAttributeName, nil]];
    }
    }
    else{
        if([[filteredNames objectAtIndex:indexPath.row] isEqualToString:@"Avans Hogeschool"])
        {
            [self performSegueWithIdentifier:@"segueAvans" sender:self];
            [self applyBarColor:[UIColor colorWithRed:192.0/255.0 green:0.0/255.0 blue:42.0/255.0 alpha:255.0/255.0] withTextColor:[UIColor whiteColor]];
            [self.navigationController.navigationBar setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                                              [UIColor whiteColor], NSForegroundColorAttributeName,
                                                                              [UIFont fontWithName:@"FrutigerLTW01-55Roman" size:18.0], NSFontAttributeName, nil]];
        }
        else
        {
            _indexPathSegue = indexPath;
            _checkifSearch = YES;
            [self performSegueWithIdentifier:@"segueStudyCall" sender:self];
            [self applyBarColor:[UIColor colorWithRed:35.0/255.0 green:35.0/255.0 blue:35.0/255.0 alpha:255.0/255.0] withTextColor:[UIColor colorWithRed:255.0/255.0 green:192.0/255.0 blue:0.0/255.0 alpha:255.0/255.0]];
            [self.navigationController.navigationBar setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                                              [UIColor colorWithRed:255.0/255.0 green:192.0/255.0 blue:0.0/255.0 alpha:255.0/255.0], NSForegroundColorAttributeName,
                                                                              [UIFont fontWithName:@"Rockwell" size:18.0], NSFontAttributeName, nil]];
        }
    }
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    if(tableView.tag == 1)
    {
        return letterList;
    }
    else {
        return nil;
    }
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    NSInteger count = 0;
    for(NSString *character in letterList)
    {
        if([character isEqualToString:title])
            return count;
        count ++;
    }
    
    return 0;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(tableView.tag == 1)
    {
        return letterList[section];
    }
    else{
        return nil;
    }
}




-(void)applyBarColor:(UIColor *) background withTextColor:(UIColor *)color
{
    [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:color, NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObject:color forKey:NSForegroundColorAttributeName]];
    [self.navigationController.navigationBar setTintColor:color];

    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        [self.navigationController.navigationBar setBarTintColor:background];
    }
    else
    {
        self.navigationController.navigationBar.tintColor = background;
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [[self view] endEditing:TRUE];
}

- (void)requestCompletedWithData:(id)json request:(WebRequest *)originalRequest
{
    //NSLog(@"Done loading data: %@", json);
    DataManager *dataManager = [DataManager sharedManager];
    
    // Parse the feed
    NSArray *jsonScholen = json;
    
    NSArray *schoolArray = [jsonScholen valueForKeyPath:@"Name"];
    
    [dataManager.serverData clearTable:@"School"];
    
    for(NSString *school in schoolArray)
    {
        if(![school isKindOfClass:[NSNull class]])
        {
            [dataManager fillLocalStudyCallScholen:school];
        }
        
    }

    [dataManager.localData setSchoolData];
    scholenList = [dataManager.localData getDataObjects];
    letterList = [dataManager.localData getDataIndex];
    [spinner stopAnimating];
    [_tableScholen reloadData];
}

- (void)requestFailedWithError:(NSError *)error request:(WebRequest *)originalRequest
{
    
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}


#pragma mark Search Display Delegate Methods
-(void)searchDisplayController:(UISearchDisplayController *)controller didLoadSearchResultsTableView:(UITableView *)tableView
{
    [tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [filteredNames removeAllObjects];
    
    if(searchString.length > 0)
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains [search] %@",self.searchBar.text];
        
        for(NSString *key in letterList)
        {
            NSArray *matches = [scholenList[key]filteredArrayUsingPredicate:predicate];
            
            [filteredNames addObjectsFromArray:matches];
        }
    }
    return YES;
}




@end
