//
//  AvansDetailViewController.m
//  SpoK
//
//  Created by wkroos on 25/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "AvansDetailViewController.h"
#import "TestDataGetter.h"

@interface AvansDetailViewController ()

@end

@implementation AvansDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    _berichten = [TestDataGetter getStudyCallBerichten];
    
    MMPageView *pageView = [MMPageView pageViewWithFrame:CGRectMake(0, 107, 320, 250) leftButtonImage:[UIImage imageNamed:@"AvansPageViewButton Left"] rightButtonImage:[UIImage imageNamed:@"AvansPageViewButton Right"] pageControlTint:[UIColor colorWithRed:64.0/255.0 green:64.0/255.0 blue:64.0/255.0 alpha:1]];
    
    [pageView setArrowStyle:ArrowStyleView];
    [pageView setPageContentOffset:45.0f];
    
    pageView.dataSource = self;
    [self.view addSubview:pageView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - PageViewDataSource
- (NSInteger)numberOfPagesInPageView:(MMPageView *)pageView
{
    return [_berichten count];
}

- (UIView*)pageview:(MMPageView*)pageView titleViewForIndex:(NSInteger)index
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 150.0f, 120.0f);
    UITextView *titleLabel = [[UITextView alloc] initWithFrame:rect];
    
    [titleLabel setText: [_berichten objectAtIndex:index][0]];
    [titleLabel setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0]];
    [titleLabel setTextColor: [UIColor colorWithRed:64.0/255.0 green:64.0/255.0 blue:64.0/255.0 alpha:1]];
    [titleLabel setFont: [UIFont fontWithName:@"Helvetica-Light" size:23.0f]];
    [titleLabel setEditable:false];
    [titleLabel setSelectable:false];
    
    return titleLabel;
}

- (UIView*)pageView:(MMPageView *)pageView contentViewForIndex:(NSInteger)index
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 150.0f, 150.0f);
    UITextView *contentLabel = [[UITextView alloc] initWithFrame:rect];
    NSString *text = [NSString stringWithFormat:@"%@\n\n%@", [_berichten objectAtIndex:index][1], [_berichten objectAtIndex:index][2]];
    [contentLabel setText: text];
    [contentLabel setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0]];
    [contentLabel setTextColor: [UIColor colorWithRed:64.0/255.0 green:64.0/255.0 blue:64.0/255.0 alpha:1]];
    [contentLabel setFont: [UIFont fontWithName:@"Helvetica-Light" size:15.0f]];
    [contentLabel setEditable:false];
    [contentLabel setSelectable:false];
    return contentLabel;
}

@end
