//
//  AvansSettingsViewController.h
//  SpoK
//
//  Created by wkroos on 21/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AvansSettingsViewController : UIViewController
@property NSArray *categories;
@property (strong, nonatomic) IBOutlet UITableView *awesomeTableView;

@end
