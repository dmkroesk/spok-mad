//
//  AvansSettingsViewController.m
//  SpoK
//
//  Created by wkroos on 21/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "AvansSettingsViewController.h"

@interface AvansSettingsViewController ()

@end

@implementation AvansSettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
