//
//  StudyCallBerichtenViewController.h
//  SpoK
//
//  Created by wkroos on 25/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMPageView.h"
#import "WebRequestHandler.h"
#import "DataManager.h"
#import "DataConverter.h"

@interface StudyCallBerichtenViewController : UIViewController <MMPageViewDataSource, UIWebViewDelegate, WebRequestHandlerCallback>

@property (nonatomic, strong) NSArray *berichten;
@property (nonatomic, strong) NSMutableArray *messages;
@property (nonatomic, strong) IBOutlet UIWebView *adView;
@property (nonatomic, strong) MMPageView *pageViewer;
@property NSInteger runningAsyncCounter;
@property UIActivityIndicatorView *spinner;


-(void)getNewRoosterDataRequest;

@end
