//
//  StudyCallBerichtenViewController.m
//  SpoK
//
//  Created by wkroos on 25/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "StudyCallBerichtenViewController.h"
#import "StudyCallKalenderViewController.h"
#import "TestDataGetter.h"
#import "AdvertManager.h"
#import "GetRoosterRequest.h"
#import "MessagesWebRequest.h"
#import "Message.h"

@interface StudyCallBerichtenViewController ()

@end

@implementation StudyCallBerichtenViewController

@synthesize runningAsyncCounter, spinner;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)addAsync {
    if(runningAsyncCounter == 0){
        spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        spinner.center = CGPointMake(160, 200);
        [self.view addSubview:spinner];
        [spinner startAnimating];
    }
    runningAsyncCounter++;
}

-(void)removeAsync {
    runningAsyncCounter--;
    if(runningAsyncCounter==0){
        [spinner stopAnimating];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.pageViewer = [MMPageView pageViewWithFrame:CGRectMake(0, 107, 320, 250) leftButtonImage:[UIImage imageNamed:@"SPoKPageViewButton Left"] rightButtonImage:[UIImage imageNamed:@"SPoKPageViewButton Right"] pageControlTint:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:255.0/255.0]];
    [self.pageViewer setArrowStyle:ArrowStyleView];
    [self.pageViewer setPageContentOffset:45.0f];
    self.pageViewer.dataSource = self;
    
    
    dispatch_queue_t queue = dispatch_queue_create("", 0);
    dispatch_queue_t main = dispatch_get_main_queue();
    [self addAsync];
    dispatch_async(queue,
                   ^{
                       _berichten = [TestDataGetter getStudyCallBerichten];
                       AdvertManager *adManager = [AdvertManager sharedManager];
                       NSURLRequest *request = [NSURLRequest requestWithURL: [adManager getAdvertURLForStudentId:@"test"]];
                       [_adView loadRequest:request];
                       dispatch_async(main, ^{
                           
                           [self.view addSubview:self.pageViewer];
                           [self removeAsync];
                       });
                   });
    
    [self applyBarColor:[UIColor colorWithRed:35.0/255.0 green:35.0/255.0 blue:35.0/255.0 alpha:255.0/255.0] withTextColor:[UIColor colorWithRed:255.0/255.0 green:192.0/255.0 blue:0.0/255.0 alpha:255.0/255.0]];
    [self.navigationController.navigationBar setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                                      [UIColor colorWithRed:255.0/255.0 green:192.0/255.0 blue:0.0/255.0 alpha:255.0/255.0], NSForegroundColorAttributeName,
                                                                      [UIFont fontWithName:@"Rockwell" size:18.0], NSFontAttributeName, nil]];
    //[self.view setFrame: CGRectMake(40, 100, 320, 540)];
    
    [self getNewRoosterDataRequest];

}

-(void)viewDidAppear:(BOOL)animated
{
    //[self.navigationController.visibleViewController.view setFrame: CGRectMake(0, 100, 320, 580)];
    //self.navigationController.visibleViewController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth| UIViewAutoresizingFlexibleTopMargin;
    //self.navigationController.visibleViewController.view.frame = CGRectMake(0, 0, self.view.bounds.size.width,  self.view.bounds.size.height-100);

    //self.navigationController.edgesForExtendedLayout = UIRectEdgeTop;
    
    self.edgesForExtendedLayout = UIRectEdgeAll;
    [self getNewRoosterDataRequest];
}

-(void)getNewRoosterDataRequest
{
    GetRoosterRequest *request = [GetRoosterRequest initWithStudentId:STUDYCALL_STUDENTNUMMER];
    
    [self addAsync];
    [[WebRequestHandler sharedWebRequestHandler] executeGET:request callBack:self];
    
    self.messages = [[NSMutableArray alloc] init];
    
    MessagesWebRequest *msgRequest = [MessagesWebRequest initWithStudentId:STUDYCALL_STUDENTNUMMER];
    [self addAsync];
    [[WebRequestHandler sharedWebRequestHandler] executeGET:msgRequest callBack:self];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - PageViewDataSource
- (NSInteger)numberOfPagesInPageView:(MMPageView *)pageView
{
    return [self.messages count];
}

- (UIView*)pageview:(MMPageView*)pageView titleViewForIndex:(NSInteger)index
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 230.0f, 120.0f);
    UITextView *titleLabel = [[UITextView alloc] initWithFrame:rect];
    Message *object = [self.messages objectAtIndex:index];
    
    [titleLabel setText: object.dateString];
    [titleLabel setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0]];
    [titleLabel setTextColor: [UIColor colorWithRed:1 green:1 blue:1 alpha:1]];
    [titleLabel setFont: [UIFont fontWithName:@"HelveticaNeue-Thin" size:32.0f]];
    [titleLabel setEditable:false];
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        [titleLabel setSelectable:false];
    }
    else
    {
        [titleLabel setUserInteractionEnabled:NO];
    }
    
    return titleLabel;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

-(void)applyBarColor:(UIColor *) background withTextColor:(UIColor *)color
{
    [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:color, NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObject:color forKey:NSForegroundColorAttributeName]];
    [self.navigationController.navigationBar setTintColor:color];
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        [self.navigationController.navigationBar setBarTintColor:background];
    }
    else
    {
        self.navigationController.navigationBar.tintColor = background;
    }
}

- (UIView*)pageView:(MMPageView *)pageView contentViewForIndex:(NSInteger)index
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 150.0f, 150.0f);
    UITextView *contentLabel = [[UITextView alloc] initWithFrame:rect];
    Message *msg = [self.messages objectAtIndex:index];
    
    NSString *text = msg.theMessage;
    [contentLabel setText: text];
    [contentLabel setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0]];
    [contentLabel setTextColor: [UIColor colorWithRed:1 green:1 blue:1 alpha:1]];
    [contentLabel setFont: [UIFont fontWithName:@"Helvetica-Light" size:15.0f]];
    [contentLabel setEditable:false];
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        [contentLabel setSelectable:false];
    }
    else
    {
        [contentLabel setUserInteractionEnabled:NO];
    }
    return contentLabel;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"kalenderSegue"])
    {
        //StudyCallKalenderViewController *destViewController = segue.destinationViewController;
        
        
    } else {
        //back segue?
        NSLog(@"back");
    }
}

#pragma mark - WebRequestHandlerCallback

- (void)requestCompletedWithData:(id)theData request:(WebRequest *)originalRequest
{
    if([[originalRequest.requestURL absoluteString] rangeOfString:@"getrooster.php"].location != NSNotFound)
    {
        DataManager *dataManager = [DataManager sharedManager];
        NSArray *jsonRooster = theData;
        NSString *output = [jsonRooster[0] valueForKeyPath:@"studentnummer"];
        
        NSLog(@"StudentNr: %@", output);
    
        NSArray *rooster = [DataConverter jsonToObjectRooster:jsonRooster];
    
        [dataManager.serverData clearTable:@"Rooster"];
    
        for(RoosterItem *roosterItem in rooster)
        {
            [dataManager fillLocalStudyCallData:roosterItem];
        }
    } else if([[originalRequest.requestURL absoluteString] rangeOfString:@"getberichten.php"].location != NSNotFound)
    {
        [self.messages removeAllObjects];
        for(NSDictionary *messageDic in theData)
        {
            Message *msg = [[Message alloc] init];
            msg.theMessage = [messageDic valueForKey:@"bericht"];
            msg.dateString = [((NSDictionary*)[messageDic valueForKey:@"datum"]) objectForKey:@"date"];
            msg.ai_value = [messageDic valueForKey:@"berichten_id"];
            
            
            [self.messages addObject:msg];
            
            
        }
        NSArray *sortedArray;
        sortedArray = [self.messages sortedArrayUsingSelector:@selector(compare:)];
        self.messages = [sortedArray mutableCopy];
        [self.pageViewer reloadPages];
    }
    [self removeAsync];
}



- (void)requestFailedWithError:(NSError *)error request:(WebRequest *)originalRequest
{
    
    [self removeAsync];
}


@end
