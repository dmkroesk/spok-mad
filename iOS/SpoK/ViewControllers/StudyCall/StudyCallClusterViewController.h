//
//  StudyCallClusterViewController.h
//  SpoK
//
//  Created by Dennis Keldermans on 11/29/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DTCustomColoredAccessory.h"
#import "DataManager.h"

@interface StudyCallClusterViewController : UITableViewController

@property (nonatomic, strong) NSMutableIndexSet *expandedSections;
@property (nonatomic, strong) NSArray *kalender;
@property (nonatomic, strong) NSArray *uniqueClusters;
@property (nonatomic, strong) NSArray *clusterRoosterItems;
@property (nonatomic, strong) DataManager *dataManager;

@end
