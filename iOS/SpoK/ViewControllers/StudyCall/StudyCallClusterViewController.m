//
//  StudyCallClusterViewController.m
//  SpoK
//
//  Created by Dennis Keldermans on 11/29/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "StudyCallClusterViewController.h"
#import "ClusterHeaderCell.h"
#import "ClusterRowCell.h"
#import "ClusterHelpCell.h"


@interface StudyCallClusterViewController ()

@end

@implementation StudyCallClusterViewController

@synthesize expandedSections,kalender,uniqueClusters,clusterRoosterItems,dataManager;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    dataManager = [DataManager sharedManager];
    clusterRoosterItems = [[NSArray alloc]init];
    
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spinner.center = CGPointMake(160, 200);
    [self.view addSubview:spinner];
    dispatch_queue_t queue = dispatch_queue_create("", 0);
    dispatch_queue_t main = dispatch_get_main_queue();
    
    dispatch_async(queue,
                   ^{
                       [spinner startAnimating];
                       kalender = [dataManager.localData getLocalStudyCallDataFromCore:STUDYCALL_STUDENTNUMMER];
                       uniqueClusters = [dataManager.localData getLocalClusterData:STUDYCALL_STUDENTNUMMER];
                       clusterRoosterItems = [dataManager.localData GetLocalScheduleFromClusters:STUDYCALL_STUDENTNUMMER clusterID:uniqueClusters];

                       dispatch_async(main, ^{

                           [spinner stopAnimating];
                       });
                   });
    if (!expandedSections)
    {
        expandedSections = [[NSMutableIndexSet alloc] init];
    }
    
    self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"StudyCall Background"]];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (BOOL)tableView:(UITableView *)tableView canCollapseSection:(NSInteger)section
{
    if (section>0) return YES;
    
    return NO;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.

    return [uniqueClusters count] +1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self tableView:tableView canCollapseSection:section])
    {
        if ([expandedSections containsIndex:section])
        {
            return 4; // return rows when expanded
        }
        
        return 1; // only top row showing
    }
    
    // Return the number of rows in the section.
    return 2;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self tableView:tableView canCollapseSection:indexPath.section])
    {
        if (!indexPath.section)
        {
            // first row (Section Header)
            return 42;
        }
        else
        {
            // all other rows (Section Cell)

            if(indexPath.row==4 || [kalender count]==indexPath.row+1){
               return 27;
            } else if(indexPath.row==0 ){
                return 38;
            
            } else {
                return 26;
            }
        
        }
    }
    else
    {
        //Normal Cell
        if(indexPath.row == 0){
            return 35;
        } else if(indexPath.row == 1) {
            return 76;
        }
        else {
            return 38;
        }
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ClusterCellNib";
    static NSString *CellIdentifierKalender = @"ClusterRowCellNib";
    static NSString *CellIdentifierHelp = @"ClusterHelpCellNib";

    ClusterHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    ClusterRowCell *cellKalender = [tableView dequeueReusableCellWithIdentifier:CellIdentifierKalender];
    ClusterHelpCell *cellHelp = [tableView dequeueReusableCellWithIdentifier:CellIdentifierHelp];
    
    if (cell == nil) {
        
        cell = [[ClusterHeaderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.contentView.userInteractionEnabled = NO;
        [cell bringSubviewToFront:cell.clusterSwitch];
        
        
        // Configure the cell...
        NSArray *nib;
        if ([self tableView:tableView canCollapseSection:indexPath.section])
        {
            if (!indexPath.row)
            {
                // first row (Section Header)
                nib = [[NSBundle mainBundle] loadNibNamed:@"ClusterHeaderInterfaceCell" owner:self options:nil];
                
                if ([expandedSections containsIndex:indexPath.section])
                {
                    cell.accessoryView = [DTCustomColoredAccessory accessoryWithColor:[UIColor whiteColor] type:DTCustomColoredAccessoryTypeUp];
                }
                else
                {
                    cell.accessoryView = [DTCustomColoredAccessory accessoryWithColor:[UIColor whiteColor] type:DTCustomColoredAccessoryTypeDown];
                }
                
                cell = [nib objectAtIndex:0];
                [cell setBackgroundColor:[UIColor clearColor]];
            }
            else
            {
                // all other rows (Section Cell)
                nib = [[NSBundle mainBundle] loadNibNamed:@"ClusterRowInterfaceCell" owner:self options:nil];
                cellKalender = [[ClusterRowCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierKalender];
                
                cellKalender = [nib objectAtIndex:0];
                cellKalender.selectionStyle = UITableViewCellSelectionStyleNone;
                [cellKalender setBackgroundColor:[UIColor clearColor]];
            }
        }
        else
        {
            //Normal Cell
            if(indexPath.row == 1)
            {
                nib = [[NSBundle mainBundle] loadNibNamed:@"ClusterHelpInterfaceCell" owner:self options:nil];
                cellHelp = [[ClusterHelpCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierHelp];
                cellHelp.accessoryView = nil;
                cellHelp.selectionStyle = UITableViewCellSelectionStyleNone;
                cellHelp = [nib objectAtIndex:0];
                [cellHelp setBackgroundColor:[UIColor clearColor]];
                return cellHelp;
            }
            else
            {
            nib = [[NSBundle mainBundle] loadNibNamed:@"ClusterHeaderInterfaceCell" owner:self options:nil];
            cell.accessoryView = nil;
            
            cell = [nib objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell setBackgroundColor:[UIColor clearColor]];
            }
        }
        
    }
    
    // Configure the cell...
    
    if ([self tableView:tableView canCollapseSection:indexPath.section])
    {
        if (!indexPath.row)
        {

            // first row
            NSInteger test2 = indexPath.section -1;
            cell.clusterLabel.text = [uniqueClusters objectAtIndex:test2]; // only top row showing
            
            cell.accessoryView = [DTCustomColoredAccessory accessoryWithColor:[UIColor whiteColor] type:DTCustomColoredAccessoryTypeDown];
            
            return cell;

        }
        else
        {
            NSInteger test2 = indexPath.section-1;
            NSString *clusterName = [uniqueClusters objectAtIndex:test2];
            
            NSArray *singleClusterItems = [[NSArray alloc]init];
            
            singleClusterItems = [dataManager.localData getSingleClusterItems:clusterRoosterItems group:clusterName];
            
            RoosterItem *roosterItem = [singleClusterItems objectAtIndex:indexPath.row];
            
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
            [dateFormat setDateFormat:@"yyyy-mm-dd HH:mm:ss.SSS"];
            
            [dateFormat setDateFormat:@"HH:mm"];
            NSString *startDateTimeStr = [dateFormat stringFromDate:roosterItem.beginDatumTijd];
            NSString *endDateTimeStr = [dateFormat stringFromDate:roosterItem.eindDatumTijd];
            
            
            NSString *startEndTime = [NSString stringWithFormat:@"%@ - %@", startDateTimeStr, endDateTimeStr];
            
            cellKalender.tijdLabel.text = startEndTime;
            cellKalender.vakLabel.text = roosterItem.vak;
            cellKalender.lokaalLabel.text = roosterItem.lokaal;
            cellKalender.uurLabel.text = [NSString stringWithFormat:@"%@",roosterItem.lesuur];
            
            return cellKalender;
        }
    }
    else
    {
        if(indexPath.row == 1)
        {
            cell.accessoryView = nil;
            cell.clusterLabel.text = @"Dit is het cluster scherm, hier kun je je clusters afzonderlijk bekijken. Ook kun je ze hier uit en aan zetten voor een persoonlijk overzicht.";
            cell.clusterSwitch.hidden = YES;
            return cell;
        }
        else
        {
        cell.accessoryView = nil;
        cell.clusterLabel.text = @"Cluster Help";
        cell.clusterSwitch.hidden = YES;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
        }
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self tableView:tableView canCollapseSection:indexPath.section])
    {
        if (!indexPath.row)
        {
            // only first row toggles exapand/collapse
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            
            NSInteger section = indexPath.section;
            BOOL currentlyExpanded = [expandedSections containsIndex:section];
            NSInteger rows;
            
            NSMutableArray *tmpArray = [NSMutableArray array];
            
            if (currentlyExpanded)
            {
                rows = [self tableView:tableView numberOfRowsInSection:section];
                [expandedSections removeIndex:section];
                
            }
            else
            {
                [expandedSections addIndex:section];
                rows = [self tableView:tableView numberOfRowsInSection:section];
            }
            
            for (int i=1; i<rows; i++)
            {
                NSIndexPath *tmpIndexPath = [NSIndexPath indexPathForRow:i
                                                               inSection:section];
                [tmpArray addObject:tmpIndexPath];
            }
            
            UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            
            if (currentlyExpanded)
            {
                [tableView deleteRowsAtIndexPaths:tmpArray
                                 withRowAnimation:UITableViewRowAnimationTop];
                
                cell.accessoryView = [DTCustomColoredAccessory accessoryWithColor:[UIColor grayColor] type:DTCustomColoredAccessoryTypeDown];
                
            }
            else
            {
                [tableView insertRowsAtIndexPaths:tmpArray
                                 withRowAnimation:UITableViewRowAnimationTop];
                cell.accessoryView =  [DTCustomColoredAccessory accessoryWithColor:[UIColor grayColor] type:DTCustomColoredAccessoryTypeUp];
                
            }
        }
    }
}







@end
