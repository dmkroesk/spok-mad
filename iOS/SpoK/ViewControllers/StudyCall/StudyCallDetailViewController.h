//
//  StudyCallDetailViewController.h
//  SpoK
//
//  Created by wkroos on 25/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMPageView.h"

@interface StudyCallDetailViewController : UIViewController

@property NSArray *berichten;

@property (weak, nonatomic) IBOutlet UITextView *dateTextView;
@property (weak, nonatomic) IBOutlet UILabel *classLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *docentLabel;


@property NSString *dateText;
@property NSString *classText;
@property NSString *timeText;
@property NSString *docentText;


@end
