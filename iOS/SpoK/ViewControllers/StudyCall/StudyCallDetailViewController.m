//
//  StudyCallDetailViewController.m
//  SpoK
//
//  Created by wkroos on 25/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "StudyCallDetailViewController.h"
#import "TestDataGetter.h"

@interface StudyCallDetailViewController ()

@end

@implementation StudyCallDetailViewController

@synthesize classLabel,timeLabel,dateText,dateTextView,classText,timeText,docentText,docentLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self setLabels];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) setLabels
{
    classLabel.text = classText;
    timeLabel.text = timeText;
    dateTextView.text = dateText;
    docentLabel.text = docentText;
    
    [dateTextView setFont: [UIFont fontWithName:@"HelveticaNeue-Thin" size:32.0f]];
    [dateTextView setTextColor: [UIColor colorWithRed:1 green:1 blue:1 alpha:1]];
    
}

@end
