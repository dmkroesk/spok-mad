//
//  StudyCallGegevensViewController.h
//  SpoK
//
//  Created by Dennis Keldermans on 11/20/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebRequestHandler.h"

@interface StudyCallGegevensViewController : UIViewController <UITextFieldDelegate, WebRequestHandlerCallback>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *middleNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *birthDateTextField;
@property (weak, nonatomic) IBOutlet UITextField *schoolTextField;
@property (weak, nonatomic) IBOutlet UITextField *studentId;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (nonatomic, strong) NSString *school;

- (IBAction)saveProfile:(id)sender;
- (void)birthDateTextFieldClicked;
@property (strong, atomic) UIActivityIndicatorView *spinner;
@end
