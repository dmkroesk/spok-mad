//
//  StudyCallGegevensViewController.m
//  SpoK
//
//  Created by Dennis Keldermans on 11/20/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "StudyCallGegevensViewController.h"
#import "AFNetworking.h"
#import "CreateUserWebRequest.h"
#import "AppDelegate.h"

@interface StudyCallGegevensViewController ()


@end

@implementation StudyCallGegevensViewController

@synthesize firstNameTextField,middleNameTextField,lastNameTextField,emailTextField,birthDateTextField,schoolTextField, spinner, studentId, passwordField,scrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	UITapGestureRecognizer *tapScroll = [[UITapGestureRecognizer alloc]initWithTarget:self     action:@selector(tapped)];
    tapScroll.cancelsTouchesInView = NO;
    [scrollView addGestureRecognizer:tapScroll];
    studentId.delegate = self;
    passwordField.delegate = self;
    firstNameTextField.delegate = self;
    middleNameTextField.delegate = self;
    lastNameTextField.delegate = self;
    emailTextField.delegate = self;
    birthDateTextField.delegate = self;
    schoolTextField.delegate = self;
    schoolTextField.text = _school;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) tapped
{
    [self.view endEditing:YES];
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if(textField.tag==7){
        [self.view endEditing:YES];
        [self birthDateTextFieldClicked];
        
        return NO;
    } else {
        return YES;
    }
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField:textField up:YES];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:textField up:NO];
}


- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    int animatedDistance = 0;
    int moveUpValue = textField.frame.origin.y-scrollView.contentOffset.y+ textField.frame.size.height;
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        if(textField.tag < 3)
        {
            [scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x, scrollView.contentOffset.y+textField.frame.size.height+20) animated:YES];
        }
        else if(textField.tag > 3 || textField.tag < 7)
        {
            if ([ [ UIScreen mainScreen ] bounds ].size.height == 568)
            {
            animatedDistance = 220-(320-moveUpValue-5);
            }
            else {
                animatedDistance = 220-(320-moveUpValue);
            }
        }
        else{
            [scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x, scrollView.contentOffset.y+textField.frame.size.height+20) animated:YES];
        }
    }
    else
    {
        animatedDistance = 162-(320-moveUpValue-5);
    }
    
    if(animatedDistance>0)
    {
        const int movementDistance = animatedDistance;
        const float movementDuration = 0.3f;
        int movement = (up ? -movementDistance : movementDistance);
        [UIView beginAnimations: nil context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: movementDuration];
        self.view.frame = CGRectOffset(self.view.frame, 0, movement);
        [UIView commitAnimations];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    NSInteger nextTag = textField.tag +1;
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if(nextResponder){
        [nextResponder becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
        [self saveProfile];
    }
    return NO;
}

-(void)saveProfile
{
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    spinner.center = CGPointMake(385, 484);
    spinner.hidesWhenStopped = YES;
    [self.view addSubview:spinner];
    [spinner startAnimating];
    
    CreateUserWebRequest *request = [CreateUserWebRequest initWithStudentId:studentId.text firstname:firstNameTextField.text insertion:middleNameTextField.text lastname:lastNameTextField.text email:emailTextField.text dateOfBirth:birthDateTextField.text school:schoolTextField.text password:passwordField.text];
    
    [[WebRequestHandler sharedWebRequestHandler] executeGET:request callBack:self];
}

- (IBAction)saveProfile:(id)sender {
    
    [self saveProfile];
}

- (void)requestCompletedWithData:(NSDictionary *)theData request:(WebRequest *)originalRequest
{
    NSLog(@"completed");
    [spinner stopAnimating];
    [self saveUserInCoreData];
    [self performSegueWithIdentifier:@"goToLoginSchermSegue" sender:nil];
}

- (void)saveUserInCoreData
{
    NSManagedObjectContext *context = ((AppDelegate *)[[UIApplication sharedApplication] delegate]).managedObjectContext;
    
    // Create a new managed object
    NSManagedObject *newUser = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:context];
    [newUser setValue:self.studentId.text forKey:@"studentnummer"];
    [newUser setValue:self.passwordField.text forKey:@"password"];
    [newUser setValue:self.firstNameTextField.text forKey:@"voornaam"];
    [newUser setValue:self.middleNameTextField.text forKey:@"tussenvoegsel"];
    [newUser setValue:self.lastNameTextField.text forKey:@"achternaam"];
    [newUser setValue:self.emailTextField.text forKey:@"email"];
    [newUser setValue:self.birthDateTextField.text forKey:@"geboortedatum"];
    [newUser setValue:self.schoolTextField.text forKey:@"school"];
    [newUser setValue: [NSString stringWithFormat:@"%@%@", self.firstNameTextField.text, self.studentId.text] forKey:@"username"];
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
}

- (void)requestFailedWithError:(NSError *)error request:(WebRequest *)originalRequest
{
    NSLog(@"%@", error);
    [spinner stopAnimating];
    
}

- (void)birthDateTextFieldClicked {
    
    CGRect toolbarTargetFrame = CGRectMake(0, self.view.bounds.size.height-216-44, 320, 44);
    CGRect datePickerTargetFrame = CGRectMake(0, self.view.bounds.size.height-216, 320, 216);
    
    UIView *darkView = [[UIView alloc] initWithFrame:self.view.bounds];
    darkView.alpha = 0;
    darkView.backgroundColor = [UIColor blackColor];
    darkView.tag = 9;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissDatePicker:)];
    [darkView addGestureRecognizer:tapGesture];
    [self.view addSubview:darkView];
    
    UIDatePicker *datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height+44, 320, 216)];
    datePicker.tag = 10;
    datePicker.backgroundColor = [UIColor whiteColor];
    [datePicker addTarget:self action:@selector(changeDate:) forControlEvents:UIControlEventValueChanged];
    datePicker.datePickerMode = UIDatePickerModeDate;
    
    if(![birthDateTextField.text isEqualToString:@""]){
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
        [dateFormat setDateFormat:@"dd-MM-yyyy"];
        NSDate *date = [dateFormat dateFromString:birthDateTextField.text];
        [datePicker setDate:date];
    }
    [self.view addSubview:datePicker];
    
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height, 320, 44)];
    toolBar.tag = 11;
    toolBar.barStyle = UIBarStyleDefault;
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissDatePicker:)];
    
    [toolBar setItems:[NSArray arrayWithObjects:spacer, doneButton, nil]];
    [self.view addSubview:toolBar];
    
    [UIView beginAnimations:@"MoveIn" context:nil];
    toolBar.frame = toolbarTargetFrame;
    datePicker.frame = datePickerTargetFrame;
    darkView.alpha = .5;
    [UIView commitAnimations];
}

- (void)changeDate:(UIDatePicker *)sender {
    NSLog(@"New Date: %@", sender.date);
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"dd-MM-yyyy"];
    NSString *dateString = [dateFormat stringFromDate:sender.date];
    
    [birthDateTextField setText:dateString];
}

- (void)removeViews:(id)object {
    NSLog(@"Removeviews");
    [[self.view viewWithTag:9] removeFromSuperview];
    [[self.view viewWithTag:10] removeFromSuperview];
    [[self.view viewWithTag:11] removeFromSuperview];
}

- (void)dismissDatePicker:(id)sender {
    NSLog(@"Dismiss datepicker");
    CGRect toolbarTargetFrame = CGRectMake(0, self.view.bounds.size.height, 320, 44);
    CGRect datePickerTargetFrame = CGRectMake(0, self.view.bounds.size.height+44, 320, 216);
    [UIView beginAnimations:@"MoveOut" context:nil];
    [self.view viewWithTag:9].alpha = 0;
    [self.view viewWithTag:10].frame = datePickerTargetFrame;
    [self.view viewWithTag:11].frame = toolbarTargetFrame;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(removeViews:)];
    [UIView commitAnimations];
    [self saveProfile];


    
}
@end
