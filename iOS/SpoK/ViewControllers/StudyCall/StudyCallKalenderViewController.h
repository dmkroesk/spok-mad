//
//  StudyCallKalenderViewController.h
//  SpoK
//
//  Created by wkroos on 25/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMPageView.h"
#import "KalenderCell.h"
#import "StudyCallDetailViewController.h"

@interface StudyCallKalenderViewController : UIViewController <MMPageViewDataSource,UITableViewDataSource,UITableViewDelegate, UIWebViewDelegate>

@property (nonatomic, strong) NSArray *kalender;
@property (nonatomic, strong) NSString *targetDateStr;
@property (nonatomic, strong) MMPageView *pageView;
@property (nonatomic, strong) IBOutlet UIWebView *adView;
@property (nonatomic, strong) KalenderCell *kalCel;
@property (nonatomic, strong) NSMutableArray *clusterColors;
@property (nonatomic,strong) NSMutableDictionary *clusterColorCombination;

-(void)initView;

@end
