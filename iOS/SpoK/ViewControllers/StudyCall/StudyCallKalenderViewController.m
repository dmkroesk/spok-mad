//
//  StudyCallKalenderViewController.m
//  SpoK
//
//  Created by wkroos on 25/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "StudyCallKalenderViewController.h"
#import "TestDataGetter.h" 
#import "DataManager.h"
#import "UIImage+ColorFilling.h"
#import "AdvertManager.h"

@interface StudyCallKalenderViewController ()

@end

@implementation StudyCallKalenderViewController

@synthesize targetDateStr,pageView,kalCel,clusterColors,clusterColorCombination;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
    pageView = [MMPageView pageViewWithFrame:CGRectMake(0, 20, 320, 320) leftButtonImage:[UIImage imageNamed:@"SPoKPageViewButton Left"] rightButtonImage:[UIImage imageNamed:@"SPoKPageViewButton Right"] pageControlTint:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:255.0/255.0]];
    [pageView setArrowStyle:ArrowStyleTitle];
    pageView.dataSource = self;
    [self.view addSubview:pageView];
    [self initView];

    
}

-(void)initView
{
    [pageView removeFromSuperview];
    DataManager *dataManager = [DataManager sharedManager];
    kalCel = [[KalenderCell alloc]init];
    AdvertManager *adManager = [AdvertManager sharedManager];
    clusterColorCombination = [[NSMutableDictionary alloc]init];
    [self fillColors];
    
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spinner.center = CGPointMake(160, 200);
    [self.view addSubview:spinner];
    dispatch_queue_t queue = dispatch_queue_create("", 0);
    dispatch_queue_t main = dispatch_get_main_queue();
    
    dispatch_async(queue,
                   ^{
                       [spinner startAnimating];
                       _kalender = [dataManager.localData getLocalStudyCallDataFromCore:STUDYCALL_STUDENTNUMMER];
                       NSURLRequest *request = [NSURLRequest requestWithURL: [adManager getAdvertURLForStudentId:@"test"]];
                       [_adView loadRequest:request];
                       dispatch_async(main, ^{
                           [self.view addSubview:pageView];
                           [pageView reloadPages];
                           [spinner stopAnimating];
                       });
                   });
    
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *backBtnImage = [UIImage imageNamed:@"StudyCall Berichten Icon"];
    
    [backBtn setBackgroundImage:backBtnImage forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(goback) forControlEvents:UIControlEventTouchUpInside];
    backBtn.frame = CGRectMake(0, 0, 22, 22);
    
    UIBarButtonItem *backButn = [[UIBarButtonItem alloc] initWithImage:backBtnImage style:UIBarButtonItemStylePlain target:self action:@selector(goback)];
    
    self.navigationItem.leftBarButtonItem = backButn;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)goback
{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"detailView"])
    {
        MMPage *currentPage = [pageView.pages objectAtIndex:pageView.pageControl.currentPage];
        
        UIView *currentView = (UIView*)currentPage.contentView;
        UITableView *currentTableView;
        
        for(UIView *view in currentView.subviews)
        {
            if([view class] == [UITableView class])
            {
                currentTableView = (UITableView*) view;
                break;
            }
        }
        
        StudyCallDetailViewController *destViewController = segue.destinationViewController;
        
        NSArray *a2 = [_kalender objectAtIndex:currentTableView.tag];
        NSIndexPath *indexRow = [currentTableView indexPathForSelectedRow];
        RoosterItem *roosterItem = [a2 objectAtIndex:indexRow.row];
        
        NSDateFormatter* df = [[NSDateFormatter alloc] init];
        [df setTimeZone:[NSTimeZone localTimeZone]];
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"nl_NL"];
        [df setLocale:locale];

        NSDate *targetDate = roosterItem.beginDatumTijd;
        [df setDateFormat:@"EEEE"];
        targetDateStr = [[df stringFromDate:targetDate] capitalizedString];
        [df setDateFormat:@"dd MMMM"];
        targetDateStr = [NSString stringWithFormat:@"%@,\n%@", targetDateStr, [df stringFromDate:targetDate]];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
        [dateFormat setDateFormat:@"yyyy-mm-dd HH:mm:ss.SSS"];
        
        [dateFormat setDateFormat:@"HH:mm"];
        NSString *startDateTimeStr = [dateFormat stringFromDate:roosterItem.beginDatumTijd];
        NSString *endDateTimeStr = [dateFormat stringFromDate:roosterItem.eindDatumTijd];
        
        
        NSString *startEndTime = [NSString stringWithFormat:@"%@ - %@", startDateTimeStr, endDateTimeStr];
        
        destViewController.dateText = [NSString stringWithFormat:@"%@ %@",targetDateStr,startEndTime];
        destViewController.timeText = [NSString stringWithFormat:@"Klas: %@",roosterItem.groep];
        destViewController.classText = [NSString stringWithFormat:@"Vak: %@", roosterItem.vak];
        destViewController.docentText = [NSString stringWithFormat:@"Docent: %@",roosterItem.docentNaam];
        

    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[_kalender objectAtIndex:tableView.tag ] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *dagArray = [[NSArray alloc]init];
    dagArray = [_kalender objectAtIndex:tableView.tag];
    if(indexPath.row+2 <= dagArray.count){
        RoosterItem *currentLes = [dagArray objectAtIndex:indexPath.row];
        RoosterItem *nextLes = [dagArray objectAtIndex:indexPath.row+1];
        if([currentLes.vak isEqualToString:nextLes.vak]){
            //Vervolg van de les
            return 22;
        }
    }
    //Einde van de les
    return 26;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{    
    static NSString *simpleTableIdentifier = @"KalenderCellNib";
    KalenderCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[KalenderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"KalenderInterfaceCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    NSArray *a2 = [_kalender objectAtIndex:tableView.tag];
    RoosterItem *roosterItem = [a2 objectAtIndex:indexPath.row];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"yyyy-mm-dd HH:mm:ss.SSS"];
    
    [dateFormat setDateFormat:@"HH:mm"];
    NSString *startDateTimeStr = [dateFormat stringFromDate:roosterItem.beginDatumTijd];
    NSString *endDateTimeStr = [dateFormat stringFromDate:roosterItem.eindDatumTijd];

    
    NSString *startEndTime = [NSString stringWithFormat:@"%@ - %@", startDateTimeStr, endDateTimeStr];
    
    cell.tijdLabel.text = startEndTime;
    cell.vakLabel.text = roosterItem.vak;
    cell.lokaalLabel.text = roosterItem.lokaal;
    cell.uurLabel.text = [NSString stringWithFormat:@"%@",roosterItem.lesuur];
    
    [cell setBackgroundColor:[UIColor clearColor]];
    
    //Create background for selected state
    UIImage *img = [UIImage coloredImage:[UIColor colorWithRed:1 green:1 blue:1 alpha:0.25] dimension:CGSizeMake(1, 1)];

    cell.selectedBackgroundView = [[UIImageView alloc] initWithImage:img];
    
    //Define the line colors for the clusters
    if(indexPath.row+2 <= a2.count){
        RoosterItem *previousLes = nil;
        if(indexPath.row-1 >= 0)
        {
            previousLes = [a2 objectAtIndex:indexPath.row-1];
        }
        RoosterItem *currentLes = [a2 objectAtIndex:indexPath.row];
        RoosterItem *nextLes = [a2 objectAtIndex:indexPath.row+1];
        if(!([currentLes.groep isEqualToString:nextLes.groep] || [currentLes.groep isEqualToString:previousLes.groep]))
        {
            if(![clusterColorCombination count] == 0)
            {
                if([clusterColorCombination objectForKey:currentLes.groep])
                {
                    [cell.leftLine setBackgroundColor:[clusterColorCombination objectForKey:currentLes.groep]];
                }
                else
                {
                    int randomIndex = arc4random() % 3;
                    [cell.leftLine setBackgroundColor:[clusterColors objectAtIndex:randomIndex]];
                    [clusterColorCombination setObject:[clusterColors objectAtIndex:randomIndex] forKey:currentLes.groep];
                }
            }
        
        else
            {
            int randomIndex = arc4random() % 3;
            [cell.leftLine setBackgroundColor:[clusterColors objectAtIndex:randomIndex]];
            [clusterColorCombination setObject:[clusterColors objectAtIndex:randomIndex] forKey:currentLes.groep];
            }
        }
    }
    
    
 
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"detailView" sender:self];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark - PageViewDataSource
- (NSInteger)numberOfPagesInPageView:(MMPageView *)pageView
{
    return [_kalender count];
}

- (UIView*)pageview:(MMPageView*)pageView titleViewForIndex:(NSInteger)index
{
    CGRect rect = CGRectMake(0, 0, 275, 100);
    UIView *totalView = [[UIView alloc] initWithFrame:rect];
    
    CGRect rectTitle = CGRectMake(40, 0, 230, 100);
    UITextView *titleLabel = [[UITextView alloc] initWithFrame:rectTitle];
    

    //index is nu lesuur dat moet veranderd worden naar dag, maar we hebben nog geen dagen in de testdate. alleen maar 1 dag
    NSArray *dagRooster = [_kalender objectAtIndex:index];
    RoosterItem *roosterItem = [dagRooster objectAtIndex:0];
    
    NSDateFormatter* df = [[NSDateFormatter alloc] init];
    [df setTimeZone:[NSTimeZone localTimeZone]];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"nl_NL"];
    [df setLocale:locale];
    NSDate *targetDate = roosterItem.beginDatumTijd;
    [df setDateFormat:@"EEEE"];
    targetDateStr = [[df stringFromDate:targetDate] capitalizedString];
    [df setDateFormat:@"dd MMMM"];
    targetDateStr = [NSString stringWithFormat:@"%@,\n%@", targetDateStr, [df stringFromDate:targetDate]];
    
    [titleLabel setText: targetDateStr];
    [titleLabel setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0]];
    [titleLabel setTextColor: [UIColor colorWithRed:1 green:1 blue:1 alpha:1]];
    [titleLabel setFont: [UIFont fontWithName:@"HelveticaNeue-Thin" size:32.0f]];
    [titleLabel setEditable:false];
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        [titleLabel setSelectable:false];
    }
    else
    {
        [titleLabel setUserInteractionEnabled:NO];
    }
    
    //[titleLabel setBounds:rect];
    
    [totalView addSubview:titleLabel];
    return totalView;
}

- (UIView*)pageView:(MMPageView *)pageView contentViewForIndex:(NSInteger)index
{
    CGRect rectTotal = CGRectMake(0, 0, 275, 200);
    UIView *wrapperView = [[UIView alloc]initWithFrame:rectTotal];
    
    
    CGRect rectLine = CGRectMake(45, 0, 230, 0.5f);
    UIView *lineView = [[UIView alloc]initWithFrame:rectLine];
    [lineView setBackgroundColor:[UIColor whiteColor]];
    
    CGRect rectTable = CGRectMake(15, 2, 260, 176);
    UITableView *tableContent = [[UITableView alloc]initWithFrame:rectTable];
    
    CGRect rectLineBottom = CGRectMake(45, 179, 230, 0.5f);
    UIView *lineViewBottom = [[UIView alloc]initWithFrame:rectLineBottom];
    [lineViewBottom setBackgroundColor:[UIColor whiteColor]];
    
    tableContent.dataSource = self;
    tableContent.delegate = self;
    
    NSNumber *index1 = [NSNumber numberWithInt:index];
    
    tableContent.tag = [index1 integerValue];
    [tableContent setBackgroundColor:[UIColor clearColor]];
    [tableContent setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        tableContent.sectionIndexBackgroundColor = [UIColor clearColor];
    }
    else
    {
        // Apperantly, we don't need to do a thing =D
    }
    tableContent.sectionIndexColor = [UIColor clearColor];
    

    [wrapperView addSubview:lineView];
    [wrapperView addSubview:tableContent];
    [wrapperView addSubview:lineViewBottom];
    
    
    return wrapperView;
}

-(void)fillColors
{
    clusterColors = [[NSMutableArray alloc]init];
    UIColor *white = [UIColor colorWithRed:220/255 green:255/255 blue:255/255 alpha:1.0];
    UIColor *black = [UIColor colorWithRed:51/255 green:51/255 blue:51/255 alpha:1.0];
    UIColor *yellow = [UIColor colorWithRed:51/255 green:51/255 blue:51/255 alpha:1.0];
    UIColor *green = [UIColor colorWithRed:198/255 green:204/255 blue:20/255 alpha:1.0];
    [clusterColors addObject:white];
    [clusterColors addObject:black];
    [clusterColors addObject:yellow];
    [clusterColors addObject:green];
}

@end
