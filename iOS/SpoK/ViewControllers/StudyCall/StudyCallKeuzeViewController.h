//
//  StudyCallKeuzeViewController.h
//  SpoK
//
//  Created by Dennis Keldermans on 12/20/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "ViewController.h"

@interface StudyCallKeuzeViewController : ViewController
@property (weak, nonatomic) IBOutlet UILabel *schoolLabel;
@property NSString *schoolText;

@end
