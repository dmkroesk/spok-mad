//
//  StudyCallKeuzeViewController.m
//  SpoK
//
//  Created by Dennis Keldermans on 12/20/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "StudyCallKeuzeViewController.h"
#import "StudyCallGegevensViewController.h"

@interface StudyCallKeuzeViewController ()

@end

@implementation StudyCallKeuzeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	_schoolLabel.text = _schoolText;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"segueRegister"]) {
        StudyCallGegevensViewController *destViewController = segue.destinationViewController;
        destViewController.school = _schoolText;
    } 
}

@end
