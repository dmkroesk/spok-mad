//
//  StudyCallLoginViewController.h
//  SpoK
//
//  Created by atmstudent on 15/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebRequestHandler.h"

@interface StudyCallLoginViewController : UIViewController <UITextFieldDelegate, WebRequestHandlerCallback>
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, atomic) UIActivityIndicatorView *spinner;
- (IBAction)onOkTouch:(id)sender;

@end
