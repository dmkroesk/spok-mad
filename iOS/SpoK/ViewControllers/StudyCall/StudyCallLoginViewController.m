//
//  StudyCallLoginViewController.m
//  SpoK
//
//  Created by atmstudent on 15/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "StudyCallLoginViewController.h"
#import "DataManager.h"
#import "AFNetworking.h"
#import "LoginUserWebRequest.h"


@interface StudyCallLoginViewController ()

@end

@implementation StudyCallLoginViewController

@synthesize usernameTextField,passwordTextField, spinner;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	usernameTextField.delegate = self;
    passwordTextField.delegate = self;
    
    DataManager *dataManager = [DataManager sharedManager];
    
    [dataManager.localData getLocalStudyCallDataFromCore:@"91779"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [[self view] endEditing:TRUE];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField:textField up:YES];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:textField up:NO];
}


- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    int animatedDistance;
    int moveUpValue = textField.frame.origin.y+ textField.frame.size.height;
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        
        animatedDistance = 420-(460-moveUpValue-5);
    }
    else
    {
        animatedDistance = 162-(320-moveUpValue-5);
    }
    
    if(animatedDistance>0)
    {
        const int movementDistance = animatedDistance;
        const float movementDuration = 0.3f;
        int movement = (up ? -movementDistance : movementDistance);
        [UIView beginAnimations: nil context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: movementDuration];
        self.view.frame = CGRectOffset(self.view.frame, 0, movement);
        [UIView commitAnimations];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)onOkTouch:(id)sender {
    /*
    // replace right bar button 'refresh' with spinner
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    if ([ [ UIScreen mainScreen ] bounds ].size.height == 568) {
        spinner.center = CGPointMake(265, 265);
    }
    else{
    spinner.center = CGPointMake(265, 184);
    }
    spinner.hidesWhenStopped = YES;
    [self.view addSubview:spinner];
    [spinner startAnimating];
    
    
    DataManager *dataManager = [DataManager sharedManager];
    
    
    // how we stop refresh from freezing the main UI thread
    dispatch_queue_t signInQueue = dispatch_queue_create("signinprocess", NULL);
    dispatch_async(signInQueue, ^{
        
        
        
        // do any UI stuff on the main UI thread
        dispatch_async(dispatch_get_main_queue(), ^{
            //self.myLabel.text = @"After!";
            
            NSString *studentnummer = usernameTextField.text;
            NSString *password = passwordTextField.text;
            
            //[dataManager.serverData logInToStudyCallWithStudentnummer:studentnummer andPassword:password];
            
                        
            //END ServerDateGetter part
        });
        
    });
     */
    
    /*
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:YES forKey:@"ShouldStartAtBerichten"];
    [defaults synchronize];
    [self performSegueWithIdentifier:@"goToBerichtenschermSegue" sender:nil];
    */
    
    NSString *deviceToken = [[NSUserDefaults standardUserDefaults]valueForKey:@"deviceToken"];
    if(deviceToken == Nil){
        deviceToken = @"";
    }
    LoginUserWebRequest *request = [LoginUserWebRequest initWithStudentId:usernameTextField.text password:passwordTextField.text iosDeviceToken:deviceToken];
    [[WebRequestHandler sharedWebRequestHandler] executeGET:request callBack:self];
}

- (void)requestCompletedWithData:(id)json request:(WebRequest *)originalRequest
{
    //NSLog(@"Done loading data: %@", json);
    DataManager *dataManager = [DataManager sharedManager];
    
    // Parse the feed
    NSArray *jsonLoginResult = json;
    
    NSString *status = [jsonLoginResult valueForKeyPath:@"status"];
    NSString *error = [jsonLoginResult valueForKeyPath:@"error"];
    
    if([status isEqualToString:@"Logged in"]){
        [self didLogIn:status];
    } else {
        [self failedToLogIn:@"Verkeerde studentnummer en/of wachtwoord"];
    }
    /*
    [dataManager.serverData clearTable:@"School"];
    
    for(NSString *school in schoolArray)
    {
        if(![school isKindOfClass:[NSNull class]])
        {
            [dataManager fillLocalStudyCallScholen:school];
        }
        
    }
    
    [dataManager.localData setSchoolData];
    scholenList = [dataManager.localData getDataObjects];
    letterList = [dataManager.localData getDataIndex];
    [spinner stopAnimating];
    [_tableScholen reloadData];*/
}

- (void)didLogIn:(NSString *) statusMessage {
    NSLog(@"Logged in");
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:YES forKey:@"ShouldStartAtBerichten"];
    [defaults synchronize];
    
    [spinner stopAnimating];
    
    [self performSegueWithIdentifier:@"goToBerichtenschermSegue" sender:nil];
}


- (void)requestFailedWithError:(NSError *)error request:(WebRequest *)originalRequest
{
    //failed login
    [self failedToLogIn:@"Geen internet verbinding met de server."];
}

- (void)failedToLogIn:(NSString *) errorMessage {
    [spinner stopAnimating];
    NSLog(@"Failed to login. Error: %@", errorMessage);
    
}
@end
