//
//  StudyCallSettingsAdvertentiesViewController.h
//  SpoK
//
//  Created by wkroos on 20/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StudyCallSettingsAdvertentiesViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property NSArray *advertentieCategorieen;

@end
