//
//  StudyCallSettingsAdvertentiesViewController.m
//  SpoK
//
//  Created by wkroos on 20/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "StudyCallSettingsAdvertentiesViewController.h"
#import "TestDataGetter.h"
#import "AdCategory.h"
#import "AdCell.h"
#import "AdvertManager.h"
@interface StudyCallSettingsAdvertentiesViewController ()

@end

@implementation StudyCallSettingsAdvertentiesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    _advertentieCategorieen = [AdvertManager sharedManager].adCategories;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger total = [_advertentieCategorieen count];
    return total;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"AdCell";
    
    AdCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"AdCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    NSInteger index = indexPath.row;
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    AdCategory *category = [_advertentieCategorieen objectAtIndex:index];
    
    [cell.adNameLabel setText:category.name];
    
    [cell.adDescriptionTextView setText:category.description];
    [cell.adDescriptionTextView setTextColor:[UIColor whiteColor]];
    
    [cell.adEnabledSwitch setOn:category.enabled];
    [cell.adEnabledSwitch addTarget:self action:@selector(adToggeled:) forControlEvents:UIControlEventValueChanged];
    cell.adEnabledSwitch.tag = index;

    return cell;
}

- (void)adToggeled:(UISwitch *)theSwitch
{
    [[AdvertManager sharedManager] changeCategoryEnabledAtIndex:theSwitch.tag toValue:theSwitch.isOn];
}

@end
