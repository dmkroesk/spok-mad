//
//  StudyCallSettingsRoosterViewController.h
//  SpoK
//
//  Created by wkroos on 20/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebRequestHandler.h"

@interface StudyCallSettingsLogoutViewController : UIViewController <WebRequestHandlerCallback>
- (IBAction)logoutButton:(id)sender;


@end
