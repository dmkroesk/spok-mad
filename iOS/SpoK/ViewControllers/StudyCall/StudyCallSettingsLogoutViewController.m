//
//  StudyCallSettingsRoosterViewController.m
//  SpoK
//
//  Created by wkroos on 20/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "StudyCallSettingsLogoutViewController.h"
#import "LogoutUserWebRequest.h"

@interface StudyCallSettingsLogoutViewController ()

@end

@implementation StudyCallSettingsLogoutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)logoutButton:(id)sender {
    NSString *deviceToken = [[NSUserDefaults standardUserDefaults]valueForKey:@"deviceToken"];
    if(deviceToken == Nil){
        deviceToken = @"";
    }
    
    LogoutUserWebRequest *request = [LogoutUserWebRequest initWithDeviceToken: deviceToken];
    [[WebRequestHandler sharedWebRequestHandler] executeGET:request callBack:self];
}


- (void)requestCompletedWithData:(id)json request:(WebRequest *)originalRequest
{
    NSLog(@"succeeded to logout");
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
     [defaults setBool:NO forKey:@"ShouldStartAtBerichten"];
     [defaults synchronize];
    
    //NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    //[[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}

- (void)requestFailedWithError:(NSError *)error request:(WebRequest *)originalRequest
{
    //failed login
    NSLog(@"Failed to logout");
}


@end
