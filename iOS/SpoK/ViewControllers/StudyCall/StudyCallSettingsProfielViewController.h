//
//  StudyCallSettingsProfielViewController.h
//  SpoK
//
//  Created by wkroos on 20/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebRequestHandler.h"

@interface StudyCallSettingsProfielViewController : UIViewController <UITextFieldDelegate, WebRequestHandlerCallback>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *middleNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *birthDateTextField;
@property (weak, nonatomic) IBOutlet UITextField *schoolTextField;

@end
