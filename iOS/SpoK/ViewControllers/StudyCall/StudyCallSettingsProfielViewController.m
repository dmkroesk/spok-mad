//
//  StudyCallSettingsProfielViewController.m
//  SpoK
//
//  Created by wkroos on 20/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "StudyCallSettingsProfielViewController.h"

#import "UpdateUserInfoWebRequest.h"
#import "DataManager.h"

@interface StudyCallSettingsProfielViewController ()

@end

@implementation StudyCallSettingsProfielViewController

@synthesize firstNameTextField,middleNameTextField,lastNameTextField,emailTextField,birthDateTextField,schoolTextField;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    UITapGestureRecognizer *tapScroll = [[UITapGestureRecognizer alloc]initWithTarget:self     action:@selector(tapped)];
    tapScroll.cancelsTouchesInView = NO;
    [_scrollView addGestureRecognizer:tapScroll];
    
    firstNameTextField.delegate = self;
    middleNameTextField.delegate = self;
    lastNameTextField.delegate = self;
    emailTextField.delegate = self;
    birthDateTextField.delegate = self;
    schoolTextField.delegate = self;
    
    NSManagedObject *loggedUser = [DataManager sharedManager].loggedUser;
    firstNameTextField.text = [loggedUser valueForKey:@"voornaam"];
    middleNameTextField.text = [loggedUser valueForKey:@"tussenvoegsel"];
    lastNameTextField.text = [loggedUser valueForKey:@"achternaam"];
    emailTextField.text = [loggedUser valueForKey:@"email"];
    birthDateTextField.text = [loggedUser valueForKey:@"geboortedatum"];
    schoolTextField.text = [loggedUser valueForKey:@"school"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) tapped
{
    [self.view endEditing:YES];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [[self view] endEditing:TRUE];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField:textField up:YES];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:textField up:NO];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    NSInteger nextTag = textField.tag +1;
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if(nextResponder){
        [nextResponder becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return NO;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if(textField.tag==5){
        [self.view endEditing:YES];
        [self birthDateTextFieldClicked];
        
        return NO;
    } else {
        return YES;
    }
}


- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    int animatedDistance;
    int moveUpValue = textField.frame.origin.y+ textField.frame.size.height;
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        
        animatedDistance = 400-(460-moveUpValue-5);
    }
    else
    {
        animatedDistance = 162-(320-moveUpValue-5);
    }
    
    if(animatedDistance>0)
    {
        const int movementDistance = animatedDistance;
        const float movementDuration = 0.3f;
        int movement = (up ? -movementDistance : movementDistance);
        [UIView beginAnimations: nil context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: movementDuration];
        self.view.frame = CGRectOffset(self.view.frame, 0, movement);
        [UIView commitAnimations];
    }
}


- (IBAction)saveUserData:(id)sender
{
    UpdateUserInfoWebRequest *request = [UpdateUserInfoWebRequest initWithStudentId:@"112" firstname:firstNameTextField.text insertion:middleNameTextField.text lastname:lastNameTextField.text email:emailTextField.text dateOfBirth:birthDateTextField.text school:schoolTextField.text];
    [[WebRequestHandler sharedWebRequestHandler] executeGET:request callBack:nil];
    
    NSManagedObject *loggedUser = [DataManager sharedManager].loggedUser;
    NSManagedObjectContext *context = ((AppDelegate*)[[UIApplication sharedApplication] delegate]).managedObjectContext;

    [loggedUser setValue:self.firstNameTextField.text forKey:@"voornaam"];
    [loggedUser setValue:self.middleNameTextField.text forKey:@"tussenvoegsel"];
    [loggedUser setValue:self.lastNameTextField.text forKey:@"achternaam"];
    [loggedUser setValue:self.emailTextField.text forKey:@"email"];
    [loggedUser setValue:self.birthDateTextField.text forKey:@"geboortedatum"];
    [loggedUser setValue:self.schoolTextField.text forKey:@"school"];
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }

}

- (void)birthDateTextFieldClicked {
    
    CGRect toolbarTargetFrame = CGRectMake(0, self.view.bounds.size.height-216-44, 320, 44);
    CGRect datePickerTargetFrame = CGRectMake(0, self.view.bounds.size.height-216, 320, 216);
    
    UIView *darkView = [[UIView alloc] initWithFrame:self.view.bounds];
    darkView.alpha = 0;
    darkView.backgroundColor = [UIColor blackColor];
    darkView.tag = 9;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissDatePicker:)];
    [darkView addGestureRecognizer:tapGesture];
    [self.view addSubview:darkView];
    
    UIDatePicker *datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height+44, 320, 216)];
    datePicker.tag = 10;
    datePicker.backgroundColor = [UIColor whiteColor];
    [datePicker addTarget:self action:@selector(changeDate:) forControlEvents:UIControlEventValueChanged];
    datePicker.datePickerMode = UIDatePickerModeDate;
    
    if(![birthDateTextField.text isEqualToString:@""]){
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
        [dateFormat setDateFormat:@"dd-MM-yyyy"];
        NSDate *date = [dateFormat dateFromString:birthDateTextField.text];
        [datePicker setDate:date];
    }
    [self.view addSubview:datePicker];
    
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height, 320, 44)];
    toolBar.tag = 11;
    toolBar.barStyle = UIBarStyleDefault;
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissDatePicker:)];
    
    [toolBar setItems:[NSArray arrayWithObjects:spacer, doneButton, nil]];
    [self.view addSubview:toolBar];
    
    [UIView beginAnimations:@"MoveIn" context:nil];
    toolBar.frame = toolbarTargetFrame;
    datePicker.frame = datePickerTargetFrame;
    darkView.alpha = .5;
    [UIView commitAnimations];
}

- (void)changeDate:(UIDatePicker *)sender {
    NSLog(@"New Date: %@", sender.date);
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"dd-MM-yyyy"];
    NSString *dateString = [dateFormat stringFromDate:sender.date];
    
    [birthDateTextField setText:dateString];
}

- (void)removeViews:(id)object {
    NSLog(@"Removeviews");
    [[self.view viewWithTag:9] removeFromSuperview];
    [[self.view viewWithTag:10] removeFromSuperview];
    [[self.view viewWithTag:11] removeFromSuperview];
}

- (void)dismissDatePicker:(id)sender {
    NSLog(@"Dismiss datepicker");
    CGRect toolbarTargetFrame = CGRectMake(0, self.view.bounds.size.height, 320, 44);
    CGRect datePickerTargetFrame = CGRectMake(0, self.view.bounds.size.height+44, 320, 216);
    [UIView beginAnimations:@"MoveOut" context:nil];
    [self.view viewWithTag:9].alpha = 0;
    [self.view viewWithTag:10].frame = datePickerTargetFrame;
    [self.view viewWithTag:11].frame = toolbarTargetFrame;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(removeViews:)];
    [UIView commitAnimations];
    UITextField *textFieldNext = (UITextField *)[self.view viewWithTag:6];
    [textFieldNext becomeFirstResponder];
    
}


- (void)requestCompletedWithData:(NSDictionary *)theData request:(WebRequest *)originalRequest
{
    NSLog(@"completed");
}

- (void)requestFailedWithError:(NSError *)error request:(WebRequest *)originalRequest
{
    NSLog(@"failed");
}

@end
