//
//  StudyCallSettingsSocialMediaViewController.m
//  SpoK
//
//  Created by wkroos on 20/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "StudyCallSettingsSocialMediaViewController.h"

@interface StudyCallSettingsSocialMediaViewController ()

@end

@implementation StudyCallSettingsSocialMediaViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
