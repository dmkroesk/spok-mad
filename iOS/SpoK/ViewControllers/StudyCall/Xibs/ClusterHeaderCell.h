//
//  ClusterHeaderCell.h
//  SpoK
//
//  Created by Dennis Keldermans on 12/4/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClusterHeaderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *clusterLabel;
@property (weak, nonatomic) IBOutlet UISwitch *clusterSwitch;

@end
