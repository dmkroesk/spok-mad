//
//  ClusterHeaderCell.m
//  SpoK
//
//  Created by Dennis Keldermans on 12/4/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "ClusterHeaderCell.h"

@implementation ClusterHeaderCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
