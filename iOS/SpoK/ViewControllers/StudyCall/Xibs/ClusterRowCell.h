//
//  ClusterRowCell.h
//  SpoK
//
//  Created by Dennis Keldermans on 12/4/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClusterRowCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *tijdLabel;
@property (nonatomic, weak) IBOutlet UILabel *vakLabel;
@property (nonatomic, weak) IBOutlet UILabel *lokaalLabel;
@property (weak, nonatomic) IBOutlet UILabel *uurLabel;

@end
