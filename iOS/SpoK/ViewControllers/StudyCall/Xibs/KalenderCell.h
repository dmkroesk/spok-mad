//
//  KalenderCell.h
//  SpoK
//
//  Created by Dennis Keldermans on 26/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KalenderCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *tijdLabel;
@property (nonatomic, weak) IBOutlet UILabel *vakLabel;
@property (nonatomic, weak) IBOutlet UILabel *lokaalLabel;
@property (weak, nonatomic) IBOutlet UILabel *uurLabel;
@property (weak, nonatomic) IBOutlet UIView *leftLine;

@end
