//
//  AdCell.h
//  SpoK
//
//  Created by atmstudent on 18/12/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UISwitch *adEnabledSwitch;
@property (weak, nonatomic) IBOutlet UITextView *adDescriptionTextView;
@property (weak, nonatomic) IBOutlet UILabel *adNameLabel;

@end
