//
//  DayPage.h
//  SpoK
//
//  Created by atmstudent on 27/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DayObject.h"

@interface DayPage : NSObject <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *theTableview;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) NSMutableArray *data;

- (id)initWithFrame:(CGRect)frame;
- (void)addDataObject:(DayObject*)dayObj;
- (void)setTitle:(NSString*)toSet;

@end
