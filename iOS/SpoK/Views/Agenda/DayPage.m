//
//  DayPage.m
//  SpoK
//
//  Created by atmstudent on 27/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "DayPage.h"
#import "DayTableViewCell.h"


@implementation DayPage
@synthesize theTableview, data, titleLabel;

- (id)initWithFrame:(CGRect)frame
{
    if (self) {
        // Initialization code
        data = [[NSMutableArray alloc] init];
        titleLabel = [[UILabel alloc] init];
        [titleLabel setTextColor:COLOR_AVANSRED];
        [titleLabel setFont:[UIFont boldSystemFontOfSize:18.0]];
        [titleLabel sizeToFit];
        
        theTableview = [[UITableView alloc] initWithFrame:frame];
        theTableview.delegate = self;
        theTableview.dataSource = self;
    }
    return self;
}

- (void)setTitle:(NSString*)toSet
{
    [titleLabel setText:toSet];
    [titleLabel sizeToFit];
}

- (void)addDataObject:(DayObject*)dayObj
{
    [data addObject:dayObj];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DayTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AvansDayCell"];
    
    if (cell == nil)
    {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"DayTableViewCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }
    return cell.frame.size.height;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [data count];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DayTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DayCell"];
    
    if (cell == nil)
    {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"DayTableViewCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
        
    }
    
    DayObject *obj = [data objectAtIndex:indexPath.row];
    [cell.courseNameLabel setText:obj.name];
    [cell.timeLabel setText:obj.time];
    [cell.locationLabel setText:obj.location];
    
    return cell;
}

@end
