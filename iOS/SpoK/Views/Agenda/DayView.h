//
//  DayView.h
//  SpoK
//
//  Created by atmstudent on 27/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMPageView.h"
#import "DayPage.h"

@interface DayView : UIView <MMPageViewDataSource>

@property (nonatomic, strong) NSMutableArray *dayPages;
@property (nonatomic, strong) MMPageView *m_pageView;

- (void)addDayPage:(DayPage*)toAdd;

@end
