//
//  MeetingView.m
//  SpoK
//
//  Created by atmstudent on 04/12/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "MeetingView.h"
#import "TimerView.h"

@interface MeetingView ()
{

}
@end

@implementation MeetingView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        TimerView *tView = [[TimerView alloc] initWithFrame:frame];
        [self addSubview:tView];
    }
    return self;
}

/*// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    // Get the current graphics context
    // (ie. where the drawing should appear)
}*/


@end
