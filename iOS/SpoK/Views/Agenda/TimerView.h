//
//  TimerView.h
//  SpoK
//
//  Created by atmstudent on 04/12/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimerView : UIView

@property (nonatomic, strong) NSDate *meetingDate;

@end
