//
//  MeetingView.m
//  SpoK
//
//  Created by atmstudent on 04/12/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "TimerView.h"
#import <math.h>

@interface TimerView ()
{
    CGFloat startAngleRadians;
    CGFloat endAngleRadians;
    CGFloat radius;
    CGFloat lineWidth;
    CGFloat innerCircleOffset;
    CGFloat fontSize;
    NSTimer *updateTimer;
    
}
@end

@implementation TimerView
@synthesize meetingDate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

- (void)willMoveToSuperview:(UIView *)newSuperview
{
    meetingDate = [NSDate dateWithTimeIntervalSinceNow:60]; //TESTVALUE
    if(!lineWidth)
    {
        lineWidth = 2.5;
    }
    if(!radius)
    {
        radius = (self.frame.size.height > self.frame.size.width) ? self.frame.size.width : self.frame.size.height;
        radius = (radius / 2) - lineWidth;
    }
    if(!innerCircleOffset)
    {
        innerCircleOffset = 6.0;
    }
    if(!fontSize)
    {
        fontSize = 35.0;
    }
    updateTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateView) userInfo:Nil repeats:YES];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    // Get the current graphics context
    // (ie. where the drawing should appear)
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //Drawing the black outer circle
    CGContextBeginPath(context);
    CGContextAddArc(context,
                    self.center.x,
                    self.center.y,
                    radius,
                    startAngleRadians,
                    endAngleRadians,
                    YES);
    
    CGContextSetLineWidth(context, lineWidth);
    CGContextSetRGBStrokeColor(context, 0.0, 0.0, 0.0, 1.0); //black
    CGContextDrawPath(context, kCGPathStroke);
    
    //Drawing the inner circle
    CGContextBeginPath(context);
    const CGFloat *components = CGColorGetComponents(COLOR_AVANSRED.CGColor);
    CGContextSetRGBFillColor(context, components[0], components[1], components[2], components[3]); //Avansred
    CGContextAddArc(context, self.center.x, self.center.y, radius - innerCircleOffset, 0, 2 * M_PI, YES);
    CGContextDrawPath(context, kCGPathFill);
    
    //Drawing the time label
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"HH:mm";
    NSString *timeString = [dateFormatter stringFromDate:meetingDate];
    UIFont *font = [UIFont systemFontOfSize:fontSize];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    NSDictionary *attributes = @{
                                 NSFontAttributeName: font,
                                 NSParagraphStyleAttributeName: paragraphStyle,
                                 NSForegroundColorAttributeName: [UIColor whiteColor]
                                 };
    
    CGSize stringSize;
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:timeString attributes:attributes];
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        stringSize = [timeString sizeWithAttributes:attributes];
    }
    else
    {
        
        CGRect size = [attString boundingRectWithSize:CGSizeMake((radius - innerCircleOffset) *2, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
        stringSize = size.size;
    }
    CGRect textRect = CGRectMake(self.center.x - radius + innerCircleOffset, self.center.y - (stringSize.height/2), (radius - innerCircleOffset) *2, stringSize.height);

    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        [timeString drawInRect:textRect withAttributes:attributes];
    }
    else
    {
        [[UIColor whiteColor] setFill];
        [attString drawInRect:textRect];
    }
    [[UIColor blackColor] setFill];
}

- (void)calculateRadians
{
    NSTimeInterval secondsToMeeting = [meetingDate timeIntervalSinceNow];
    double secondsInHour = 60;// * 60;
    if(secondsToMeeting > 0)      // If the time for the meeting is to come.
    {
        if(secondsToMeeting <= secondsInHour)
        {
            double percentage = (secondsToMeeting / secondsInHour);
            double endDegrees = 270 - (360 * percentage);
            double radians = endDegrees * (M_PI / 180);
            
            endAngleRadians = radians;
        }
        else
        {
            endAngleRadians = 270 * (M_PI / 180);
        }
    }
    else
    {
        endAngleRadians = 270 * (M_PI / 180);
    }
    startAngleRadians = 270 * (M_PI / 180);
}

- (void)updateView
{
    [self calculateRadians];
    [self setNeedsDisplay];
}

@end
