//
//  WeekPage.h
//  SpoK
//
//  Created by atmstudent on 29/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WeekObject.h"

@interface WeekPage : NSObject <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *theTableview;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) NSMutableArray *data;

- (id)initWithFrame:(CGRect)frame;
- (void)addDataObject:(WeekObject*)weekObj;
- (void)setTitle:(NSString*)toSet;

@end
