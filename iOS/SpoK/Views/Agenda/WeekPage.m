//
//  WeekPage.m
//  SpoK
//
//  Created by atmstudent on 29/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "WeekPage.h"
#import "WeekTableViewCell.h"

@implementation WeekPage
@synthesize data, theTableview, titleLabel;

- (id)initWithFrame:(CGRect)frame
{
    if (self) {
        // Initialization code
        data = [[NSMutableArray alloc] init];
        titleLabel = [[UILabel alloc] init];
        [titleLabel setTextColor:COLOR_AVANSRED];
        [titleLabel setFont:[UIFont boldSystemFontOfSize:18.0]];
        [titleLabel sizeToFit];
        
        theTableview = [[UITableView alloc] initWithFrame:frame];
        theTableview.delegate = self;
        theTableview.dataSource = self;
    }
    return self;
}

- (void)setTitle:(NSString*)toSet
{
    [titleLabel setText:toSet];
    [titleLabel sizeToFit];
}

- (void)addDataObject:(WeekObject*)weekObj
{
    [data addObject:weekObj];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WeekTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AvansWeekCell"];
    
    if (cell == nil)
    {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"WeekTableViewCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }
    return cell.frame.size.height;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [data count];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WeekTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AvansWeekCell"];
    
    if (cell == nil)
    {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"WeekTableViewCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    WeekObject *obj = [data objectAtIndex:indexPath.row];
    cell.messageAmountLabel.text = obj.messageAmount;
    cell.dayName.text = obj.dayName;
    
    return cell;
}


@end
