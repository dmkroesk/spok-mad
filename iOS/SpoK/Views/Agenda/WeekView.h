//
//  WeekView.h
//  SpoK
//
//  Created by atmstudent on 29/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMPageView.h"
#import "WeekPage.h"

@interface WeekView : UIView <MMPageViewDataSource>

@property (nonatomic, strong) NSMutableArray *weekPages;
@property (nonatomic, strong) MMPageView *m_pageView;

- (void)addWeekPage:(WeekPage*)toAdd;

@end
