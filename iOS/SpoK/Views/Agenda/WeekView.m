//
//  WeekView.m
//  SpoK
//
//  Created by atmstudent on 27/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "WeekView.h"

#import "WeekTableViewCell.h"

@implementation WeekView
@synthesize m_pageView, weekPages;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        m_pageView = [MMPageView pageViewWithFrame:frame leftButtonImage: [UIImage imageNamed:@"AvansPageViewButtonTitle Left"] rightButtonImage:[UIImage imageNamed:@"AvansPageViewButtonTitle Right"] pageControlTint:[UIColor colorWithRed:64.0/255.0 green:64.0/255.0 blue:64.0/255.0 alpha:255.0/255.0]];
        [m_pageView setArrowStyle:ArrowStyleTitleEdge];
        m_pageView.dataSource = self;
        weekPages = [[NSMutableArray alloc] init];
        [self addSubview:m_pageView];
    }
    return self;
}

- (void)addWeekPage:(WeekPage*)toAdd
{
    [weekPages addObject:toAdd];
    [m_pageView reloadPages];
}

# pragma mark - MMPageView datasource

- (NSInteger)numberOfPagesInPageView:(MMPageView *)pageView
{
    return [weekPages count];
}

- (UIView*)pageView:(MMPageView *)pageView contentViewForIndex:(NSInteger)index
{
    return ((WeekPage*)[weekPages objectAtIndex:index]).theTableview;
}

- (UIView*)pageview:(MMPageView*)pageView titleViewForIndex:(NSInteger)index
{
    return ((WeekPage*)[weekPages objectAtIndex:index]).titleLabel;
}

@end
