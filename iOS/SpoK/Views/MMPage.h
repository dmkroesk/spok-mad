//
//  MMPage.h
//  SpoK
//
//  Created by atmstudent on 21/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMPage : UIView

typedef enum {
    AlignmentLeft,                                                  // Aligns the view to the left of the page viewer
    AlignmentCenter,                                                // Aligns the view in the center of the page viewer
    AlignmentRight,                                                 // Aligns the view to the right of the page viewer
} Alignment;

@property (nonatomic, strong) UIView *titleView;
@property (nonatomic, strong) UIView *contentView;

+ (MMPage*)pageWithContentView:(UIView*)content titleView:(UIView*)title frame:(CGRect)frame contentAlignment:(Alignment)contentAlignment titleAlignment:(Alignment)titleAlignment contentOffset:(CGFloat)offset;

@end
