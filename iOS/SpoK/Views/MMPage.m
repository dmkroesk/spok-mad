//
//  MMPage.m
//  SpoK
//
//  Created by atmstudent on 21/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "MMPage.h"
@interface MMPage ()
{
    float originalContentY;
    float originalTitleY;
}

@end

@implementation MMPage

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

+ (MMPage*)pageWithContentView:(UIView*)content titleView:(UIView*)title frame:(CGRect)frame contentAlignment:(Alignment)contentAlignment titleAlignment:(Alignment)titleAlignment contentOffset:(CGFloat)offset
{
    MMPage* page = [[MMPage alloc] initWithFrame:frame];
    
    if(!page->originalContentY)
    {
        page->originalContentY = content.frame.origin.y;
    }
    if(!page->originalTitleY)
    {
        page->originalTitleY = title.frame.origin.y;
    }
    
    page.contentView = content;
    page.titleView = title;
    
    [content setFrame:CGRectMake(content.frame.origin.x, page->originalTitleY, content.frame.size.width, content.frame.size.height)];
    [title setFrame:CGRectMake(title.frame.origin.x, page->originalTitleY, title.frame.size.width, title.frame.size.height)];
    
    switch (titleAlignment)
    {
        case AlignmentRight:
        {
            [title setFrame:CGRectMake((page.frame.size.width - title.frame.size.width) - offset, title.frame.origin.y, title.frame.size.width, title.frame.size.height)];
            break;
        }
        case AlignmentCenter:
        {
            [title setFrame:CGRectMake((page.frame.size.width/2) - (title.frame.size.width/2), title.frame.origin.y, title.frame.size.width, title.frame.size.height)];
            break;
        }
        case AlignmentLeft:
        default:
        {
            [title setFrame:CGRectMake(0 + offset, title.frame.origin.y, title.frame.size.width, title.frame.size.height)];
            break;
        }
    }
    
    switch (contentAlignment)
    {
        case AlignmentRight:
        {
            [content setFrame:CGRectMake((page.frame.size.width - content.frame.size.width) - offset, content.frame.origin.y, content.frame.size.width, content.frame.size.height)];
            break;
        }
        case AlignmentCenter:
        {
            [content setFrame:CGRectMake((page.frame.size.width/2) - (content.frame.size.width/2), content.frame.origin.y, content.frame.size.width, content.frame.size.height)];
            break;
        }
        case AlignmentLeft:
        default:
        {
            [content setFrame:CGRectMake(0 + offset, content.frame.origin.y, content.frame.size.width, content.frame.size.height)];
            break;
        }
    }
    
    [content setFrame:CGRectMake(content.frame.origin.x, content.frame.origin.y + title.frame.size.height, content.frame.size.width, content.frame.size.height)];    //Set the frame of the content view so it is located beneath the titleview
    
    [page addSubview:title];
    [page addSubview:content];
    
    return page;
}

@end
