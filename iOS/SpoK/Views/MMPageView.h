//
//  PagedView.h
//  SpoK
//
//  Created by atmstudent on 15/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMPage.h"

@class MMPageView;
@protocol MMPageViewDataSource <NSObject>

@required
- (UIView*)pageView:(MMPageView*)pageView contentViewForIndex:(NSInteger)index;
- (UIView*)pageview:(MMPageView*)pageView titleViewForIndex:(NSInteger)index;
- (NSInteger)numberOfPagesInPageView:(MMPageView*)pageView;

@optional
- (void)pageView:(MMPageView*)pageView enteredPageAtIndex:(NSInteger)index;

@end

@interface MMPageView : UIView <UIScrollViewDelegate>

typedef enum {
    ArrowStyleContent,                                              // Centers the side arrows on the content view
    ArrowStyleTitle,                                                // Aligns the side arrows on the title view on it's center Y
    ArrowStyleTitleEdge,                                            // Aligns the arrows to the center Y of the title view and with an offset of 20 pixels to it's left/right
    ArrowStyleView,                                                 // Centers the side arrows on the entire view
    ArrowStyleNone                                                  // Hides the arrows
} ArrowStyle;

@property (nonatomic, strong) NSMutableArray *titleViews;           // Array containing the title views
@property (nonatomic, strong) NSMutableArray *contentViews;         // Array containing the content views
@property (nonatomic, strong) NSMutableArray *pages;                // Array containing the UIView objects
@property (nonatomic, strong) UIScrollView *scrollView;             // The container for our views
@property (nonatomic, strong) UIPageControl *pageControl;           // Pagecontrol indicating the page.
@property (nonatomic, strong) UIButton *leftButton;                 // Left side button, public for optional customization
@property (nonatomic, strong) UIButton *rightButton;                // Right side button, public for optional custumization
@property (nonatomic, assign) ArrowStyle arrowStyle;                // Enum indicating where the arrow indicators should be. Default is ArrowStyleView
@property (nonatomic, assign) Alignment contentAlignment;           // Enum indicating where the content is aligned in the pageviewer. Default is ContentAlignmentLeft
@property (nonatomic, assign) Alignment titleAlignment;             // Enum indicating where the title is aligned in the pageviewer. Default is TitleAlignmentLeft
@property (nonatomic, assign) float contentOffset;

@property (nonatomic, strong) id<MMPageViewDataSource> dataSource;  // The datasource for the pageview

+ (MMPageView*) pageViewWithFrame:(CGRect)frame leftButtonImage:(UIImage*)leftImage rightButtonImage:(UIImage*)rightImage pageControlTint:(UIColor*)tint;

- (void)reloadPages;
- (void)setArrowStyle:(ArrowStyle)style;                            // Sets the arrow style
- (void)setPageContentOffset:(float)newOffset;                      // Sets the LEFT and RIGHT border offset for the content. If the content is larger than the width - 2 * offset, the content will be clipped
- (void)goToPage:(NSInteger)pageIndex;                              // Moves the pageviewer to the given page
- (void)disableManualScrolling;                                     // Disables manual scrolling of the pages. Use this if you only want to use it in combination with buttons for instance.
- (void)setContentViewAlignment:(Alignment)newAlignment;            // Sets a new alignment for the content, call reloadPages to apply
- (void)setTitleViewAlignment:(Alignment)newAlignment;              // Sets a new alignment for the title, call reloadPages to apply

@end
