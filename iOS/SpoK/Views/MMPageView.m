//
//  PagedView.m
//  SpoK
//
//  Created by atmstudent on 15/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#define BUTTON_VIEW_OFFSET 15

#import "MMPageView.h"

@implementation MMPageView
{
    NSTimer *buttonAnimationTimer;
}

@synthesize pageControl, scrollView, pages, leftButton, rightButton, arrowStyle, contentOffset;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        UIImage *leftButtonImg = [UIImage imageNamed:@"SPoKPageViewButton Left"];
        UIImage *rightButtonImg = [UIImage imageNamed:@"SPoKPageViewButton Right"];
        
        leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [leftButton setImage:leftButtonImg forState:UIControlStateNormal];
        [leftButton sizeToFit];
        [leftButton addTarget:self action:@selector(previousPage:) forControlEvents:UIControlEventTouchUpInside];
        
        rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [rightButton setImage:rightButtonImg forState:UIControlStateNormal];
        [rightButton sizeToFit];
        [rightButton addTarget:self action:@selector(nextPage:) forControlEvents:UIControlEventTouchUpInside];
        
        pages = [[NSMutableArray alloc] init];
        
        float pageControlHeight = 37;
        
        pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, 0, 50, pageControlHeight)];
        [pageControl addTarget:self action:@selector(pageControlPageChangedValue) forControlEvents:UIControlEventValueChanged];
        pageControl.center = [self convertPoint:self.center fromView:self];
        pageControl.frame = CGRectMake(pageControl.frame.origin.x, 0 + self.frame.size.height - pageControlHeight, pageControl.frame.size.width, pageControl.frame.size.height);
        pageControl.currentPage = 0;
        [self addSubview:pageControl];
        
        [self setUserInteractionEnabled:YES];
    }
    return self;
}

+ (MMPageView*) pageViewWithFrame:(CGRect)frame leftButtonImage:(UIImage*)leftImage rightButtonImage:(UIImage*)rightImage pageControlTint:(UIColor*)tint
{
    MMPageView *pageView = [[MMPageView alloc] initWithFrame:frame];
    [pageView.leftButton setImage:leftImage forState:UIControlStateNormal];
    [pageView.rightButton setImage:rightImage forState:UIControlStateNormal];
    [[UIPageControl appearance] setPageIndicatorTintColor:[tint colorWithAlphaComponent:0.5]];
    [[UIPageControl appearance] setCurrentPageIndicatorTintColor:tint];
    
    return pageView;
}

- (void)willMoveToSuperview:(UIView *)newSuperview
{
    [self reloadPages];
}

# pragma mark - Public functions

- (void)reloadPages
{
    if(scrollView)
    {
        [scrollView removeFromSuperview];
        [leftButton removeFromSuperview];
        [rightButton removeFromSuperview];
    }
    
    for(UIView *subView in scrollView.subviews)
    {
        [subView removeFromSuperview];
    }
    
    CGRect scrollViewFrame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height - 37); //deduct 37 for the pagecontrol height
    scrollView = [[UIScrollView alloc] initWithFrame:scrollViewFrame];
    scrollView.delegate = self;
    [scrollView setBounces:YES];
    [scrollView setPagingEnabled:YES];
    [scrollView setShowsHorizontalScrollIndicator:NO];
    [scrollView setShowsVerticalScrollIndicator:NO];
    
    [self addSubview:scrollView];
    [self addSubview:leftButton];
    [self addSubview:rightButton];
    
    pages = [[NSMutableArray alloc] init];
    
    if(self.dataSource)
    {
        NSInteger amountOfPages = [self.dataSource numberOfPagesInPageView:self];
        
        for(int i = 0; i < amountOfPages; i++)
        {
            UIView *content = [self.dataSource pageView:self contentViewForIndex:i];
            UIView *titleView = [self.dataSource pageview:self titleViewForIndex:i];
            [self createPageForContentView:content titleView:titleView frame:self.frame];           //Creates a new page with the width of the pageviewer.
        }
    }
    
    [self applyArrowStyle:arrowStyle];
    [self checkPageEdgeHide];
}

- (void)setArrowStyle:(ArrowStyle)style
{
    arrowStyle = style;
    [self applyArrowStyle:arrowStyle];
}

- (void)setPageContentOffset:(float)newOffset
{
    self.contentOffset = newOffset;
}

- (void)setContentViewAlignment:(Alignment)newAlignment
{
    self.contentAlignment = newAlignment;
}

- (void)setTitleViewAlignment:(Alignment)newAlignment
{
    self.titleAlignment = newAlignment;
}

- (void)disableManualScrolling
{
    [scrollView setScrollEnabled:NO];
}

- (void)goToPage:(NSInteger)pageIndex
{
    CGRect lastVisibleRect;
    CGSize contentSize = [scrollView contentSize];
    lastVisibleRect.size.height = contentSize.height;
    lastVisibleRect.origin.y = 0.0;
    lastVisibleRect.size.width = self.frame.size.width;
    lastVisibleRect.origin.x  = self.frame.size.width * pageIndex;
    
    [scrollView scrollRectToVisible:lastVisibleRect animated:YES];                         //After this is called, scrollViewDidScroll is called to handle the rest.
}

#pragma mark - private functions
- (void)applyArrowStyle:(ArrowStyle)style
{
    [leftButton setHidden:NO];
    [rightButton setHidden:NO];
    switch(style)
    {
        case ArrowStyleNone:
        {
            [leftButton setHidden:YES];
            [rightButton setHidden:YES];
        }
        case ArrowStyleContent:
        {
            if([pages count] > 0)
            {
                MMPage *pageView = ((MMPage*)[pages objectAtIndex:pageControl.currentPage]);
                UIView *currentContentView = pageView.contentView;
                float buttonY = currentContentView.frame.size.height / 2 - leftButton.frame.size.height / 2 + currentContentView.frame.origin.y;
            
                [leftButton setFrame:CGRectMake(BUTTON_VIEW_OFFSET, buttonY, leftButton.frame.size.width, leftButton.frame.size.height)];
                [rightButton setFrame:CGRectMake(self.frame.size.width - rightButton.frame.size.width - BUTTON_VIEW_OFFSET, buttonY, rightButton.frame.size.width, rightButton.frame.size.height)];
            }
            break;
        }
        case ArrowStyleTitle:
        {
            if([pages count] > 0)
            {
                MMPage *pageView = ((MMPage*)[pages objectAtIndex:pageControl.currentPage]);
                UIView *currentTitleView = pageView.titleView;
                float buttonY = currentTitleView.frame.size.height / 2 - leftButton.frame.size.height / 2;
                
                [leftButton setFrame:CGRectMake(BUTTON_VIEW_OFFSET, buttonY, leftButton.frame.size.width, leftButton.frame.size.height)];
                [rightButton setFrame:CGRectMake(self.frame.size.width - rightButton.frame.size.width - BUTTON_VIEW_OFFSET, buttonY, rightButton.frame.size.width, rightButton.frame.size.height)];
            }
            break;
        }
        case ArrowStyleTitleEdge:
        {
            if([pages count] > 0)
            {
                MMPage *pageView = ((MMPage*)[pages objectAtIndex:pageControl.currentPage]);
                UIView *currentTitleView = pageView.titleView;
                float buttonY = currentTitleView.frame.size.height / 2 - leftButton.frame.size.height / 2;
                float leftButtonX = currentTitleView.frame.origin.x - leftButton.frame.size.width - 20;
                float rightButtonX = currentTitleView.frame.origin.x + currentTitleView.frame.size.width + 20;
            
                [leftButton setFrame:CGRectMake(leftButtonX, buttonY, leftButton.frame.size.width, leftButton.frame.size.height)];
                [rightButton setFrame:CGRectMake(rightButtonX, buttonY, rightButton.frame.size.width, rightButton.frame.size.height)];
            }
            break;
        }
        case ArrowStyleView:
        default:
        {
                float buttonY = ((self.frame.size.height - pageControl.frame.size.height) / 2) - (leftButton.frame.size.height / 2) ;
            [leftButton setFrame:CGRectMake(BUTTON_VIEW_OFFSET, buttonY, leftButton.frame.size.width, leftButton.frame.size.height)];
            [rightButton setFrame:CGRectMake(self.frame.size.width - rightButton.frame.size.width - BUTTON_VIEW_OFFSET, buttonY, rightButton.frame.size.width, rightButton.frame.size.height)];
            break;
        }
    }
    
}

- (void)createPageForContentView:(UIView*)content titleView:(UIView*)titleView frame:(CGRect)frame
{
    MMPage *page = [MMPage pageWithContentView:content titleView:titleView frame:frame contentAlignment:self.contentAlignment titleAlignment:self.titleAlignment contentOffset:self.contentOffset];
    [self addPage:page];
}

- (void)addPage:(UIView *)page
{
    [page setFrame:[self createPageFrameForView:page]];
    [self.scrollView addSubview:page];
    [pages addObject:page];
    pageControl.numberOfPages = [pages count];
    [self updateContentSize];
}

- (CGRect)createPageFrameForView:(UIView*)view
{
    CGRect frame;
    
    frame.origin.x = (self.scrollView.frame.size.width * [pages count]);
    frame.origin.y = 0;
    frame.size = view.frame.size;
    if((frame.size.width + frame.origin.x) > (self.frame.size.width))
    {
        frame.size.width = self.frame.size.width - 2 * contentOffset;
    }
    
    return frame;
}

- (void)updateContentSize
{
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width * [pages count], scrollView.frame.size.height);
}

- (void)showButtons
{
    if(pageControl.currentPage > 0)
    {
        [leftButton setAlpha:1.0];
    }
    if(pageControl.currentPage < [pages count] - 1)
    {
        [rightButton setAlpha:1.0];
    }
    
    [buttonAnimationTimer invalidate];
    buttonAnimationTimer = nil;
}

- (void)hideButtons
{
    buttonAnimationTimer = [NSTimer timerWithTimeInterval:2.0 target:self selector:@selector(hideButtonsAnimation) userInfo:nil repeats:NO];
    
    NSRunLoop *runLoop = [NSRunLoop currentRunLoop];
    [runLoop addTimer:buttonAnimationTimer forMode:NSDefaultRunLoopMode];
}

- (void)hideButtonsAnimation
{
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveLinear
                     animations:
     ^{
         [leftButton setAlpha:0.0];
         [rightButton setAlpha:0.0];
     }
                     completion:nil];
}

- (void)checkPageEdgeHide
{
    if(pageControl.currentPage == 0)
    {
        [leftButton setAlpha:0.0];
    }
    else
    {
        [leftButton setAlpha:1.0];
    }
    if(pageControl.currentPage == [pages count] - 1)
    {
        [rightButton setAlpha:0.0];
    }
    else
    {
        [rightButton setAlpha:1.0];
    }
}

#pragma mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    [self showButtons];
    // Update the page when more than 50% of the previous/next page is visible
    CGFloat pageWidth = self.scrollView.frame.size.width;
    int currentCalculatedPage = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    
    if(pageControl.currentPage != currentCalculatedPage)
    {
        pageControl.currentPage = currentCalculatedPage;
        
        if([self.dataSource respondsToSelector:@selector(pageView:enteredPageAtIndex:)])
        {
            [self.dataSource pageView:self enteredPageAtIndex:currentCalculatedPage];
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self hideButtons];
    [self checkPageEdgeHide];
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    [self hideButtons];
    [self checkPageEdgeHide];
}

#pragma mark - PageControl valueChanged handler
- (void)pageControlPageChangedValue
{
    [self goToPage:pageControl.currentPage];
}

#pragma mark - Arrow handlers
- (void)nextPage:(id)sender
{
    [self goToPage:pageControl.currentPage + 1];
}

-(void)previousPage:(id)sender
{
    [self goToPage:pageControl.currentPage - 1];
}

@end
