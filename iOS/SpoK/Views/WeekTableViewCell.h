//
//  WeekTableViewCell.h
//  SpoK
//
//  Created by atmstudent on 29/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WeekTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *messageAmountBackground;
@property (weak, nonatomic) IBOutlet UILabel *dayName;
@property (weak, nonatomic) IBOutlet UILabel *messageAmountLabel;


@end
